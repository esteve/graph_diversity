INSERT OR IGNORE INTO t_edge_meta (
	edge_type,
	domain,
	codomain
) VALUES (
	?1,
	?2,
	?3
);
