import sqlite3 as sql
import sys
import time
import re

#DB_PATH = "data/target/main8.db"
DB_PATH = "data/target/main_FR.db"

ENABLE_PRINT = False

def reconstruct_sentences(db, tgt: str):
    curs1 = db.cursor()
    curs2 = db.cursor()

    """
    f = open("src/sql/reconstruct_sentences.sql", "rt", encoding="utf-8")
    s = f.read()
    f.close()

    r = curs.execute(s, (tgt,))

    output = []
    previous_fk_sentence = None
    for row in r:
        if previous_fk_sentence != None and row[0] != previous_fk_sentence:
            yield " ".join(output)
            output = []
        output.append(row[2])
        previous_fk_sentence = row[0]
    """

    f1 = open("src/sql/get_fk_sentence.sql", "rt", encoding="utf-8")
    s1 = f1.read()
    f1.close()

    f2 = open("src/sql/get_tokens.sql", "rt", encoding="utf-8")
    s2 = f2.read()
    f2.close()

    for row1 in curs1.execute(s1, (tgt,)):
        """
        output = []
        for row2 in curs2.execute(s2, (row1[0],)):
            output.append(row2[0])
        yield " ".join(output)
        """
        r = curs2.execute(s2, (row1[0],))
        #yield " ".join(r.fetchall())
        #print(r.fetchall())
        yield " ".join(list(i[0] for i in r.fetchall()))

'''
def regexp(expr, item_):
    """
    print(expr)
    print(item_)
    """
    return re.search(expr, item_) != None
    """"""
    """
    try:
        r = re.compile(expr)
        return r.search(item_) != None
    except Exception as e:
        print(e)
    """
'''

def _regexp_wrapper():
    latest_pattern_str = None
    current_regex = None

    def regexp(expr, item_):
        nonlocal latest_pattern_str
        nonlocal current_regex

        if expr != latest_pattern_str:
            current_regex = re.compile(expr)
            latest_pattern_str = expr[:]

        return current_regex.search(item_) != None

    return regexp

regexp = _regexp_wrapper()

def reconstruct_sentences_regexp(db, tgt: str):
    curs1 = db.cursor()
    curs2 = db.cursor()

    """
    f = open("src/sql/reconstruct_sentences.sql", "rt", encoding="utf-8")
    s = f.read()
    f.close()

    r = curs.execute(s, (tgt,))

    output = []
    previous_fk_sentence = None
    for row in r:
        if previous_fk_sentence != None and row[0] != previous_fk_sentence:
            yield " ".join(output)
            output = []
        output.append(row[2])
        previous_fk_sentence = row[0]
    """

    f1 = open("src/sql/get_fk_sentence_regexp.sql", "rt", encoding="utf-8")
    s1 = f1.read()
    f1.close()

    f2 = open("src/sql/get_tokens_regexp.sql", "rt", encoding="utf-8")
    s2 = f2.read()
    f2.close()

    for row1 in curs1.execute(s1, ("|".join(tgt),)):
        """
        output = []
        for row2 in curs2.execute(s2, (row1[0],)):
            output.append(row2[0])
        yield " ".join(output)
        """
        r = curs2.execute(s2, (row1[0],))
        #yield " ".join(r.fetchall())
        #print(r.fetchall())
        yield " ".join(list(i[0] for i in r.fetchall()))

def main():
    db = sql.connect(DB_PATH)

    t = time.time()

    db.create_function("REGEXP", 2, regexp)

    """
    for a in sys.argv[1:]:
        n = 0
        for sentence in reconstruct_sentences(db, a):
            n += 1
            if n % 1000 == 0:
                print(n, time.time() - t)
            if ENABLE_PRINT:
                print(n, sentence)
    """

    #db.execute(".load /usr/lib/sqlite3/pcre.so")

    n = 0
    for sentence in reconstruct_sentences_regexp(db, sys.argv[1:]):
        n += 1
        if n % 1000 == 0:
            print(n, time.time() - t)
        if ENABLE_PRINT:
            print(n, sentence)


    db.close()
    return 0

if __name__ == "__main__":
    exit(main())
