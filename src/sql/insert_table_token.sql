INSERT OR IGNORE INTO t_token (
	fk_sentence,
	id_raw,
	fk_form,
	fk_lemma,
	fk_upos,
	fk_xpos,
	fk_feats,
	head,
	fk_deprel,
	deps,
	misc,
	mwe,
	fk_lang
-- ) VALUES (
-- 	(SELECT rowid FROM t_sentence WHERE filename=?1 AND sent_id=?2), -- fk_sentence
-- 	?3, -- id_raw
-- 	(SELECT rowid FROM t_form WHERE form=?4), -- fk_form
-- 	(SELECT rowid FROM t_lemma WHERE lemma=?5), -- fk_lemma
-- 	(SELECT rowid FROM t_upos WHERE upos=?6), -- fk_upos
-- 	(SELECT rowid FROM t_xpos WHERE xpos=?7), -- fk_xpos
-- 	(SELECT rowid FROM t_feats WHERE feats=?8), -- fk_feats
-- 	?9, -- head
-- 	(SELECT rowid FROM t_deprel WHERE deprel=?10), -- fk_deprel
-- 	?11, -- deps
-- 	?12, -- misc
-- 	?13  -- mwe
-- );
--) SELECT	t_sentence.rowid,
--		?3,
--		t_form.rowid,
--		t_lemma.rowid,
--		t_upos.rowid,
--		t_xpos.rowid,
--		t_feats.rowid,
--		?9,
--		t_deprel.rowid,
--		?11,
--		?12,
--		?13
--FROM (
--	(SELECT rowid FROM t_sentence WHERE filename=?1 AND sent_id=?2) JOIN
-- 	(SELECT rowid FROM t_form WHERE form=?4)			JOIN -- fk_form
-- 	(SELECT rowid FROM t_lemma WHERE lemma=?5)			JOIN -- fk_lemma
-- 	(SELECT rowid FROM t_upos WHERE upos=?6)			JOIN -- fk_upos
-- 	(SELECT rowid FROM t_xpos WHERE xpos=?7)			JOIN -- fk_xpos
-- 	(SELECT rowid FROM t_feats WHERE feats=?8)			     -- fk_feats
--									ON 1=1
--);
) VALUES (
	?1,
	?2,
	?3,
	?4,
	?5,
	?6,
	?7,
	?8,
	?9,
	?10,
	?11,
	?12,
	?13
);
