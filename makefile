#
#      DiversGraph - Graphs to measure diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

#CC = gcc
CC = clang
CFLAGS_PEDANTIC = -std=c99 -Wall -Wextra -Werror -Wformat -Wformat-security -Wconversion -pedantic
CFLAGS_STANDARD = -std=c99 -Wall -Wextra -Wformat -Wformat-security
CFLAGS_OPTIM = -std=c99 -Wall -Wextra -Wformat -Wformat-security
CFLAGS_DEBUG = -g3 -std=c99 -Wall -Wextra -Wformat -Wformat-security
CFLAGS_TEST = -std=c99 -Wall -Wextra -Wformat -Wformat-security
CFLAGS_COVERAGE = -std=c99 -Wall -Wextra -Wformat -Wformat-security --coverage
CFLAGS_PROFILING = -std=c99 -Wall -Wextra -Wformat -Wformat-security -pg -g
WG4_DIVERSUTILS = $(HOME)/Documents/thesis/other_repos/WG4/diversutils
CPPFLAGS = -Isrc/include -I$(WG4_DIVERSUTILS)/src/include -I$(HOME)/.local/conda/include -L$(HOME)/.local/conda/lib 
CPPFLAGS_TEST = -Isrc/include -Itest/include -I$(HOME)/Documents/thesis/workrepo/src/include 
OPT_LEVEL = -O2
OPT_LEVEL_OPTIM = -O3
OPT_LEVEL_TEST = -O2
OPT_LEVEL_DEBUG = -O0
LINKER_FLAGS = -lm -pthread

# ----------------
# MACRO PARAMETERS
# ----------------

ENABLE_EARLY_TOKEN_INDEX_GENERATION = 0
ENABLE_LATE_TOKEN_INDEX_GENERATION = 1
FILTER_OUT_FORMS = 0
EARLY_STANDARD_EDGE_COMPUTATION = 0
LATE_STANDARD_EDGE_COMPUTATION = 1
ENABLE_EDGE_CONSTRUCTION = 1

ENABLE_CAP_NUM_SENTENCES = 1
CAP_NUM_SENTENCES = 1000000

ENABLE_SHORT_TRANSACTIONS = 0
SHORT_TRANSACTION_SIZE = 1000

ENABLE_FTS5 = 0

#DB_PATH = data/target/main.db
#DB_PATH = data/target/main_DE.db
DB_PATH = data/target/main_EL.db
#DB_PATH = data/target/main_EU.db
#DB_PATH = data/target/main_FR.db
#DB_PATH = data/target/main_GA.db
#DB_PATH = data/target/main_HE.db
#DB_PATH = data/target/main_HI.db
#DB_PATH = data/target/main_IT.db
#DB_PATH = data/target/main_PL.db
#DB_PATH = data/target/main_PT.db
#DB_PATH = data/target/main_RO.db
#DB_PATH = data/target/main_SV.db
#DB_PATH = data/target/main_TR.db
#DB_PATH = data/target/main_ZH.db

CPP_MACRO = \
	-DENABLE_EARLY_TOKEN_INDEX_GENERATION=$(ENABLE_EARLY_TOKEN_INDEX_GENERATION) \
	-DENABLE_LATE_TOKEN_INDEX_GENERATION=$(ENABLE_LATE_TOKEN_INDEX_GENERATION) \
	-DFILTER_OUT_FORMS=$(FILTER_OUT_FORMS) \
	-DEARLY_STANDARD_EDGE_COMPUTATION=$(EARLY_STANDARD_EDGE_COMPUTATION) \
	-DLATE_STANDARD_EDGE_COMPUTATION=$(LATE_STANDARD_EDGE_COMPUTATION) \
	-DENABLE_EDGE_CONSTRUCTION=$(ENABLE_EDGE_CONSTRUCTION) \
	-DENABLE_CAP_NUM_SENTENCES=$(ENABLE_CAP_NUM_SENTENCES) \
	-DCAP_NUM_SENTENCES=$(CAP_NUM_SENTENCES) \
	-DENABLE_SHORT_TRANSACTIONS=$(ENABLE_SHORT_TRANSACTIONS) \
	-DSHORT_TRANSACTION_SIZE=$(SHORT_TRANSACTION_SIZE) \
	-DENABLE_FTS5=$(ENABLE_FTS5) \
	-DDB_PATH=\"$(DB_PATH)\" \


compile_standard:
	$(CC) --version
	mkdir -p bin/target
	$(CC) $(CFLAGS_STANDARD) $(CPPFLAGS) $(OPT_LEVEL) $(CPP_MACRO) -o bin/target/main src/target/main.c $(LINKER_FLAGS)

compile_optim:
	$(CC) --version
	mkdir -p bin/target
	$(CC) $(CFLAGS_OPTIM) $(CPPFLAGS) $(OPT_LEVEL_OPTIM) $(CPP_MACRO) -o bin/target/main src/target/main.c $(LINKER_FLAGS)

compile_debug:
	$(CC) --version
	mkdir -p bin/target
	$(CC) $(CFLAGS_DEBUG) $(CPPFLAGS) $(OPT_LEVEL_DEBUG) $(CPP_MACRO) -o bin/target/main src/target/main.c $(LINKER_FLAGS)

compile_pedantic:
	$(CC) --version
	mkdir -p bin/target
	$(CC) $(CFLAGS_PEDANTIC) $(CPPFLAGS) $(OPT_LEVEL) $(CPP_MACRO) -o bin/target/main src/target/main.c $(LINKER_FLAGS)

compile_db_build:
	$(CC) --version
	mkdir -p bin/target
	mkdir -p data/target
	$(CC) -g3 $(CFLAGS_STANDARD) $(CPPFLAGS) -march=native $(OPT_LEVEL) $(CPP_MACRO) -o bin/target/db_build src/target/db_build.c $(LINKER_FLAGS) -lsqlite3

analyse_db_build:
	$(CC) --version
	$(CC) $(CFLAGS_STANDARD) $(CPPFLAGS) -O2 --analyze $(CPP_MACRO) src/target/db_build.c

compile_db_measure:
	$(CC) --version
	mkdir -p bin/target
	mkdir -p data/target
	$(CC) -g3 $(CFLAGS_STANDARD) $(CPPFLAGS) -march=native $(OPT_LEVEL) $(CPP_MACRO) -o bin/target/db_measure src/target/db_measure.c $(LINKER_FLAGS) -lsqlite3

analyse_db_measure:
	$(CC) --version
	$(CC) -g3 $(CFLAGS_STANDARD) $(CPPFLAGS) $(OPT_LEVEL) --analyze src/target/db_measure.c
