/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DB_INTERFACE_H
#define DB_INTERFACE_H

#include <stdint.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <regex.h>
#include <sqlite3.h>

#include "cupt/parser.h"
#include "gengraph.h"
#include "distributions.h"

// #define PATH_SQL_CREATE_TABLE_FORM "src/sql/create_table_form.sql"

const char* const PATH_SQL_CREATE_TABLE_LANG = "src/sql/create_table_lang.sql";
const char* const PATH_SQL_CREATE_TABLE_FORM = "src/sql/create_table_form.sql";
const char* const PATH_SQL_CREATE_TABLE_LEMMA = "src/sql/create_table_lemma.sql";
const char* const PATH_SQL_CREATE_TABLE_FEATS = "src/sql/create_table_feats.sql";
const char* const PATH_SQL_CREATE_TABLE_DEPREL = "src/sql/create_table_deprel.sql";
const char* const PATH_SQL_CREATE_TABLE_UPOS = "src/sql/create_table_upos.sql";
const char* const PATH_SQL_CREATE_TABLE_XPOS = "src/sql/create_table_xpos.sql";
const char* const PATH_SQL_CREATE_TABLE_MWE_CANONICAL = "src/sql/create_table_mwe_canonical.sql";
const char* const PATH_SQL_CREATE_TABLE_MWE_NON_CANONICAL = "src/sql/create_table_mwe_non_canonical.sql";
const char* const PATH_SQL_CREATE_TABLE_TOKEN = "src/sql/create_table_token.sql";
const char* const PATH_SQL_CREATE_TABLE_SENTENCE = "src/sql/create_table_sentence.sql";
const char* const PATH_SQL_CREATE_TABLE_SENTENCE_FTS5 = "src/sql/create_table_sentence_fts5.sql";
const char* const PATH_SQL_CREATE_TABLE_FILENAME = "src/sql/create_table_filename.sql";
const char* const PATH_SQL_CREATE_TABLE_EDGE = "src/sql/create_table_edge.sql";
const char* const PATH_SQL_CREATE_TABLE_EDGE_META = "src/sql/create_table_edge_meta.sql";

const char* const PATH_SQL_CREATE_INDEX_LANG = "src/sql/create_index_lang.sql";
const char* const PATH_SQL_CREATE_INDEX_FORM = "src/sql/create_index_form.sql";
const char* const PATH_SQL_CREATE_INDEX_LEMMA = "src/sql/create_index_lemma.sql";
const char* const PATH_SQL_CREATE_INDEX_FEATS = "src/sql/create_index_feats.sql";
const char* const PATH_SQL_CREATE_INDEX_DEPREL = "src/sql/create_index_deprel.sql";
const char* const PATH_SQL_CREATE_INDEX_UPOS = "src/sql/create_index_upos.sql";
const char* const PATH_SQL_CREATE_INDEX_XPOS = "src/sql/create_index_xpos.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN = "src/sql/create_index_token.sql";
const char* const PATH_SQL_CREATE_INDEX_EDGE = "src/sql/create_index_edge.sql";
const char* const PATH_SQL_CREATE_INDEX_EDGE_META = "src/sql/create_index_edge_meta.sql";

const char* const PATH_SQL_CREATE_INDEX_TOKEN_FORM = "src/sql/create_index_token_form.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_LEMMA = "src/sql/create_index_token_lemma.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_FEATS = "src/sql/create_index_token_feats.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_DEPREL = "src/sql/create_index_token_deprel.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_UPOS = "src/sql/create_index_token_upos.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_XPOS = "src/sql/create_index_token_xpos.sql";
const char* const PATH_SQL_CREATE_INDEX_TOKEN_LANG = "src/sql/create_index_token_lang.sql";

const char* const PATH_SQL_SELECT_LANG_ROWID = "src/sql/select_lang_rowid.sql";
const char* const PATH_SQL_SELECT_FORM_ROWID = "src/sql/select_form_rowid.sql";
const char* const PATH_SQL_SELECT_LEMMA_ROWID = "src/sql/select_lemma_rowid.sql";
const char* const PATH_SQL_SELECT_FEATS_ROWID = "src/sql/select_feats_rowid.sql";
const char* const PATH_SQL_SELECT_DEPREL_ROWID = "src/sql/select_deprel_rowid.sql";
const char* const PATH_SQL_SELECT_UPOS_ROWID = "src/sql/select_upos_rowid.sql";
const char* const PATH_SQL_SELECT_XPOS_ROWID = "src/sql/select_xpos_rowid.sql";
const char* const PATH_SQL_SELECT_SENTENCE_ROWID = "src/sql/select_sentence_rowid.sql";
const char* const PATH_SQL_SELECT_FILENAME_ROWID = "src/sql/select_filename_rowid.sql";

const char* const PATH_SQL_INSERT_TABLE_LANG = "src/sql/insert_table_lang.sql";
const char* const PATH_SQL_INSERT_TABLE_FORM = "src/sql/insert_table_form.sql";
const char* const PATH_SQL_INSERT_TABLE_LEMMA = "src/sql/insert_table_lemma.sql";
const char* const PATH_SQL_INSERT_TABLE_FEATS = "src/sql/insert_table_feats.sql";
const char* const PATH_SQL_INSERT_TABLE_DEPREL = "src/sql/insert_table_deprel.sql";
const char* const PATH_SQL_INSERT_TABLE_UPOS = "src/sql/insert_table_upos.sql";
const char* const PATH_SQL_INSERT_TABLE_XPOS = "src/sql/insert_table_xpos.sql";
const char* const PATH_SQL_INSERT_TABLE_MWE_CANONICAL = "src/sql/insert_table_mwe_canonical.sql";
const char* const PATH_SQL_INSERT_TABLE_MWE_NON_CANONICAL = "src/sql/insert_table_mwe_non_canonical.sql";
const char* const PATH_SQL_INSERT_TABLE_TOKEN = "src/sql/insert_table_token.sql";
const char* const PATH_SQL_INSERT_TABLE_SENTENCE = "src/sql/insert_table_sentence.sql";
const char* const PATH_SQL_INSERT_TABLE_SENTENCE_FTS5 = "src/sql/insert_table_sentence_fts5.sql";
const char* const PATH_SQL_INSERT_TABLE_FILENAME = "src/sql/insert_table_filename.sql";
const char* const PATH_SQL_INSERT_TABLE_EDGE = "src/sql/insert_table_edge.sql";
const char* const PATH_SQL_INSERT_TABLE_EDGE_META = "src/sql/insert_table_edge_meta.sql";

const char* const PATH_SQL_SELECT_FORM = "src/sql/select_form.sql";

const char* const PATH_SQL_METAPATH_INIT_WEIGHTED_FORM = "src/sql/metapath_init_weighted_form.sql";
const char* const PATH_SQL_METAPATH_FORM_TO_LEMMA = "src/sql/metapath_form_to_lemma.sql";

// const char* const PATH_SQL_MAX_ROWID = "src/sql/max_rowid.sql";
const char* const PATH_SQL_MAX_ROWID_LANG = "src/sql/max_rowid_lang.sql";
const char* const PATH_SQL_MAX_ROWID_FORM = "src/sql/max_rowid_form.sql";
const char* const PATH_SQL_MAX_ROWID_LEMMA = "src/sql/max_rowid_lemma.sql";
const char* const PATH_SQL_MAX_ROWID_FEATS = "src/sql/max_rowid_feats.sql";
const char* const PATH_SQL_MAX_ROWID_DEPREL = "src/sql/max_rowid_deprel.sql";
const char* const PATH_SQL_MAX_ROWID_UPOS = "src/sql/max_rowid_upos.sql";
const char* const PATH_SQL_MAX_ROWID_XPOS = "src/sql/max_rowid_xpos.sql";
const char* const PATH_SQL_MAX_ROWID_MWE_CANONICAL = "src/sql/max_rowid_mwe_canonical.sql";
const char* const PATH_SQL_MAX_ROWID_MWE_NON_CANONICAL = "src/sql/max_rowid_mwe_non_canonical.sql";

const char* const PATH_SQL_EDGE_PASS_FORM_TO_LANG = "src/sql/edge_pass_form_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_LEMMA = "src/sql/edge_pass_form_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_UPOS = "src/sql/edge_pass_form_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_XPOS = "src/sql/edge_pass_form_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_FEATS = "src/sql/edge_pass_form_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_DEPREL = "src/sql/edge_pass_form_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_MWE_CANONICAL = "src/sql/edge_pass_form_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_FORM_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_form_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_LANG = "src/sql/edge_pass_lemma_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_FORM = "src/sql/edge_pass_lemma_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_UPOS = "src/sql/edge_pass_lemma_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_XPOS = "src/sql/edge_pass_lemma_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_FEATS = "src/sql/edge_pass_lemma_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_DEPREL = "src/sql/edge_pass_lemma_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_MWE_CANONICAL = "src/sql/edge_pass_lemma_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_LEMMA_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_lemma_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_LANG = "src/sql/edge_pass_upos_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_FORM = "src/sql/edge_pass_upos_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_LEMMA = "src/sql/edge_pass_upos_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_XPOS = "src/sql/edge_pass_upos_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_FEATS = "src/sql/edge_pass_upos_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_DEPREL = "src/sql/edge_pass_upos_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_MWE_CANONICAL = "src/sql/edge_pass_upos_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_UPOS_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_upos_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_LANG = "src/sql/edge_pass_xpos_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_FORM = "src/sql/edge_pass_xpos_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_LEMMA = "src/sql/edge_pass_xpos_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_UPOS = "src/sql/edge_pass_xpos_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_FEATS = "src/sql/edge_pass_xpos_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_DEPREL = "src/sql/edge_pass_xpos_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_MWE_CANONICAL = "src/sql/edge_pass_xpos_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_XPOS_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_xpos_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_LANG = "src/sql/edge_pass_feats_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_FORM = "src/sql/edge_pass_feats_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_LEMMA = "src/sql/edge_pass_feats_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_UPOS = "src/sql/edge_pass_feats_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_XPOS = "src/sql/edge_pass_feats_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_DEPREL = "src/sql/edge_pass_feats_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_MWE_CANONICAL = "src/sql/edge_pass_feats_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_FEATS_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_feats_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_LANG = "src/sql/edge_pass_deprel_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_FORM = "src/sql/edge_pass_deprel_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_LEMMA = "src/sql/edge_pass_deprel_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_UPOS = "src/sql/edge_pass_deprel_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_XPOS = "src/sql/edge_pass_deprel_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_FEATS = "src/sql/edge_pass_deprel_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_MWE_CANONICAL = "src/sql/edge_pass_deprel_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_DEPREL_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_deprel_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_LANG = "src/sql/edge_pass_mwe_canonical_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_FORM = "src/sql/edge_pass_mwe_canonical_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_LEMMA = "src/sql/edge_pass_mwe_canonical_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_UPOS = "src/sql/edge_pass_mwe_canonical_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_XPOS = "src/sql/edge_pass_mwe_canonical_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_FEATS = "src/sql/edge_pass_mwe_canonical_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_DEPREL = "src/sql/edge_pass_mwe_canonical_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_mwe_canonical_to_mwe_non_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_LANG = "src/sql/edge_pass_mwe_non_canonical_to_lang.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_FORM = "src/sql/edge_pass_mwe_non_canonical_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_LEMMA = "src/sql/edge_pass_mwe_non_canonical_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_UPOS = "src/sql/edge_pass_mwe_non_canonical_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_XPOS = "src/sql/edge_pass_mwe_non_canonical_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_FEATS = "src/sql/edge_pass_mwe_non_canonical_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_DEPREL = "src/sql/edge_pass_mwe_non_canonical_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_MWE_CANONICAL = "src/sql/edge_pass_mwe_non_canonical_to_mwe_canonical.sql";

const char* const PATH_SQL_EDGE_PASS_LANG_TO_FORM = "src/sql/edge_pass_lang_to_form.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_LEMMA = "src/sql/edge_pass_lang_to_lemma.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_UPOS = "src/sql/edge_pass_lang_to_upos.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_XPOS = "src/sql/edge_pass_lang_to_xpos.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_FEATS = "src/sql/edge_pass_lang_to_feats.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_DEPREL = "src/sql/edge_pass_lang_to_deprel.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_MWE_CANONICAL = "src/sql/edge_pass_lang_to_mwe_canonical.sql";
const char* const PATH_SQL_EDGE_PASS_LANG_TO_MWE_NON_CANONICAL = "src/sql/edge_pass_lang_to_mwe_non_canonical.sql";


// const char* const PATH_SQL_INIT_VECTOR_FORM = "src/sql/edge_pass_form_to_lemma.sql"; // normal
const char* const PATH_SQL_INIT_VECTOR_LANG = "src/sql/init_vector_lang.sql";
const char* const PATH_SQL_INIT_VECTOR_FORM = "src/sql/init_vector_form.sql";
const char* const PATH_SQL_INIT_VECTOR_LEMMA = "src/sql/init_vector_lemma.sql";
const char* const PATH_SQL_INIT_VECTOR_FEATS = "src/sql/init_vector_feats.sql";
const char* const PATH_SQL_INIT_VECTOR_DEPREL = "src/sql/init_vector_deprel.sql";
const char* const PATH_SQL_INIT_VECTOR_UPOS = "src/sql/init_vector_upos.sql";
const char* const PATH_SQL_INIT_VECTOR_XPOS = "src/sql/init_vector_xpos.sql";
const char* const PATH_SQL_INIT_VECTOR_MWE_CANONICAL = "src/sql/init_vector_mwe_canonical.sql";
const char* const PATH_SQL_INIT_VECTOR_MWE_NON_CANONICAL = "src/sql/init_vector_mwe_non_canonical.sql";

const char* const PATH_SQL_RECOMPUTE_STANDARD_EDGES = "src/sql/recompute_standard_edges.sql";

const char* const PATH_SQL_LIST_LANG = "src/sql/list_lang.sql";
const char* const PATH_SQL_LIST_FORM = "src/sql/list_form.sql";
const char* const PATH_SQL_LIST_LEMMA = "src/sql/list_lemma.sql";
const char* const PATH_SQL_LIST_FEATS = "src/sql/list_feats.sql";
const char* const PATH_SQL_LIST_DEPREL = "src/sql/list_deprel.sql";
const char* const PATH_SQL_LIST_UPOS = "src/sql/list_upos.sql";
const char* const PATH_SQL_LIST_XPOS = "src/sql/list_xpos.sql";
const char* const PATH_SQL_LIST_MWE_CANONICAL = "src/sql/list_mwe_canonical.sql";
const char* const PATH_SQL_LIST_MWE_NON_CANONICAL = "src/sql/list_mwe_non_canonical.sql";

#ifndef FILTER_OUT_FORMS
#define FILTER_OUT_FORMS 0
#endif

#ifndef EARLY_STANDARD_EDGE_COMPUTATION
#define EARLY_STANDARD_EDGE_COMPUTATION 0
#endif

#ifndef LATE_STANDARD_EDGE_COMPUTATION
#define LATE_STANDARD_EDGE_COMPUTATION 1
#endif

#ifndef ENABLE_CAP_NUM_SENTENCES
#define ENABLE_CAP_NUM_SENTENCES 0
#endif

#ifndef CAP_NUM_SENTENCES
#define CAP_NUM_SENTENCES 1000
#endif

#ifndef ENABLE_EARLY_TOKEN_INDEX_GENERATION
#define ENABLE_EARLY_TOKEN_INDEX_GENERATION 1
#endif

#ifndef ENABLE_SHORT_TRANSACTIONS
#define ENABLE_SHORT_TRANSACTIONS 0
#endif

#ifndef SHORT_TRANSACTION_SIZE
#define SHORT_TRANSACTION_SIZE 1000
#endif

#ifndef ENABLE_EDGE_CONSTRUCTION
#define ENABLE_EDGE_CONSTRUCTION 0
#endif

#ifndef ENABLE_FTS5
#define ENABLE_FTS5 1
#endif

int32_t strcmp_wrapper(const void* const a, const void* const b){
	return strcmp((char*) a, (char*) b);
}

int32_t db_execute_statement_from_file(sqlite3* const db, const char* const path){
	const uint32_t bfr_size = 4096;
	char bfr[bfr_size];
	FILE* f;
	char* sql_err = NULL;

	f = fopen(path, "r");

	if(f == NULL){
		fprintf(stderr, "Failed to open %s\n", path);
		return 1;
	}

	memset(bfr, '\0', bfr_size * sizeof(char));
	fread(bfr, sizeof(char), bfr_size - 1, f);

	fclose(f);

	if(sqlite3_exec(db, bfr, NULL, 0, &sql_err) != SQLITE_OK){
		fprintf(stderr, "Failed to execute query from %s; error: %s\n", path, sql_err);
		return 1;
	}
	
	return 0;
}

int32_t db_prepare_statement(sqlite3* const db, sqlite3_stmt** const stmt, const char* const path){
	const uint32_t bfr_size = 4096;
	char bfr[bfr_size];
	FILE* f;

	f = fopen(path, "r");

	if(f == NULL){
		fprintf(stderr, "Failed to open %s\n", path);
		return 1;
	}

	memset(bfr, '\0', bfr_size * sizeof(char));
	fread(bfr, sizeof(char), bfr_size - 1, f);

	fclose(f);

	if(sqlite3_prepare_v2(db, bfr, bfr_size, stmt, NULL) != SQLITE_OK){
		fprintf(stderr, "Failed to prepare query from %s\n", path);
		return 1;
	}
	
	return 0;
}


// --------

int32_t try_statement(sqlite3_stmt* const stmt, uint8_t* const can_continue){
	int32_t looping = 1;
	while(looping){
		int32_t res = sqlite3_step(stmt);
		switch(res){
			case SQLITE_DONE:
				looping = 0;
				if(can_continue != NULL){
					(*can_continue) = 0;
				}
				break;
			case SQLITE_BUSY:
				break;
			case SQLITE_ROW:
				looping = 0;
				if(can_continue != NULL){
					(*can_continue) = 1;
				}
				break;
			default:
				fprintf(stderr, "sqlite3_step failed; res: %i\n", res);
				return 1;
		}
	}

	return 0;
}

int32_t db_insert_table_edge_meta(sqlite3_stmt* const stmt, const char* const a, const char* const b, const char* const c){
	if(sqlite3_bind_text(stmt, 1, a, strlen(a), NULL) != 0){goto bind_text_fail;} // "form_to_lemma"
	if(sqlite3_bind_text(stmt, 2, b, strlen(b), NULL) != 0){goto bind_text_fail;} // "form"
	if(sqlite3_bind_text(stmt, 3, c, strlen(c), NULL) != 0){goto bind_text_fail;} // "lemma"
	if(try_statement(stmt, NULL) != 0){perror("failed to call try_statement\n"); return 1;}
	sqlite3_reset(stmt);
	return 0;

	bind_text_fail:
	perror("failed to call sqlite3_bind_text\n");
	return 1;
}

int32_t db_insert_table_edge(sqlite3_stmt* const stmt, const char* const src, const char* const edge_type, const char* const tgt){
	if(sqlite3_bind_text(stmt, 1, src, strlen(src), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text\n"); return 1;}
	if(sqlite3_bind_text(stmt, 2, edge_type, strlen(edge_type), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text\n"); return 1;}
	if(sqlite3_bind_text(stmt, 3, tgt, strlen(tgt), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text\n"); return 1;}
	if(try_statement(stmt, NULL) != 0){perror("Failed to run stmt_insert_table_edge\n"); return 1;}
	sqlite3_reset(stmt);
	return 0;
}

int32_t db_create_token_indices(sqlite3* const db){
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_FORM) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_LEMMA) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_FEATS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_DEPREL) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_UPOS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN_XPOS) != 0){return 1;}
	return 0;
}

/*
int32_t db_custom_regexp(const char* const expr_, const char* const item_){
	regex_t reg;

	if(regcomp(&reg, expr_, REG_EXTENDED | REG_NOSUB) != 0){
		fprintf(stderr, "Failed to compile regex %s\n", expr_);
		exit(1);
	}

	return regexec(&reg, item_, 0, NULL, 0);
}
*/

regex_t sql_custom_regexp;
char* sql_custom_regexp_pattern = NULL;

void db_custom_regexp(sqlite3_context* const context, const int32_t argc, sqlite3_value** const argv){
	if(argc != 2){
		fprintf(stderr, "REGEXP should receive 2 arguments, not %i\n", argc);
		exit(1);
	}

	const char* const expr_ = (char*) sqlite3_value_text(argv[0]);
	const char* const item_ = (char*) sqlite3_value_text(argv[1]);

	// printf("found %s and %s\n", expr_, item_);

	if(sql_custom_regexp_pattern == NULL || strcmp(expr_, sql_custom_regexp_pattern) != 0){
		// printf("compiling %s\n", expr_);

		if(sql_custom_regexp_pattern != NULL){
			regfree(&sql_custom_regexp);
		}

		if(regcomp(&sql_custom_regexp, expr_, REG_EXTENDED | REG_NOSUB) != 0){
			fprintf(stderr, "Failed to compile regex %s\n", expr_);
			exit(1);
		}
		size_t len_expr_ = strlen(expr_);
		size_t alloc_size = len_expr_ + 1;
		if(sql_custom_regexp_pattern == NULL){
			sql_custom_regexp_pattern = malloc(alloc_size);
		} else {
			sql_custom_regexp_pattern = realloc(sql_custom_regexp_pattern, alloc_size);
		}
		if(sql_custom_regexp_pattern == NULL){
			perror("malloc or realloc failed\n");
			exit(1);
		}
		memcpy(sql_custom_regexp_pattern, expr_, len_expr_);
		sql_custom_regexp_pattern[len_expr_] = '\0';
	}

	// printf("running %s\n", item_);

	sqlite3_result_int(context, !(regexec(&sql_custom_regexp, item_, 0, NULL, 0)));
}

void db_custom_regexp_cleanup(void* argv){
	if(argv != NULL){perror("db_custom_regexp_cleanup should not receive arguments, argv is there for function signature\n");}
	// printf("Freeing after custom regexp\n");
	if(sql_custom_regexp_pattern != NULL){
		free(sql_custom_regexp_pattern);
		regfree(&sql_custom_regexp);
	}
}

int32_t db_create_function_regexp(sqlite3* const db){
	if(sqlite3_create_function_v2(db, "REGEXP", 2, SQLITE_UTF8 | SQLITE_DETERMINISTIC, NULL, db_custom_regexp, NULL, NULL, db_custom_regexp_cleanup) != SQLITE_OK){
		perror("failed to call sqlite3_create_function\n");
		return 1;
	}
	return 0;
}


int32_t db_prepare(sqlite3* const db){
	sqlite3_stmt* stmt;

	if(sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL) != SQLITE_OK){perror("failed to begin transaction\n"); return 1;}

	// if(sqlite3_exec(db, "PRAGMA automatic_index = 0;", NULL, NULL, NULL) != SQLITE_OK){perror("failed to disable autoindex\n"); return 1;}
	
	if(db_create_function_regexp(db) != 0){return 1;}

	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_LANG) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_FORM) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_LEMMA) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_FEATS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_DEPREL) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_UPOS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_XPOS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_MWE_CANONICAL) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_MWE_NON_CANONICAL) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_FILENAME) != 0){return 1;}
	if(ENABLE_FTS5){
		if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_SENTENCE_FTS5) != 0){return 1;}
	} else {
		if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_SENTENCE) != 0){return 1;}
	}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_TOKEN) != 0){return 1;}

	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_EDGE_META) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_TABLE_EDGE) != 0){return 1;}

	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_LANG) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_FORM) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_LEMMA) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_FEATS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_DEPREL) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_UPOS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_XPOS) != 0){return 1;}
	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_LANG) != 0){return 1;}
	// if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_TOKEN) != 0){return 1;}

	if(db_execute_statement_from_file(db, PATH_SQL_CREATE_INDEX_EDGE) != 0){return 1;}

	if(ENABLE_EARLY_TOKEN_INDEX_GENERATION){
		if(db_create_token_indices(db) != 0){
			perror("failed to call db_create_token_indices\n");
			return 1;
		}
	}

	if(ENABLE_EDGE_CONSTRUCTION){
		if(db_prepare_statement(db, &stmt, PATH_SQL_INSERT_TABLE_EDGE_META) != 0){
			perror("failed to call db_prepare_statement\n");
			return 1;
		}
		
		if(db_insert_table_edge_meta(stmt, "form_to_lang", "form", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_lemma", "form", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_feats", "form", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_deprel", "form", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_upos", "form", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_xpos", "form", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_mwe_canonical", "form", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "form_to_mwe_non_canonical", "form", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_lang", "lemma", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_form", "lemma", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_feats", "lemma", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_deprel", "lemma", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_upos", "lemma", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_xpos", "lemma", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_mwe_canonical", "lemma", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lemma_to_mwe_non_canonical", "lemma", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_lang", "feats", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_form", "feats", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_lemma", "feats", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_deprel", "feats", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_upos", "feats", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_xpos", "feats", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_mwe_canonical", "feats", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "feats_to_mwe_non_canonical", "feats", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_lang", "deprel", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_form", "deprel", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_lemma", "deprel", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_feats", "deprel", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_upos", "deprel", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_xpos", "deprel", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_mwe_canonical", "deprel", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "deprel_to_mwe_non_canonical", "deprel", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_lang", "upos", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_form", "upos", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_lemma", "upos", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_feats", "upos", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_deprel", "upos", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_xpos", "upos", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_mwe_canonical", "upos", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "upos_to_mwe_non_canonical", "upos", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_lang", "xpos", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_form", "xpos", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_lemma", "xpos", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_feats", "xpos", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_deprel", "xpos", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_mwe_canonical", "xpos", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_mwe_non_canonical", "xpos", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "xpos_to_upos", "xpos", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_form", "lang", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_lemma", "lang", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_feats", "lang", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_deprel", "lang", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_upos", "lang", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_xpos", "lang", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_mwe_canonical", "lang", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "lang_to_mwe_non_canonical", "lang", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_lang", "mwe_canonical", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_lemma", "mwe_canonical", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_form", "mwe_canonical", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_feats", "mwe_canonical", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_deprel", "mwe_canonical", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_upos", "mwe_canonical", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_xpos", "mwe_canonical", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_canonical_to_mwe_non_canonical", "mwe_canonical", "mwe_non_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_lang", "mwe_non_canonical", "lang") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_lemma", "mwe_non_canonical", "lemma") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_form", "mwe_non_canonical", "form") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_feats", "mwe_non_canonical", "feats") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_deprel", "mwe_non_canonical", "deprel") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_upos", "mwe_non_canonical", "upos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_xpos", "mwe_non_canonical", "xpos") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		if(db_insert_table_edge_meta(stmt, "mwe_non_canonical_to_mwe_canonical", "mwe_non_canonical", "mwe_canonical") != 0){perror("failed to call db_insert_table_edge_meta\n"); return 1;}
		sqlite3_finalize(stmt);	
	}

	if(sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL) != SQLITE_OK){perror("failed to end transaction\n"); return 1;}

	return 0;
}

int32_t db_recompute_standard_edges(sqlite3* const db){
	sqlite3_stmt *stmt_insert_table_edge_meta;
	size_t i, j;
	time_t start, end;
	const int32_t bfr_size = 64;
	const int32_t big_bfr_size = 4096;
	char bfr[bfr_size];
	char bfr_a1[bfr_size];
	char bfr_a2[bfr_size];
	char bfr_b1[bfr_size];
	char bfr_b2[bfr_size];
	char big_bfr[big_bfr_size];

	const char pattern[] = "INSERT OR REPLACE INTO t_edge (src, fk_edge_type, tgt, passes) SELECT \
        (SELECT %s FROM %s WHERE rowid=fk_%s), (SELECT rowid FROM t_edge_meta WHERE edge_type=\"%s\"), (SELECT %s FROM %s WHERE rowid=fk_%s), COUNT(*) AS n \
        FROM \
        t_token \
        GROUP BY \
        fk_%s, fk_%s \
;"; 

	const char* columns[] = {
		"form",
		"lemma",
		"feats",
		"deprel",
		"upos",
		"xpos",
		"lang",
	};

	const size_t n = sizeof(columns) / sizeof(columns[0]);

	/* // DO NOT REMOVE
	for(i = 0 ; i < n ; i++){
		const char local_pattern[] = "src/sql/create_index_token_%s.sql";
		const uint32_t local_bfr_size = 256;
		char local_bfr[local_bfr_size];
		sqlite3_stmt* local_stmt;
		memset(local_bfr, '\0', local_bfr_size);
		snprintf(local_bfr, local_bfr_size, local_pattern, columns[i]);
		printf("running %s ... ", local_bfr);
		start = time(NULL);
		if(db_prepare_statement(db, &local_stmt, local_bfr) != 0){
			fprintf(stderr, "Failed to call db_prepare_statement for %s\n", local_bfr);
			return 1;
		}
		if(try_statement(local_stmt, NULL) != 0){
			fprintf(stderr, "Failed to call try_statement for %s\n", local_bfr);
			sqlite3_finalize(local_stmt);
			return 1;	
		}
		end = time(NULL);
		printf("%lis\n", end - start);
		sqlite3_finalize(local_stmt);
	}
	*/

	if(db_prepare_statement(db, &stmt_insert_table_edge_meta, PATH_SQL_INSERT_TABLE_EDGE_META) != 0){perror("Failed to call db_prepare_statement\n"); return 1;}

	if(sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL) != SQLITE_OK){perror("failed to begin transaction\n"); return 1;}

	for(i = 0 ; i < n ; i++){
		for(j = 0 ; j < n ; j++){
			if(i == j){continue;}
			start = time(NULL);
			memset(bfr, '\0', bfr_size * sizeof(char));
			memset(bfr_a1, '\0', bfr_size * sizeof(char));
			memset(bfr_a2, '\0', bfr_size * sizeof(char));
			memset(bfr_b1, '\0', bfr_size * sizeof(char));
			memset(bfr_b2, '\0', bfr_size * sizeof(char));
			snprintf(bfr, bfr_size, "%s_to_%s", columns[i], columns[j]);
			snprintf(bfr_a1, bfr_size, "%s", columns[i]);
			snprintf(bfr_a2, bfr_size, "t_%s", columns[i]);
			snprintf(bfr_b1, bfr_size, "%s", columns[j]);
			snprintf(bfr_b2, bfr_size, "t_%s", columns[j]);
			printf("%s", bfr);
			/*
			if(sqlite3_bind_text(stmt, 1, columns[i], strlen(columns[i]), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 2, bfr, strlen(bfr), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 3, columns[j], strlen(columns[j]), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 4, bfr_a1, strlen(bfr_a1), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 5, bfr_a2, strlen(bfr_a2), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 6, bfr_b1, strlen(bfr_b1), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt, 7, bfr_b2, strlen(bfr_b2), NULL) != 0){goto failure_sqlite3_bind_text;}
			if(try_statement(stmt, NULL) != 0){perror("Failed to call try_statement\n"); sqlite3_finalize(stmt); return 1;}
			sqlite3_reset(stmt);
			*/

			if(sqlite3_bind_text(stmt_insert_table_edge_meta, 1, bfr, strlen(bfr), NULL) != SQLITE_OK){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt_insert_table_edge_meta, 2, bfr_a1, strlen(bfr_a1), NULL) != SQLITE_OK){goto failure_sqlite3_bind_text;}
			if(sqlite3_bind_text(stmt_insert_table_edge_meta, 3, bfr_b1, strlen(bfr_b1), NULL) != SQLITE_OK){goto failure_sqlite3_bind_text;}
			if(try_statement(stmt_insert_table_edge_meta, NULL) != 0){perror("failed to call try_statement\n"); sqlite3_finalize(stmt_insert_table_edge_meta); return 1;}
			sqlite3_reset(stmt_insert_table_edge_meta);

			snprintf(big_bfr, big_bfr_size, pattern, bfr_a1, bfr_a2, bfr_a1, bfr, bfr_b1, bfr_b2, bfr_b1, bfr_a1, bfr_b1);
			// printf("query: %s\n", big_bfr);
			if(sqlite3_exec(db, big_bfr, NULL, NULL, NULL) != SQLITE_OK){sqlite3_exec(db, "ROLLBACK;", NULL, NULL, NULL); perror("failed to call sqlite3_exec\n");}
			end = time(NULL);
			printf(" -> %lis\n", end - start);
		}
	}

	sqlite3_finalize(stmt_insert_table_edge_meta);

	if(sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL) != SQLITE_OK){perror("failed to end transaction\n"); return 1;}

	return 0;

	failure_sqlite3_bind_text:
	perror("Failed to call sqlite3_bind_text\n");
	sqlite3_finalize(stmt_insert_table_edge_meta);
	return 1;

}

int32_t cupt_to_db_insert(sqlite3* const db, const char* const path, const char* const lang){
	static uint64_t num_added_sentences = 0;

	struct cupt_sentence_iterator csi;
	sqlite3_stmt	*stmt_insert_table_lang,
			*stmt_insert_table_form,
			*stmt_insert_table_lemma,
			*stmt_insert_table_feats,
			*stmt_insert_table_deprel,
			*stmt_insert_table_upos,
			*stmt_insert_table_xpos,
			*stmt_insert_table_token,
			*stmt_insert_table_sentence,
			*stmt_insert_table_filename,
			*stmt_insert_table_edge,
			*stmt_insert_table_mwe_canonical,
			*stmt_insert_table_mwe_non_canonical
			;

	sqlite3_stmt	*stmt_select_rowid_lang,
			*stmt_select_rowid_form,
			*stmt_select_rowid_lemma,
			*stmt_select_rowid_feats,
			*stmt_select_rowid_deprel,
			*stmt_select_rowid_upos,
			*stmt_select_rowid_xpos,
			// *stmt_select_rowid_token,
			*stmt_select_rowid_sentence,
			*stmt_select_rowid_filename//,
			// *stmt_select_rowid_edge
			;

	sqlite3_int64	rowid_lang,
			rowid_form,
		      	rowid_lemma,
			rowid_feats,
			rowid_deprel,
			rowid_upos,
			rowid_xpos,
			// rowid_token,
			rowid_sentence,
			rowid_filename //,
			// rowid_edge
			;

	time_t start, end;

	if(ENABLE_CAP_NUM_SENTENCES && num_added_sentences >= CAP_NUM_SENTENCES){
		printf("Already reached CAP_NUM_SENTENCES, ignoring %s\n", path);
		return 0;
	}

	start = time(NULL);

	printf("%s ...\n", path);

	if(create_cupt_sentence_iterator(&csi, path) != 0){
		fprintf(stderr, "Failed to call create_cupt_sentence_iterator for %s\n", path);
		return 1;
	}

	if(iterate_cupt_sentence_iterator(&csi) != 0){
		fprintf(stderr, "Failed to call iterate_cupt_sentence_iterator for %s\n", path);
		free_cupt_sentence_iterator(&csi);
		return 1;
	}

	if(db_prepare_statement(db, &stmt_select_rowid_lang, PATH_SQL_SELECT_LANG_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_form, PATH_SQL_SELECT_FORM_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_lemma, PATH_SQL_SELECT_LEMMA_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_feats, PATH_SQL_SELECT_FEATS_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_deprel, PATH_SQL_SELECT_DEPREL_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_upos, PATH_SQL_SELECT_UPOS_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_xpos, PATH_SQL_SELECT_XPOS_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_sentence, PATH_SQL_SELECT_SENTENCE_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_select_rowid_filename, PATH_SQL_SELECT_FILENAME_ROWID) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}

	if(db_prepare_statement(db, &stmt_insert_table_lang, PATH_SQL_INSERT_TABLE_LANG) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_form, PATH_SQL_INSERT_TABLE_FORM) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_lemma, PATH_SQL_INSERT_TABLE_LEMMA) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_feats, PATH_SQL_INSERT_TABLE_FEATS) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_deprel, PATH_SQL_INSERT_TABLE_DEPREL) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_upos, PATH_SQL_INSERT_TABLE_UPOS) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_xpos, PATH_SQL_INSERT_TABLE_XPOS) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_token, PATH_SQL_INSERT_TABLE_TOKEN) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	if(ENABLE_FTS5){
		if(db_prepare_statement(db, &stmt_insert_table_sentence, PATH_SQL_INSERT_TABLE_SENTENCE_FTS5) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	} else {
		if(db_prepare_statement(db, &stmt_insert_table_sentence, PATH_SQL_INSERT_TABLE_SENTENCE) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	}
	if(db_prepare_statement(db, &stmt_insert_table_filename, PATH_SQL_INSERT_TABLE_FILENAME) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}	
	if(ENABLE_EDGE_CONSTRUCTION){
		if(db_prepare_statement(db, &stmt_insert_table_edge, PATH_SQL_INSERT_TABLE_EDGE) != 0){perror("Failed to call db_prepare_statement\n"); free_cupt_sentence_iterator(&csi); return 1;}
	}
	if(db_prepare_statement(db, &stmt_insert_table_mwe_canonical, PATH_SQL_INSERT_TABLE_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
	if(db_prepare_statement(db, &stmt_insert_table_mwe_non_canonical, PATH_SQL_INSERT_TABLE_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}


	if(sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, 0, NULL) != SQLITE_OK){
		perror("Failed to call sqlite3_exec to begin transaction\n");
		goto panic_exit;
	}

	if(sqlite3_bind_text(stmt_insert_table_filename, 1, path, strlen(path), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
	if(try_statement(stmt_insert_table_filename, NULL) != 0){fprintf(stderr, "Failed to run stmt_insert_table_filename\n"); goto panic_exit;}

	if(sqlite3_bind_text(stmt_select_rowid_filename, 1, path, strlen(path), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
	if(try_statement(stmt_select_rowid_filename, NULL) != 0){fprintf(stderr, "Failed to run stmt_select_rowid_filename\n"); goto panic_exit;}

	rowid_filename = sqlite3_column_int64(stmt_select_rowid_filename, 0);

	const int64_t mwe_capacity = 32;
	const int64_t mwe_max_num_tokens = 16;
	const int64_t mwe_token_bfr_size = 1024;
	
	int32_t j = 0;
	while((!(csi.file_is_done)) && (ENABLE_CAP_NUM_SENTENCES == 0 || num_added_sentences < CAP_NUM_SENTENCES)){
		int64_t mwe_lengths[mwe_capacity];
		memset(mwe_lengths, '\0', mwe_capacity * sizeof(int64_t));
		ch_t mwe_bfr_lemma[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_lemma, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_form[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_form, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_feats[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_feats, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_deprel[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_deprel, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_upos[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_upos, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_xpos[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
		memset(mwe_bfr_xpos, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		ch_t mwe_bfr_class[mwe_capacity * mwe_token_bfr_size];
		memset(mwe_bfr_class, '\0', mwe_capacity * mwe_token_bfr_size * sizeof(ch_t));



		if(sqlite3_bind_int64(stmt_insert_table_sentence, 1, rowid_filename) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
		if(sqlite3_bind_text(stmt_insert_table_sentence, 2, csi.current_sentence.sentence_id, strlen(csi.current_sentence.sentence_id), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
		if(ENABLE_FTS5){
			const uint32_t sentence_raw_size = 4096;
			char sentence_raw_form[sentence_raw_size];
			char sentence_raw_lemma[sentence_raw_size];
			memset(sentence_raw_form, '\0', sentence_raw_size);
			memset(sentence_raw_lemma, '\0', sentence_raw_size);

			uint32_t local_index_form = 0;
			uint32_t local_index_lemma = 0;
			for(int32_t w = 0 ; w < csi.current_sentence.num_tokens ; w++){
				char* ptr_spaceaftereqno = strstr(csi.current_sentence.tokens[w].misc, "SpaceAfter=no");
				size_t strlen_form, strlen_lemma;
				strlen_form = strlen(csi.current_sentence.tokens[w].form);
				strlen_lemma = strlen(csi.current_sentence.tokens[w].lemma);
				if(local_index_form + strlen_form <= sentence_raw_size - 1){
					memcpy(sentence_raw_form + local_index_form, csi.current_sentence.tokens[w].form, strlen_form);
					local_index_form += strlen_form;
					if(local_index_form + 1 <= sentence_raw_size - 1 && ptr_spaceaftereqno == NULL){
						sentence_raw_form[local_index_form] = ' ';
						local_index_form++;
					}
				}
				uint8_t lemma_already_added = 0;
				for(int32_t y = w - 1 ; y >= 0 ; y--){
					if(strcmp(csi.current_sentence.tokens[y].lemma, csi.current_sentence.tokens[w].lemma) == 0){
						lemma_already_added = 1;
						break;
					}
				}
				if(lemma_already_added){continue;}
				if(local_index_lemma + strlen_lemma <= sentence_raw_size - 1){
					memcpy(sentence_raw_lemma + local_index_lemma, csi.current_sentence.tokens[w].lemma, strlen_lemma);
					local_index_lemma += strlen_lemma;
					if(local_index_lemma + 1 <= sentence_raw_size - 1 && ptr_spaceaftereqno == NULL){
						sentence_raw_lemma[local_index_lemma] = ' ';
						local_index_lemma++;
					}
				}
			}

			if(sqlite3_bind_text(stmt_insert_table_sentence, 3, sentence_raw_form, strlen(sentence_raw_form), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}	
			if(sqlite3_bind_text(stmt_insert_table_sentence, 4, sentence_raw_lemma, strlen(sentence_raw_lemma), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}	
		}

		if(try_statement(stmt_insert_table_sentence, NULL) != 0){perror("Failed to run stmt_insert_table_sentence\n"); goto panic_exit;}

		if(sqlite3_bind_int64(stmt_select_rowid_sentence, 1, rowid_filename) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
		if(sqlite3_bind_text(stmt_select_rowid_sentence, 2, csi.current_sentence.sentence_id, strlen(csi.current_sentence.sentence_id), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}

		if(try_statement(stmt_select_rowid_sentence, NULL) != 0){perror("Failed to run stmt_select_rowid_sentence\n"); goto panic_exit;}

		rowid_sentence = sqlite3_column_int64(stmt_select_rowid_sentence, 0);

		for(int32_t i = 0 ; i < csi.current_sentence.num_tokens ; i++){
			if(FILTER_OUT_FORMS && strcmp(csi.current_sentence.tokens[i].mwe, "*") == 0){
				uint8_t	found_digit,
					found_punct,
					found_uppercase //,
					// found_lowercase
					;
		
				found_digit = 0;
				found_punct = 0;
				found_uppercase = 0;
				// found_lowercase = 0;
		
				int32_t k = 0;
				while(csi.current_sentence.tokens[i].form[k] != '\0'){
					if('0' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '9'){
						found_digit = 1;
						break;
					} else if('A' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= 'Z'){
						found_uppercase = 1;
						break;
					} else if(
						('!' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '&') ||
						('(' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '/') ||
						(':' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '@') ||
						('[' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '`') ||
						('{' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '~')
					){
						found_punct = 1;
						break;
					}
						
					k++;
				}
		
				if(found_digit || found_punct || found_uppercase){continue;}
			}

			// printf("binding lang\n");
			if(sqlite3_bind_text(stmt_insert_table_lang, 1, lang, strlen(lang), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			// printf("bound lang\n");

			if(sqlite3_bind_text(stmt_insert_table_form, 1, csi.current_sentence.tokens[i].form, strlen(csi.current_sentence.tokens[i].form), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_lemma, 1, csi.current_sentence.tokens[i].lemma, strlen(csi.current_sentence.tokens[i].lemma), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_feats, 1, csi.current_sentence.tokens[i].feats, strlen(csi.current_sentence.tokens[i].feats), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_deprel, 1, csi.current_sentence.tokens[i].deprel, strlen(csi.current_sentence.tokens[i].deprel), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_upos, 1, csi.current_sentence.tokens[i].upos, strlen(csi.current_sentence.tokens[i].upos), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_xpos, 1, csi.current_sentence.tokens[i].xpos, strlen(csi.current_sentence.tokens[i].xpos), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}

			if(try_statement(stmt_insert_table_lang, NULL) != 0){perror("Failed to run stmt_insert_table_lang\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_form, NULL) != 0){perror("Failed to run stmt_insert_table_form\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_lemma, NULL) != 0){perror("Failed to run stmt_insert_table_lemma\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_feats, NULL) != 0){perror("Failed to run stmt_insert_table_feats\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_deprel, NULL) != 0){perror("Failed to run stmt_insert_table_deprel\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_upos, NULL) != 0){perror("Failed to run stmt_insert_table_upos\n"); goto panic_exit;}
			if(try_statement(stmt_insert_table_xpos, NULL) != 0){perror("Failed to run stmt_insert_table_xpos\n"); goto panic_exit;}

			// printf("binding lang (2)\n");
			if(sqlite3_bind_text(stmt_select_rowid_lang, 1, lang, strlen(lang), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			// printf("bound lang (2)\n");
			if(sqlite3_bind_text(stmt_select_rowid_form, 1, csi.current_sentence.tokens[i].form, strlen(csi.current_sentence.tokens[i].form), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_select_rowid_lemma, 1, csi.current_sentence.tokens[i].lemma, strlen(csi.current_sentence.tokens[i].lemma), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_select_rowid_feats, 1, csi.current_sentence.tokens[i].feats, strlen(csi.current_sentence.tokens[i].feats), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_select_rowid_deprel, 1, csi.current_sentence.tokens[i].deprel, strlen(csi.current_sentence.tokens[i].deprel), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_select_rowid_upos, 1, csi.current_sentence.tokens[i].upos, strlen(csi.current_sentence.tokens[i].upos), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_select_rowid_xpos, 1, csi.current_sentence.tokens[i].xpos, strlen(csi.current_sentence.tokens[i].xpos), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}

			if(try_statement(stmt_select_rowid_lang, NULL) != 0){perror("Failed to run stmt_select_rowid_lang\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_form, NULL) != 0){perror("Failed to run stmt_select_rowid_form\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_lemma, NULL) != 0){perror("Failed to run stmt_select_rowid_lemma\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_feats, NULL) != 0){perror("Failed to run stmt_select_rowid_feats\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_deprel, NULL) != 0){perror("Failed to run stmt_select_rowid_deprel\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_upos, NULL) != 0){perror("Failed to run stmt_select_rowid_upos\n"); goto panic_exit;}
			if(try_statement(stmt_select_rowid_xpos, NULL) != 0){perror("Failed to run stmt_select_rowid_xpos\n"); goto panic_exit;}

			rowid_lang = sqlite3_column_int64(stmt_select_rowid_lang, 0);
			rowid_form = sqlite3_column_int64(stmt_select_rowid_form, 0);
			rowid_lemma = sqlite3_column_int64(stmt_select_rowid_lemma, 0);
			rowid_feats = sqlite3_column_int64(stmt_select_rowid_feats, 0);
			rowid_deprel = sqlite3_column_int64(stmt_select_rowid_deprel, 0);
			rowid_upos = sqlite3_column_int64(stmt_select_rowid_upos, 0);
			rowid_xpos = sqlite3_column_int64(stmt_select_rowid_xpos, 0);

			/* // DO NOT REMOVE
			printf("rowid_form: %lli\n", rowid_form);
			printf("rowid_lemma: %lli\n", rowid_lemma);
			printf("rowid_feats: %lli\n", rowid_feats);
			printf("rowid_deprel: %lli\n", rowid_deprel);
			printf("rowid_upos: %lli\n", rowid_upos);
			printf("rowid_xpos: %lli\n", rowid_xpos);
			*/

			if(sqlite3_bind_int64(stmt_insert_table_token, 1, rowid_sentence) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_token, 2, csi.current_sentence.tokens[i].id_raw, strlen(csi.current_sentence.tokens[i].id_raw), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 3, rowid_form) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 4, rowid_lemma) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 5, rowid_upos) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 6, rowid_xpos) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 7, rowid_feats) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_token, 8, csi.current_sentence.tokens[i].head, strlen(csi.current_sentence.tokens[i].head), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 9, rowid_deprel) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_token, 10, csi.current_sentence.tokens[i].deps, strlen(csi.current_sentence.tokens[i].deps), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_token, 11, csi.current_sentence.tokens[i].misc, strlen(csi.current_sentence.tokens[i].misc), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_token, 12, csi.current_sentence.tokens[i].mwe, strlen(csi.current_sentence.tokens[i].mwe), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_int64(stmt_insert_table_token, 13, rowid_lang) != 0){fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", path); goto panic_exit;}

			if(try_statement(stmt_insert_table_token, NULL) != 0){perror("Failed to run stmt_select_rowid_xpos\n"); goto panic_exit;}

			/*
			if(sqlite3_bind_text(stmt_insert_table_edge, 1, csi.current_sentence.tokens[i].form, strlen(csi.current_sentence.tokens[i].form), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_edge, 2, "form_to_lemma", strlen("form_to_lemma"), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(sqlite3_bind_text(stmt_insert_table_edge, 3, csi.current_sentence.tokens[i].lemma, strlen(csi.current_sentence.tokens[i].lemma), NULL) != 0){fprintf(stderr, "Failed to call sqlite3_bind_text for %s\n", path); goto panic_exit;}
			if(try_statement(stmt_insert_table_edge, NULL) != 0){perror("Failed to run stmt_insert_table_edge\n"); goto panic_exit;}
			*/
			if(ENABLE_EDGE_CONSTRUCTION && EARLY_STANDARD_EDGE_COMPUTATION){
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].form, "form_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].lemma, "lemma_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].feats, "feats_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].deprel, "deprel_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].upos, "upos_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, csi.current_sentence.tokens[i].xpos, "xpos_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_form", csi.current_sentence.tokens[i].form) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_lemma", csi.current_sentence.tokens[i].lemma) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_feats", csi.current_sentence.tokens[i].feats) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_deprel", csi.current_sentence.tokens[i].deprel) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_upos", csi.current_sentence.tokens[i].upos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
				if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_xpos", csi.current_sentence.tokens[i].xpos) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
			}

			if(		sqlite3_reset(stmt_insert_table_lang) != 0
				||	sqlite3_reset(stmt_insert_table_form) != 0
				||	sqlite3_reset(stmt_insert_table_lemma) != 0
				||	sqlite3_reset(stmt_insert_table_feats) != 0
				||	sqlite3_reset(stmt_insert_table_deprel) != 0
				||	sqlite3_reset(stmt_insert_table_upos) != 0
				||	sqlite3_reset(stmt_insert_table_xpos) != 0
				||	sqlite3_reset(stmt_select_rowid_lang) != 0
				||	sqlite3_reset(stmt_select_rowid_form) != 0
				||	sqlite3_reset(stmt_select_rowid_lemma) != 0
				||	sqlite3_reset(stmt_select_rowid_feats) != 0
				||	sqlite3_reset(stmt_select_rowid_deprel) != 0
				||	sqlite3_reset(stmt_select_rowid_upos) != 0
				||	sqlite3_reset(stmt_select_rowid_xpos) != 0
				||	sqlite3_reset(stmt_insert_table_token) != 0
				// ||	sqlite3_reset(stmt_insert_table_edge) != 0 // already done in db_insert_table_edge
			){
				fprintf(stderr, "Failed to call sqlite3_reset for %s\n", path);
				goto panic_exit;
			}

			// MWE

			if(strlen(csi.current_sentence.tokens[i].mwe) > 0 && strcmp(csi.current_sentence.tokens[i].mwe, "*") != 0){
				const int32_t key_size = 8;
				size_t bytes_to_cpy;
				ch_t key[key_size];
				memset(key, '\0', key_size);
				
				ch_t* tok = strtok(csi.current_sentence.tokens[i].mwe, ";");
		
				do {
					int32_t k = 0;
					while(k < key_size - 1){
						if('0' <= tok[k] && tok[k] <= '9'){
							key[k] = tok[k];
						}
						k++;
					}
	
					int64_t mwe_index = strtol(key, NULL, 10);

					ch_t* ptr_colon = strchr(tok, ':');
					if(ptr_colon != NULL){
						bytes_to_cpy = strlen(ptr_colon + 1);
						if(bytes_to_cpy > mwe_token_bfr_size - 1){
							bytes_to_cpy = mwe_token_bfr_size - 1;
						}
						memcpy(&(mwe_bfr_class[mwe_index * mwe_token_bfr_size]), ptr_colon + 1, bytes_to_cpy);
					}
	
					if(mwe_index < mwe_capacity){
						if(mwe_lengths[mwe_index] < mwe_max_num_tokens){
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].lemma);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_lemma[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].lemma, bytes_to_cpy);
	
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].form);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_form[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].form, bytes_to_cpy);
	
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].feats);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_feats[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].feats, bytes_to_cpy);
	
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].deprel);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_deprel[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].deprel, bytes_to_cpy);
	
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].upos);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_upos[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].upos, bytes_to_cpy);
	
							bytes_to_cpy = strlen(csi.current_sentence.tokens[i].xpos);
							if(bytes_to_cpy > mwe_token_bfr_size - 1){bytes_to_cpy = mwe_token_bfr_size - 1;}
							memcpy(&(mwe_bfr_xpos[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].xpos, bytes_to_cpy);
							mwe_lengths[mwe_index]++;
						}
					}
	
					tok = strtok(NULL, ";");
					if(tok == NULL){break;}
		
				} while (1);
			}
		}

		if(sqlite3_reset(stmt_insert_table_sentence) != 0){fprintf(stderr, "Failed to call sqlite3_reset for %s\n", path); goto panic_exit;}
		if(sqlite3_reset(stmt_select_rowid_sentence) != 0){fprintf(stderr, "Failed to call sqlite3_reset for %s\n", path); goto panic_exit;}

		// break;

		for(int64_t k = 0 ; k < mwe_capacity ; k++){
			/* // DO NOT REMOVE
			if(mwe_lengths[k] == 1){
				for(int32_t m = 0 ; m < csi.current_sentence.num_tokens ; m++){
					printf("%i: %s / %s\n", m, csi.current_sentence.tokens[m].form, csi.current_sentence.tokens[m].mwe);
				}
				continue;
			}
			*/
			if(mwe_lengths[k] > 0){
				const int32_t key_canonical_size = 256;
				const int32_t key_non_canonical_size = 256;
				int32_t local_str_index;
				ch_t key_canonical[key_canonical_size];
				ch_t key_non_canonical[key_non_canonical_size];

				// -------- lemma --------

				qsort(&(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size]), mwe_lengths[k], mwe_token_bfr_size, strcmp_wrapper);

				local_str_index = 0;
				memset(key_canonical, '\0', key_canonical_size);
				strncpy(key_canonical + local_str_index, "_MWE-", 5);
				local_str_index += 5;

				size_t len_class = strlen(&(mwe_bfr_class[k * mwe_token_bfr_size]));
				if(local_str_index + len_class < key_canonical_size - 1){
					strncpy(key_canonical + local_str_index, &(mwe_bfr_class[k * mwe_token_bfr_size]), len_class);
					local_str_index += len_class;
				}

				for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
					if(local_str_index + 1 < key_canonical_size - 1){
						strncpy(key_canonical + local_str_index, "_", 1);
						local_str_index++;
						size_t len_lemma = strlen(&(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]));
						if(local_str_index + len_lemma < key_canonical_size - 1){
							strncpy(key_canonical + local_str_index, &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), len_lemma);
							local_str_index += len_lemma;
						}
					}
				}

				/*
				int64_t index_mwe_canonical;
				if(type_node_add_node_if_absent(&(mg.type_nodes[mwe_canonical_i]), key_canonical, &index_mwe_canonical) != 0){perror("failed to call type_node_add_node_if_absent\n"); return 1;}
				*/

				if(sqlite3_bind_text(stmt_insert_table_mwe_canonical, 1, key_canonical, strlen(key_canonical), NULL) != SQLITE_OK){perror("failed to call sqlite3_bind_text\n"); return 1;}
				if(try_statement(stmt_insert_table_mwe_canonical, NULL) != 0){perror("failed to call try_statement\n"); return 1;}
				sqlite3_reset(stmt_insert_table_mwe_canonical);
				if(ENABLE_EDGE_CONSTRUCTION){
					if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_mwe_canonical", key_canonical) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
					if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}

					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_form", &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_lemma", &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_feats", &(mwe_bfr_feats[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_deprel", &(mwe_bfr_deprel[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_upos", &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_xpos", &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "form_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "lemma_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_feats[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "feats_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_deprel[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "deprel_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "upos_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "xpos_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
					}
				}

				// -------- form --------

				qsort(&(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size]), mwe_lengths[k], mwe_token_bfr_size, strcmp_wrapper);

				local_str_index = 0;
				memset(key_non_canonical, '\0', key_non_canonical_size);
				strncpy(key_non_canonical + local_str_index, "_MWE-", 5);
				local_str_index += 5;

				len_class = strlen(&(mwe_bfr_class[k * mwe_token_bfr_size]));
				if(local_str_index + len_class < key_non_canonical_size - 1){
					strncpy(key_non_canonical + local_str_index, &(mwe_bfr_class[k * mwe_token_bfr_size]), len_class);
					local_str_index += len_class;
				}

				for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
					if(local_str_index + 1 < key_non_canonical_size - 1){
						strncpy(key_non_canonical + local_str_index, "_", 1);
						local_str_index++;
						size_t len_form = strlen(&(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]));
						if(local_str_index + len_form < key_non_canonical_size - 1){
							strncpy(key_non_canonical + local_str_index, &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), len_form);
							local_str_index += len_form;
						}
					}
				}

				if(sqlite3_bind_text(stmt_insert_table_mwe_non_canonical, 1, key_non_canonical, strlen(key_non_canonical), NULL) != SQLITE_OK){perror("failed to call sqlite3_bind_text\n"); return 1;}
				if(try_statement(stmt_insert_table_mwe_non_canonical, NULL) != 0){perror("failed to call try_statement\n"); return 1;}
				sqlite3_reset(stmt_insert_table_mwe_non_canonical);
				if(ENABLE_EDGE_CONSTRUCTION){
					if(db_insert_table_edge(stmt_insert_table_edge, lang, "lang_to_mwe_non_canonical", key_non_canonical) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}
					if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_lang", lang) != 0){perror("Failed to call db_insert_table_edge\n"); goto panic_exit;}

					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_form", &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_lemma", &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_feats", &(mwe_bfr_feats[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_deprel", &(mwe_bfr_deprel[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_upos", &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_xpos", &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "form_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "lemma_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_feats[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "feats_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_deprel[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "deprel_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "upos_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
						if(db_insert_table_edge(stmt_insert_table_edge, &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), "xpos_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
					}
	
	
					// -------- binding --------
	
					if(db_insert_table_edge(stmt_insert_table_edge, key_canonical, "mwe_canonical_to_mwe_non_canonical", key_non_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
					if(db_insert_table_edge(stmt_insert_table_edge, key_non_canonical, "mwe_non_canonical_to_mwe_canonical", key_canonical) != 0){perror("failed to call db_insert_table_edge\n"); return 1;}
				}
			}
		}

		if(iterate_cupt_sentence_iterator(&csi) != 0){
			fprintf(stderr, "Failed to call iterate_cupt_sentence_iterator for %s\n", path);
			goto panic_exit;
		}

		j++;

		if(j % 10000 == 0){
			end = time(NULL);
			printf("%s: %i sentences; %lis; %li sentences/s\n", path, j, end - start, (int64_t) floor(((double) j) / ((double) (end - start))));
		}

		if(num_added_sentences < UINT64_MAX){num_added_sentences++;} else {perror("Arrived at maximum value for a uint64_t (num_added_sentences), stopping.\n"); break;}

		if(ENABLE_SHORT_TRANSACTIONS && j % SHORT_TRANSACTION_SIZE == 0){
			sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL);
			sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
		}
	}

	if(sqlite3_exec(db, "END TRANSACTION;", NULL, 0, NULL) != SQLITE_OK){
		perror("Failed to call sqlite3_exec to end transaction\n");
		goto panic_exit;
	}

	free_cupt_sentence_iterator(&csi);
	sqlite3_finalize(stmt_insert_table_lang);
	sqlite3_finalize(stmt_insert_table_form);
	sqlite3_finalize(stmt_insert_table_lemma);
	sqlite3_finalize(stmt_insert_table_feats);
	sqlite3_finalize(stmt_insert_table_deprel);
	sqlite3_finalize(stmt_insert_table_upos);
	sqlite3_finalize(stmt_insert_table_xpos);
	sqlite3_finalize(stmt_insert_table_token);
	sqlite3_finalize(stmt_insert_table_sentence);
	sqlite3_finalize(stmt_insert_table_filename);
	sqlite3_finalize(stmt_insert_table_mwe_canonical);
	sqlite3_finalize(stmt_insert_table_mwe_non_canonical);
	if(ENABLE_EDGE_CONSTRUCTION){
		sqlite3_finalize(stmt_insert_table_edge);
	}
	sqlite3_finalize(stmt_select_rowid_lang);
	sqlite3_finalize(stmt_select_rowid_form);
	sqlite3_finalize(stmt_select_rowid_lemma);
	sqlite3_finalize(stmt_select_rowid_feats);
	sqlite3_finalize(stmt_select_rowid_deprel);
	sqlite3_finalize(stmt_select_rowid_upos);
	sqlite3_finalize(stmt_select_rowid_xpos);

	end = time(NULL);

	printf("%s: DONE; %lis, %i sentences\n", path, end - start, j);

	return 0;

	panic_exit:

	sqlite3_exec(db, "ROLLBACK;", NULL, NULL, NULL);

	free_cupt_sentence_iterator(&csi);
	sqlite3_finalize(stmt_insert_table_lang);
	sqlite3_finalize(stmt_insert_table_form);
	sqlite3_finalize(stmt_insert_table_lemma);
	sqlite3_finalize(stmt_insert_table_feats);
	sqlite3_finalize(stmt_insert_table_deprel);
	sqlite3_finalize(stmt_insert_table_upos);
	sqlite3_finalize(stmt_insert_table_xpos);
	sqlite3_finalize(stmt_insert_table_token);
	sqlite3_finalize(stmt_insert_table_mwe_canonical);
	sqlite3_finalize(stmt_insert_table_mwe_non_canonical);
	sqlite3_finalize(stmt_insert_table_sentence);
	sqlite3_finalize(stmt_insert_table_filename);
	if(ENABLE_EDGE_CONSTRUCTION){
		sqlite3_finalize(stmt_insert_table_edge);
	}
	sqlite3_finalize(stmt_select_rowid_lang);
	sqlite3_finalize(stmt_select_rowid_form);
	sqlite3_finalize(stmt_select_rowid_lemma);
	sqlite3_finalize(stmt_select_rowid_feats);
	sqlite3_finalize(stmt_select_rowid_deprel);
	sqlite3_finalize(stmt_select_rowid_upos);
	sqlite3_finalize(stmt_select_rowid_xpos);
	return 1;
}

// ----

int32_t load_type_node_from_sql(struct type_node* const tn, sqlite3* db, const char* const path_sql){
	sqlite3_stmt* stmt;
	uint8_t can_continue;

	printf("load_type_node_from_sql: %s\n", path_sql);

	if(db_prepare_statement(db, &stmt, path_sql) != 0){
		fprintf(stderr, "Failed to call db_prepare_statement for %s\n", path_sql);
		return 1;
	}

	can_continue = 1;
	while(can_continue){
		if(try_statement(stmt, &can_continue) != 0){
			fprintf(stderr, "Failed to call try_statement for %s\n", path_sql);
			sqlite3_finalize(stmt);
			return 1;
		}


		// sqlite3_int64 rowid = sqlite3_column_int64(stmt, 0);
		const char* value = (char*) sqlite3_column_text(stmt, 1);

		if(value == NULL){continue;}

		if(type_node_add_node(tn, value) != 0){
			fprintf(stderr, "Failed to call type_node_add_node for %s\n", path_sql);
			sqlite3_finalize(stmt);
			return 1;
		}

		// tn->nodes[tn->node_size-1].pk = rowid;
		
		if(tn->node_size % 100000 == 0){
			printf("tn->node_size: %li\n", tn->node_size);
		}
	}

	return 0;
}

int32_t multigraph_collective_random_path_sql(sqlite3* db, struct metapath* const mp, const uint64_t num_iter){
	uint32_t i;
	uint64_t j;
	sqlite3_int64 current_pk;
	char* current_key;
	struct type_node tn;
	int64_t index;
	double res1, res2;
	struct graph g;
	time_t start, end;

	start = time(NULL);

	for(i = 0 ; i < mp->path_size ; i++){
		if(mp->path[i].path_sql != NULL){
			if(db_prepare_statement(db, &(mp->path[i].stmt), mp->path[i].path_sql) != 0){
				fprintf(stderr, "Failed to call db_prepare_statement for %s\n", mp->path[i].path_sql);
				return 1;
			}
		} else {
			perror("path_sql not defined\n");
			return 1;
		}
	}

	if(type_node_create(&tn, "mp_last_layer", 4096) != 0){
		perror("failed to call type_node_create\n");
		return 1;
	}

	for(j = 0 ; j < num_iter ; j++){
		current_pk = -1;
		current_key = NULL;
		for(i = 0 ; i < mp->path_size ; i++){
			if(i > 0){
				if(sqlite3_bind_int64(mp->path[i].stmt, 1, current_pk) != SQLITE_OK){
					fprintf(stderr, "Failed to call sqlite3_bind_int64 for %s\n", mp->path[i].path_sql);
					return 1;
				}
			}
			if(try_statement(mp->path[i].stmt, NULL) != 0){
				fprintf(stderr, "Failed to call try_statement for %s\n", mp->path[i].path_sql);
				return 1;
			}

			current_pk = sqlite3_column_int64(mp->path[i].stmt, 0);
			current_key = (char*) sqlite3_column_text(mp->path[i].stmt, 1);

			// printf("current_key: %s\n", current_key);

			if(type_node_add_node_if_absent(&tn, current_key, &index) != 0){
				fprintf(stderr, "Failed to call type_node_add_node_if_absent for %s\n", mp->path[i].path_sql);
				return 1;
			}

			if(i == mp->path_size - 1){
				index = type_node_get_index(&tn, current_key);
			}
			if(sqlite3_reset(mp->path[i].stmt) != SQLITE_OK){
				fprintf(stderr, "Failed to call sqlite3_reset for %s\n", mp->path[i].path_sql);
				return 1;
			}
		}

		if(index != -1){
			tn.nodes[index].count_end++;
		} else {
			printf("unknown key: %s\n", current_key);
		}
	}

	if(type_node_count_end_to_graph(&tn, &g, 1) != 0){
		perror("failed to call type_node_count_end_to_graph\n");
		type_node_free(&tn);
		return 1;
	}

	shannon_weaver_entropy_from_graph(&g, &res1, &res2);

	printf("(collective) sw entropy: %f\n", res1);

	shannon_evenness_from_graph(&g, &res1);

	printf("(collective) sw evenness: %f\n", res1);

	// chao_et_al_functional_diversity_from_graph(&g, 

	type_node_free(&tn);

	end = time(NULL);

	printf("%lis; %liit/s\n", end - start, (int64_t) floor(((double) num_iter) / ((double) (end - start))));

	return 0;
}

int32_t output_vector_from_metapath(sqlite3* const db, struct multigraph* const mg, struct metapath* const mp, double* const agg_pointer, double** const output_vector, uint64_t* const output_vector_cardinality){
	int32_t i;
	uint32_t p;
	sqlite3_int64 j;
	time_t step_start, step_end;

	double *src_v, *tgt_v;
	double tgt_sum;
	sqlite3_stmt *maxid_stmt, *edge_pass_stmt, *stmt_init;
	sqlite3_int64 maxid, a, b;
	double n_f64;
	uint8_t can_continue;
	size_t alloc_size;
	double entropy_fp64, evenness_fp64;
	int32_t active_nodes;
	int32_t delta_to_previous;

	if(output_vector == NULL){
		perror("double** const output_vector must not be NULL\n");
		return 1;
	}
	if(output_vector_cardinality == NULL){
		perror("uint64_t* const output_vector_cardinality must not be NULL\n");
		return 1;
	}

	src_v = NULL;
	tgt_v = NULL;

	printf("%c %-20s %-10s %17s  %5s  %5s  %s\n", '#', "metatype", "filter", "#nodes/max", "H", "even.", "time");

	if(mp->backward){i = mp->path_size - 1; delta_to_previous = 1;} else {i = 0; delta_to_previous = -1;}

	for(p = 0 ; p < mp->path_size ; p++){
		step_start = time(NULL);
		if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_LANG) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_FORM) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_LEMMA) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_FEATS) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_DEPREL) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_UPOS) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_XPOS) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_MWE_CANONICAL) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
			if(db_prepare_statement(db, &maxid_stmt, PATH_SQL_MAX_ROWID_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_stmt\n"); return 1;}
		}


		if(try_statement(maxid_stmt, NULL) != 0){
			perror("failed to call try_statement\n");
			return 1;
		}

		maxid = sqlite3_column_int64(maxid_stmt, 0);

		tgt_sum = 0.0;

		alloc_size = maxid * sizeof(double);
	
		tgt_v = malloc(alloc_size);
		memset(tgt_v, '\0', alloc_size);

		if(p == 0){
			if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
				if(db_prepare_statement(db, &stmt_init, PATH_SQL_INIT_VECTOR_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
			} else {
				fprintf(stderr, "Unknown way to start a metapath\n");
				return 1;
			}

			if(sqlite3_bind_text(stmt_init, 1, mp->path[i].filter, strlen(mp->path[i].filter), NULL) != SQLITE_OK){fprintf(stderr, "Failed to call sqlite3_bind_text to add filter %s\n", mp->path[i].filter); return 1;}

			do {
				if(try_statement(stmt_init, &can_continue) != 0){
					perror("failed to call try_statement\n");
					return 1;
				}

				a = sqlite3_column_int64(stmt_init, 0);
				n_f64 = sqlite3_column_double(stmt_init, 1);

				tgt_v[a] += (double) n_f64;
			} while(can_continue);

			src_v = tgt_v;
			tgt_v = NULL;

			sqlite3_finalize(stmt_init);
		} else {
			if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "lang") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LANG_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "form") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FORM_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "lemma") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_LEMMA_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "feats") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_FEATS_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "deprel") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_DEPREL_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "upos") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_UPOS_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "xpos") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_XPOS_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "mwe_canonical") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_non_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_CANONICAL_TO_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			} else if(strcmp(mg->type_nodes[mp->path[i + delta_to_previous].step_destination_index].key, "mwe_non_canonical") == 0){
				if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lang") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_LANG) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "form") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_FORM) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "lemma") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "feats") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_FEATS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "deprel") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "upos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_UPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "xpos") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_XPOS) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else if(strcmp(mg->type_nodes[mp->path[i].step_destination_index].key, "mwe_canonical") == 0){
					if(db_prepare_statement(db, &edge_pass_stmt, PATH_SQL_EDGE_PASS_MWE_NON_CANONICAL_TO_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); return 1;}
				} else {
					fprintf(stderr, "Unknown way to continue a metapath\n");
					return 1;
				}
			}
	
			if(sqlite3_bind_text(stmt_init, 1, mp->path[i + delta_to_previous].filter, strlen(mp->path[i + delta_to_previous].filter), NULL) != SQLITE_OK){fprintf(stderr, "Failed to call sqlite3_bind_text to add filter %s\n", mp->path[i + delta_to_previous].filter); return 1;}
			if(sqlite3_bind_text(stmt_init, 2, mp->path[i].filter, strlen(mp->path[i].filter), NULL) != SQLITE_OK){fprintf(stderr, "Failed to call sqlite3_bind_text to add filter %s\n", mp->path[i].filter); return 1;}

			can_continue = 1;
	
			do {
				if(try_statement(edge_pass_stmt, &can_continue) != 0){
					perror("failed to call try_statement\n");
					return 1;
				}

				a = sqlite3_column_int64(stmt_init, 0);
				b = sqlite3_column_int64(stmt_init, 1);
				n_f64 = sqlite3_column_double(stmt_init, 2);

				tgt_v[b] += src_v[a] * ((double) n_f64);
			} while(can_continue);

			for(j = 0 ; j < maxid ; j++){
				tgt_sum += tgt_v[j];
			}
			for(j = 0 ; j < maxid ; j++){
				tgt_v[j] /= tgt_sum;
			}
			/*
			double checksum = 0.0;
			for(j = 0 ; j < maxid ; j++){
				checksum += tgt_v[j];
			}
			printf("checksum: %f\n", checksum);
			*/

			free(src_v);
			src_v = tgt_v;
			tgt_v = NULL;
	
			sqlite3_finalize(edge_pass_stmt);
		}
		sqlite3_finalize(maxid_stmt);

		entropy_fp64 = 0.0;
		active_nodes = 0;
		for(j = 0 ; j < maxid ; j++){
			if(src_v[j] > 0.0){
				active_nodes++;
				entropy_fp64 += src_v[j] * (-log(src_v[j]));
			}
		}
		evenness_fp64 = entropy_fp64 / log((double) active_nodes);

		if(agg_pointer != NULL && p == mp->path_size - 1){(*agg_pointer) += entropy_fp64;}

		step_end = time(NULL);
	
		// printf("%i) %-20s: %-10s; nodes %8i/%8lli; H=%.3f; even.=%.3f; %lis\n", i, mg->type_nodes[mp->path[i].step_destination_index].key, mp->path[i].filter, active_nodes, maxid, entropy_fp64, evenness_fp64, step_end - step_start);
		printf("%i %-20s %-10s %8i/%8lli  %.3f  %.3f  %lis\n", i, mg->type_nodes[mp->path[i].step_destination_index].key, mp->path[i].filter, active_nodes, maxid, entropy_fp64, evenness_fp64, step_end - step_start);

		if(mp->backward){i--;} else {i++;}
	}
	
	(*output_vector) = src_v;
	(*output_vector_cardinality) = maxid;

	return 0;
}

int32_t multigraph_collective_random_path_sql_v2(sqlite3* const db, struct multigraph* const mg, struct metapath* const mp1, struct metapath* const mp2, double* const agg_pointer){
	time_t start, end;
	uint64_t i;
	double *mp1_v, *mp2_v;
	uint64_t mp1_cardinality, mp2_cardinality, mp1_cardinality_not_null, mp2_cardinality_not_null;
	double relative_entropy_fp64, sum;
	uint64_t intersection_, union_;

	start = time(NULL);

	mp1_v = NULL;
	mp2_v = NULL;

	printf("Running metapath 1 (p) ...\n");
	if(output_vector_from_metapath(db, mg, mp1, agg_pointer, &mp1_v, &mp1_cardinality) != 0){
		perror("failed to call output_vector_from_metapath\n");
		return 1;
	}
	if(mp2 != NULL){
		printf("Running metapath 2 (q) ...\n");
		if(output_vector_from_metapath(db, mg, mp2, NULL, &mp2_v, &mp2_cardinality) != 0){
			perror("failed to call output_vector_from_metapath\n");
			free(mp1_v);
			return 1;
		}

		intersection_ = 0;
		union_ = 0;
		mp1_cardinality_not_null = 0;
		mp2_cardinality_not_null = 0;

		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < mp1_cardinality ; i++){
			sum += mp1_v[i];
			if(mp1_v[i] == 0.0){
				if(mp2_v[i] > 0.0){
					union_++;
					mp2_cardinality_not_null++;
				}
			} else if(mp1_v[i] > 0.0){
				mp1_cardinality_not_null++;
				if(mp2_v[i] > 0.0){
					intersection_++;
					mp2_cardinality_not_null++;
				}
				union_++;
			}
			if(mp1_v[i] < 0.0 || mp2_v[i] < 0.0){
				printf("WARNING negative value: %f, %f\n", mp1_v[i], mp2_v[i]);
			}
			if(mp1_v[i] == 0.0 || mp2_v[i] == 0.0){continue;}
			relative_entropy_fp64 += mp1_v[i] * (-log(mp2_v[i] / mp1_v[i]));
		}
		if(round(sum * 100000.0) != 100000.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(p||q) = %f\n", relative_entropy_fp64);

		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < mp2_cardinality ; i++){
			sum += mp2_v[i];
			if(mp1_v[i] < 0.0 || mp2_v[i] < 0.0){
				printf("WARNING negative value: %f, %f\n", mp1_v[i], mp2_v[i]);
			}
			if(mp1_v[i] == 0.0 || mp2_v[i] == 0.0){continue;}
			relative_entropy_fp64 += mp2_v[i] * (-log(mp1_v[i] / mp2_v[i]));
		}
		if(round(sum * 100000.0) != 100000.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(q||p) = %f\n", relative_entropy_fp64);

		printf("Jaccard = %f\n", ((double) intersection_) / ((double) union_));

		/*
		// trying only using overlap
		sum = 0.0;
		for(i = 0 ; i < mp1_cardinality ; i++){
			if(mp2_v[i] == 0.0){
				mp1_v[i] *= 0.0;
			}
			sum += mp1_v[i];
		}
		for(i = 0 ; i < mp1_cardinality ; i++){
			mp1_v[i] /= sum;
		}

		sum = 0.0;
		for(i = 0 ; i < mp2_cardinality ; i++){
			if(mp1_v[i] == 0.0){
				mp2_v[i] *= 0.0;
			}
			sum += mp2_v[i];
		}
		for(i = 0 ; i < mp2_cardinality ; i++){
			mp2_v[i] /= sum;
		}


		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < mp1_cardinality ; i++){
			sum += mp1_v[i];
			if(mp1_v[i] < 0.0 || mp2_v[i] < 0.0){
				printf("WARNING negative value: %f, %f\n", mp1_v[i], mp2_v[i]);
			}
			if(mp1_v[i] == 0.0 || mp2_v[i] == 0.0){continue;}
			relative_entropy_fp64 += mp1_v[i] * (-log(mp2_v[i] / mp1_v[i]));
		}
		if(sum != 1.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(p||q) = %f\n", relative_entropy_fp64);

		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < mp2_cardinality ; i++){
			sum += mp2_v[i];
			if(mp1_v[i] < 0.0 || mp2_v[i] < 0.0){
				printf("WARNING negative value: %f, %f\n", mp1_v[i], mp2_v[i]);
			}
			if(mp1_v[i] == 0.0 || mp2_v[i] == 0.0){continue;}
			relative_entropy_fp64 += mp2_v[i] * (-log(mp1_v[i] / mp2_v[i]));
		}
		if(sum != 1.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(q||p) = %f\n", relative_entropy_fp64);
		*/

		double mp1_s, mp2_s;
		double *mp1_v_union, *mp2_v_union;
		size_t alloc_size = union_ * sizeof(double);
		int32_t index_in_union = 0;
		int32_t mp1_zd_index, mp2_zd_index;
		struct zipfian_distribution mp1_zd, mp2_zd;

		mp1_v_union = malloc(alloc_size);
		if(mp1_v_union == NULL){perror("failed to malloc\n"); free(mp1_v); free(mp2_v); return 1;}
		memset(mp1_v_union, '\0', alloc_size);
		mp2_v_union = malloc(alloc_size);
		if(mp2_v_union == NULL){perror("failed to malloc\n"); free(mp2_v); free(mp2_v); free(mp1_v_union); return 1;}
		memset(mp2_v_union, '\0', alloc_size);

		for(i = 0 ; i < mp1_cardinality ; i++){
			if(mp1_v[i] != 0.0 || mp2_v[i] != 0.0){
				mp1_v_union[index_in_union] = mp1_v[i];
				mp2_v_union[index_in_union] = mp2_v[i];
				index_in_union++;
			}
		}

		// if(zipfian_fit(mp1_v, mp1_cardinality, &mp1_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); return 1;}
		if(zipfian_fit(mp1_v_union, union_, &mp1_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		printf("mp1_s: %f\n", mp1_s);
		if(create_zipfian_distribution(&mp1_zd, mp1_s, union_, FP64) != 0){perror("failed to call create_zipfian_distribution\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		// if(zipfian_fit(mp2_v, mp2_cardinality, &mp2_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); return 1;}
		if(zipfian_fit(mp2_v_union, union_, &mp2_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		printf("mp2_s: %f\n", mp2_s);
		if(create_zipfian_distribution(&mp2_zd, mp2_s, union_, FP64) != 0){perror("failed to call create_zipfian_distribution\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}

		double mp1_sum_existing, mp2_sum_existing;

		mp1_sum_existing = 0.0;
		mp2_sum_existing = 0.0;

		/*
		mp1_current_rank = 1;
		mp2_current_rank = 1;
		*/

		mp1_zd_index = 0;
		mp2_zd_index = 0;

		for(i = 0 ; i < mp1_cardinality_not_null ; i++){
			// if(i < 32){printf("%lu: %f\n", i, mp1_zd.vector.fp64[i]);}
			mp1_sum_existing += mp1_zd.vector.fp64[i];
			mp1_zd_index++;
		}
		// printf("mp1_cardinality_not_null: %lu\n", mp1_cardinality_not_null);
		printf("mp1_sum_existing: %f\n", mp1_sum_existing);
		for(i = 0 ; i < mp2_cardinality_not_null ; i++){
			// if(i < 32){printf("%lu: %f\n", i, mp2_zd.vector.fp64[i]);}
			mp2_sum_existing += mp2_zd.vector.fp64[i];
			mp2_zd_index++;
		}
		// printf("mp2_cardinality_not_null: %lu\n", mp2_cardinality_not_null);
		printf("mp2_sum_existing: %f\n", mp2_sum_existing);

		for(i = 0 ; i < union_ ; i++){
			if(mp1_v_union[i] == 0.0){
				mp1_v_union[i] = mp1_zd.vector.fp64[mp1_zd_index];
				mp1_zd_index++;
			} else {
				mp1_v_union[i] *= mp1_sum_existing;
			}
			if(mp2_v_union[i] == 0.0){
				mp2_v_union[i] = mp2_zd.vector.fp64[mp2_zd_index];
				mp2_zd_index++;
			} else {
				mp2_v_union[i] *= mp2_sum_existing;
			}
		}

		/*
		// checking it's correct
		if(zipfian_fit(mp1_v_union, union_, &mp1_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		printf("mp1_s: %f\n", mp1_s);
		if(create_zipfian_distribution(&mp1_zd, mp1_s, union_, FP64) != 0){perror("failed to call create_zipfian_distribution\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		if(zipfian_fit(mp2_v_union, union_, &mp2_s) != 0){perror("failed to call zipfian_fit\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		printf("mp2_s: %f\n", mp2_s);
		if(create_zipfian_distribution(&mp2_zd, mp2_s, union_, FP64) != 0){perror("failed to call create_zipfian_distribution\n"); free(mp1_v); free(mp2_v); free(mp1_v_union); free(mp2_v_union); return 1;}
		*/
		
		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < union_ ; i++){
			sum += mp1_v_union[i];
			relative_entropy_fp64 += mp1_v_union[i] * (-log(mp2_v_union[i] / mp1_v_union[i]));
		}
		if(round(sum * 100000.0) != 100000.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(p||q) = %f\n", relative_entropy_fp64);

		relative_entropy_fp64 = 0.0;
		sum = 0.0;
		for(i = 0 ; i < union_ ; i++){
			sum += mp2_v_union[i];
			relative_entropy_fp64 += mp2_v_union[i] * (-log(mp1_v_union[i] / mp2_v_union[i]));
		}
		if(round(sum * 100000.0) != 100000.0){
			printf("WARNING sum != 1.0: %f\n", sum);
		}
		printf("Relative entropy, H(q||p) = %f\n", relative_entropy_fp64);


		free_zipfian_distribution(&mp1_zd, FP64);
		free_zipfian_distribution(&mp2_zd, FP64);
		free(mp1_v_union);
		free(mp2_v_union);
	}

	if(mp1_v != NULL){free(mp1_v);}
	if(mp2_v != NULL){free(mp2_v);}

	end = time(NULL);

	printf("%lis\n", end - start);

	return 0;
}

#endif
