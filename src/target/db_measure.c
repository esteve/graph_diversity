/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sqlite3.h>

#include "gengraph.h"
#include "gengraph_cli.h"
#include "graph.h"
#include "dfunctions.h"
#include "cupt/parser.h"
#include "db_interface.h"

#define ENABLE_SENTENCE_INSTANCE 0
#define ENABLE_TOKEN_INSTANCE 0

// #define FILTER_OUT_FORMS 1

// const char* const DB_PATH = "data/target/main.db";
// const char* const DB_PATH = "data/target/main2.db";
// const char* const DB_PATH = "data/target/main3.db";
// const char* const DB_PATH = "data/target/main6.db";
// const char* const DB_PATH = "data/target/main8.db";

#ifndef DB_PATH
#define DB_PATH "data/target/main8.db"
#endif

int32_t main(){
	struct multigraph mg;
	struct word2vec w2v;
	sqlite3* db;

	printf("Using SQLITE3 v%s (%s)\n", SQLITE_VERSION, SQLITE_SOURCE_ID);
	printf("Connecting to %s\n", DB_PATH);

	if(sqlite3_open_v2(DB_PATH, &db, SQLITE_OPEN_READONLY, NULL) != SQLITE_OK){
		fprintf(stderr, "failed to call sqlite3_open for %s\n", DB_PATH);
		return 1;
	}

	if(db_create_function_regexp(db) != 0){
		perror("Failed to call db_create_function regexp\n");
		sqlite3_close(db);
		return 1;
	}

	/*
	if(load_word2vec_binary(&w2v, "/home/esteve/Documents/thesis/other_repos/word2vec/bin/MWE_S2S_ALL_filtered_100d_skip-gram.bin") != 0){
		perror("failed to call load_word2vec_binary\n");
		return 1;
	}
	*/

	if(multigraph_create(&mg) != 0){
		perror("failed to call multigraph_create\n");
		return 1;
	}

	/*
	mg_size_t	sentence_instance_i,
			token_instance_i,
			lang_i,
			form_i,
			lemma_i,
			feats_i,
			deprel_i,
			upos_i,
			xpos_i,
			mwe_instance_i,
			mwe_non_canonical_i,
			mwe_canonical_i
			;
	*/

	if(ENABLE_SENTENCE_INSTANCE){
		if(multigraph_add_type_node(&mg, "sentence_instance", 4096) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
		// sentence_instance_i = mg.node_size - 1;
	}
	if(ENABLE_TOKEN_INSTANCE){
		if(multigraph_add_type_node(&mg, "token_instance", 4096) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
		// token_instance_i = mg.node_size - 1;
	}
	if(multigraph_add_type_node(&mg, "lang", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// lang_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "form", 2048) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// form_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "lemma", 2048) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// lemma_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "feats", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// feats_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "deprel", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// deprel_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "upos", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// upos_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "xpos", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// xpos_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_instance", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// mwe_instance_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_non_canonical", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// mwe_non_canonical_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_canonical", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	// mwe_canonical_i = mg.node_size - 1;

	/**/
	mg.type_nodes[multigraph_get_index(&mg, "form")].w2v = &(w2v);
	mg.type_nodes[multigraph_get_index(&mg, "lemma")].w2v = &(w2v);
	mg.type_nodes[multigraph_get_index(&mg, "mwe_canonical")].w2v = &(w2v);
	/**/

	/*
	for(mg_size_t i = 0 ; i < mg.node_size ; i++){
		printf("%li / %s\n", i, mg.type_nodes[i].key);
	}
	*/

	/*
	if(load_type_node_from_sql(&(mg.type_nodes[form_i]), db, PATH_SQL_SELECT_FORM) != 0){
		perror("failed to call load_type_node_from_sql\n");
		goto panic_exit;
	}
	*/

	/*
	// metapath_add_step(&mp, form_i, NULL, PATH_SQL_METAPATH_INIT_WEIGHTED_FORM);
	// metapath_add_step(&mp, lemma_i, NULL, PATH_SQL_METAPATH_FORM_TO_LEMMA);
	metapath_add_step(&mp, mwe_canonical_i, NULL, NULL, "%");
	// metapath_add_step(&mp, lemma_i, NULL, NULL);
	// metapath_add_step(&mp, form_i, NULL, NULL);
	// metapath_add_step(&mp, feats_i, NULL, NULL);
	// metapath_add_step(&mp, deprel_i, NULL, NULL);
	// metapath_add_step(&mp, xpos_i, NULL, NULL);
	// metapath_add_step(&mp, upos_i, NULL, NULL);

	// if(multigraph_collective_random_path_sql(db, &mp, 1000000) != 0){
	// if(multigraph_collective_random_path_sql(db, &mp, 1000) != 0){
	if(multigraph_collective_random_path_sql_v2(db, &mg, &mp) != 0){
		// perror("Failed to call multigraph_collective_random_path_sql\n");
		perror("Failed to call multigraph_collective_random_path_sql_v2\n");
		goto panic_exit;
	}
	*/



	// --------
	
	read_cli_sql_v2(db, &mg);

	multigraph_free(&mg);
	free_word2vec(&w2v);
	sqlite3_close(db);
	return 0;

	/*
	panic_exit:
	multigraph_free(&mg);
	free_word2vec(&w2v);
	sqlite3_close(db);
	return 1;
	*/
}
