INSERT INTO t_lemma (
	lemma,
	n
) VALUES (
	?1, -- lemma
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
