CREATE TABLE IF NOT EXISTS t_mwe_non_canonical (
	mwe_non_canonical	TEXT	UNIQUE,
	n			INTEGER	NOT NULL
) STRICT;
