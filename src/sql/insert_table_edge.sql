INSERT INTO t_edge (
	src,
	fk_edge_type,
	tgt,
	passes
) VALUES (
	?1,
	(SELECT rowid FROM t_edge_meta WHERE edge_type=?2 LIMIT 1),
	?3,
	1
) ON CONFLICT DO UPDATE SET passes = passes + 1;
