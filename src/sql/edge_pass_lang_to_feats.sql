SELECT
	(SELECT rowid FROM t_lang WHERE lang=src LIMIT 1) AS a,
	(SELECT rowid FROM t_feats WHERE feats=tgt LIMIT 1) AS b,
	CAST(passes AS FLOAT) / (
		SELECT CAST(n AS FLOAT) FROM t_lang WHERE lang=src LIMIT 1
	) AS n
FROM t_edge AS t1 WHERE src REGEXP ?1 AND tgt REGEXP ?2 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="lang_to_feats" LIMIT 1);
