/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GENGRAPH_H
#define GENGRAPH_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sqlite3.h>

#include "dfunctions.h"
#include "graph.h"

#define MULTIGRAPH_CAPACITY_BASE 32
#define MULTIGRAPH_CAPACITY_STEP 32

#define MULTIGRAPH_NODE_EDGE_CAPACITY_BASE 0
#define MULTIGRAPH_NODE_EDGE_CAPACITY_STEP 8

#define TYPE_NODE_CAPACITY_BASE 0
#define TYPE_NODE_CAPACITY_STEP 1024
// #define TYPE_NODE_CAPACITY_STEP 4096
// #define TYPE_NODE_CAPACITY_STEP 4096 * 4

#define METAPATH_CAPACITY_STEP 8

typedef int64_t mg_size_t;
typedef int64_t mgn_size_t;
typedef int64_t mge_size_t;

typedef int64_t index_t; // ?? universal index type; negative for edges, positive for vertices (?)

typedef int64_t tn_index_t; // ??

typedef int64_t tn_size_t; // type_node size

typedef int32_t mnfa_size_t; // metanfa size

// typedef unsigned char ch_t;
typedef char ch_t;

const ch_t* MULTIGRAPH_EDGE_KEY_LEMMA_TO_FORM = "lemma_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_LEMMA_TO_UPOS = "lemma_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_LEMMA_TO_XPOS = "lemma_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_FORM_TO_LEMMA = "form_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_FORM_TO_UPOS = "form_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_FORM_TO_XPOS = "form_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_UPOS_TO_FORM = "upos_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_UPOS_TO_LEMMA = "upos_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_UPOS_TO_XPOS = "upos_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_XPOS_TO_FORM = "xpos_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_XPOS_TO_LEMMA = "xpos_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_XPOS_TO_UPOS = "xpos_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_MWE_NON_CANONICAL = "mwe_canonical_to_mwe_non_canonical";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_FORM = "mwe_canonical_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_LEMMA = "mwe_canonical_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_UPOS = "mwe_canonical_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_XPOS = "mwe_canonical_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_MWE_CANONICAL = "mwe_non_canonical_to_mwe_canonical";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_FORM = "mwe_non_canonical_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_LEMMA = "mwe_non_canonical_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_UPOS = "mwe_non_canonical_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_XPOS = "mwe_non_canonical_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_INSTANCE_TO_FORM = "mwe_instance_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_MWE_INSTANCE_TO_LEMMA = "mwe_instance_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_TOKEN_INSTANCE = "token_instance_to_token_instance"; // deprel
const ch_t* MULTIGRAPH_EDGE_KEY_SENTENCE_INSTANCE_TO_TOKEN_INSTANCE = "sentence_instance_to_token_instance";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_SENTENCE_INSTANCE = "token_instance_to_sentence_instance";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_FORM = "token_instance_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_LEMMA = "token_instance_to_lemma";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_FEATS = "token_instance_to_feats";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_UPOS = "token_instance_to_upos";
const ch_t* MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_XPOS = "token_instance_to_xpos";
const ch_t* MULTIGRAPH_EDGE_KEY_FORM_IS_HEAD_OF_FORM = "form_is_head_of_form";
const ch_t* MULTIGRAPH_EDGE_KEY_FORM_HAS_HEAD_FORM = "form_has_head_form";

const ch_t* MULTIGRAPH_EDGE_KEY_FORM_TO_LANG = "form_to_lang";
const ch_t* MULTIGRAPH_EDGE_KEY_LEMMA_TO_LANG = "lemma_to_lang";
const ch_t* MULTIGRAPH_EDGE_KEY_LANG_TO_FORM = "lang_to_form";
const ch_t* MULTIGRAPH_EDGE_KEY_LANG_TO_LEMMA = "lang_to_lemma";

/*
const ch_t* MULTIGRAPH_EDGE_KEY
*/


struct multigraph_edge {
	const ch_t* key_edge; // !
	tn_index_t tn_dst_index;
	ch_t* key_tgt;
	uint32_t passes;
};

struct multigraph_node {
	/*
	// v1
	tn_index_t** edges;
	uint32_t edge_dim0_capacity;
	uint32_t edge_dim0_size;
	uint32_t* edge_dim1_size;
	*/

	// v2
	struct multigraph_edge* edges;
	mgn_size_t edge_size;
	mgn_size_t edge_sorted_size;
	mgn_size_t edge_capacity;

	mgn_size_t key_size;
	ch_t* key;
	uint32_t count;
	uint32_t count_end;
	uint32_t edge_count;
};

struct type_node {
	tn_index_t index;
	ch_t* key;
	tn_size_t node_capacity;
	tn_size_t node_capacity_step;
	tn_size_t node_size;
	tn_size_t node_sorted_size;
	struct multigraph_node* nodes;
	uint64_t total_instance_count;
	uint64_t total_instance_count_end;
	// int8_t nodes_are_sorted;
	uint32_t percentiles[100];
	uint32_t percentile_sums[100];
	struct word2vec* w2v;
	uint8_t added_node_since_last_update;
};

struct metapath_step {
	tn_index_t step_destination_index;
	const ch_t* edge_key;
	const char* path_sql;
	sqlite3_stmt* stmt;
	ch_t* filter;
};

struct metapath {
	uint32_t path_size;
	uint32_t path_capacity;
	// tn_index_t* path;
	struct metapath_step* path;
	uint8_t backward;
};

/*
struct metanfa_element {
	
};

struct metanfa {
	struct metanfa_element* elements;
	mnfa_size_t element_size;
	mnfa_size_t element_capacity;
};
*/

struct multigraph {
	mg_size_t node_capacity;
	mg_size_t node_size;
	struct type_node* type_nodes;
};

int32_t metapath_request_more_capacity(struct metapath* const mp){
	uint32_t new_capacity;
	size_t alloc_size;

	new_capacity = mp->path_capacity + METAPATH_CAPACITY_STEP;

	// alloc_size = new_capacity * sizeof(tn_size_t);
	alloc_size = new_capacity * sizeof(struct metapath_step);

	mp->path = realloc(mp->path, alloc_size);
	if(mp->path == NULL){
		perror("failed to realloc\n");
		return 1;
	}
	// memset(&(mp->path[mp->path_size]), '\0', METAPATH_CAPACITY_STEP * sizeof(tn_size_t));
	memset(&(mp->path[mp->path_size]), '\0', METAPATH_CAPACITY_STEP * sizeof(struct metapath_step));

	mp->path_capacity = new_capacity;

	return 0;
}

void metapath_free(struct metapath* const mp){
	for(uint32_t i = 0 ; i < mp->path_size ; i++){
		if(mp->path[i].filter != NULL){
			free(mp->path[i].filter);
			mp->path[i].filter = NULL;
		}
	}
	if(mp->path != NULL){
		free(mp->path);
		mp->path = NULL;
	}
}

int32_t metapath_add_step(struct metapath* const mp, const tn_size_t step_destination_index, const ch_t* const edge_key, const ch_t* const path_sql, const ch_t* const filter){
	size_t strlen_filter, alloc_size;

	if(step_destination_index < 0){
		perror("step_destination_index < 0\n");
		return 1;
	}

	if(mp->path_size == mp->path_capacity){
		if(metapath_request_more_capacity(mp) != 0){
			perror("failed to call metapath_request_more_capacity\n");
			return 1;
		}
	}

	strlen_filter = strlen(filter);
	alloc_size = (strlen_filter + 1) * sizeof(ch_t);

	mp->path[mp->path_size] = (struct metapath_step) {
		.step_destination_index = step_destination_index,
		.edge_key = edge_key,
		.path_sql = path_sql,
		.filter = malloc(alloc_size),
	};

	if(mp->path[mp->path_size].filter == NULL){
		perror("failed to malloc\n");
		return 1;
	}

	// memset(mp->path[mp->path_size].filter, '\0', alloc_size);
	memcpy(mp->path[mp->path_size].filter, filter, strlen_filter);
	mp->path[mp->path_size].filter[strlen_filter] = '\0';

	mp->path_size++;

	return 0;
}

void type_node_reset_count_end(struct type_node* const tn){
	tn_size_t i;

	for(i = 0 ; i < tn->node_size ; i++){
		tn->nodes[i].count_end = 0;
	}
}

void type_node_update_total_instance_count(struct type_node* const tn){
	tn_size_t i;
	uint64_t sum;
	uint32_t current_percentile;

	if(!(tn->added_node_since_last_update)){return;}

	tn->total_instance_count = 0;
	// tn->total_instance_count_end = 0;
	for(i = 0 ; i < tn->node_size ; i++){
		tn->total_instance_count += (uint64_t) tn->nodes[i].count;
		// tn->total_instance_count_end += (uint64_t) tn->nodes[i].count_end;
	}

	sum = 0;
	tn->percentiles[0] = 0;
	current_percentile = 1;
	for(i = 0 ; i < tn->node_size ; i++){
		sum += (uint64_t) tn->nodes[i].count;
		if(((double) sum) / ((double) tn->total_instance_count) > (0.01 * ((double) current_percentile))){
			tn->percentiles[current_percentile] = i;
			tn->percentile_sums[current_percentile] = (uint32_t) sum - tn->nodes[i].count;
			current_percentile++;
		}
	}

	tn->added_node_since_last_update = 0;
}

void type_node_update_total_instance_count_end(struct type_node* const tn){
	tn_size_t i;

	tn->total_instance_count_end = 0;

	for(i = 0 ; i < tn->node_size ; i++){
		tn->total_instance_count_end += (uint64_t) tn->nodes[i].count_end;
	}
}

tn_size_t type_node_random_index(const struct type_node* const tn){
	int32_t rand_num, percentile;
	double ratio_rand, ratio_actual;
	uint64_t sum;
	tn_size_t i;

	rand_num = rand();
	ratio_rand = ((double) rand_num) / ((double) RAND_MAX);

	percentile = (int32_t) floor(ratio_rand * 100.0);

	sum = tn->percentile_sums[percentile];
	for(i = tn->percentiles[(int32_t) floor(ratio_rand * 100.0)] ; i < tn->node_size ; i++){
		sum += (uint64_t) tn->nodes[i].count;
		ratio_actual = ((double) sum) / ((double) tn->total_instance_count);
		if(ratio_actual >= ratio_rand){break;}
	}

	return i;
}

/* // DO NOT REMOVE
void multigraph_node_update_edge_count(struct multigraph_node* const mgn){
	uint64_t sum;
	mgn_size_t i;
	uint32_t j;

	sum = 0;
	for(i = 0 ; i < mgn->edge_dim0_size ; i++){
		for(j = 0 ; j < mgn->edge_dim1_size[i] ; j++){
			sum += (uint64_t) mgn->edges[i][j];
		}
	}

	mgn->edge_count = (uint32_t) sum;
}
*/

/* // DO NOT REMOVE
mgn_size_t multigraph_node_random_edge_index(struct multigraph_node* const mgn, const tn_size_t edge_dst){
	uint64_t sum1, sum2;
	tn_size_t i;
	int32_t rand_num;
	double ratio_rand, ratio_actual;

	sum1 = 0;

	for(i = 0 ; i < mgn->edge_dim1_size[edge_dst] ; i++){
		sum1 += (uint64_t) mgn->edges[edge_dst][i];
	}

	if(sum1 == 0){
		return -1;
	}

	rand_num = rand();
	ratio_rand = ((double) rand_num) / ((double) RAND_MAX);

	sum2 = 0;
	for(i = 0 ; i < mgn->edge_dim1_size[edge_dst] ; i++){
		sum2 += (uint64_t) mgn->edges[edge_dst][i];
		ratio_actual = ((double) sum2) / ((double) mgn->edge_count);
		if(ratio_actual > ratio_rand){break;}
	}

	return i;
}
*/

int32_t multigraph_edge_cmp(const void* const a, const void* const b){
	tn_index_t tn_diff; //, local_diff;

	int32_t strcmp_result;

	strcmp_result = strcmp(((struct multigraph_edge*) a)->key_edge, ((struct multigraph_edge*) b)->key_edge);

	if(strcmp_result != 0){return strcmp_result;}

	tn_diff = (((struct multigraph_edge*) a)->tn_dst_index) - (((struct multigraph_edge*) b)->tn_dst_index);

	if(tn_diff < 0){
		return -1;
	} else if(tn_diff > 0){
		return 1;
	} else {
		return strcmp(((struct multigraph_edge*) a)->key_tgt, ((struct multigraph_edge*) b)->key_tgt);
	}
}

mgn_size_t multigraph_node_random_edge_index_v2(struct multigraph_node* const mgn, const tn_size_t edge_dst, const ch_t* const edge_key){
	uint64_t sum1, sum2;
	tn_size_t i;
	int32_t rand_num;
	double ratio_rand, ratio_actual;

	sum1 = 0;

	for(i = 0 ; i < mgn->edge_size ; i++){
		if(mgn->edges[i].tn_dst_index != edge_dst || (edge_key != NULL && strcmp(mgn->edges[i].key_edge, edge_key) != 0)){continue;}
		sum1 += (uint64_t) mgn->edges[i].passes;
	}

	if(sum1 == 0){
		return -1;
	}

	rand_num = rand();
	ratio_rand = ((double) rand_num) / ((double) RAND_MAX);

	sum2 = 0;
	for(i = 0 ; i < mgn->edge_size ; i++){
		if(mgn->edges[i].tn_dst_index != edge_dst || (edge_key != NULL && strcmp(mgn->edges[i].key_edge, edge_key) != 0)){continue;}
		sum2 += (uint64_t) mgn->edges[i].passes;
		ratio_actual = ((double) sum2) / ((double) sum1);
		if(ratio_actual > ratio_rand){return i;}
	}

	return -1;
}

int32_t multigraph_node_cmp(const void* const a, const void* const b){
	return strcmp(((struct multigraph_node*) a)->key, ((struct multigraph_node*) b)->key);
}

int32_t type_node_create(struct type_node* const tn, const ch_t* const key, const tn_size_t node_capacity_step){
	static tn_index_t num_types = 0;

	size_t alloc_size_key, alloc_size_nodes, str_len;

	str_len = strlen(key);

	alloc_size_key = (str_len + 1) * sizeof(ch_t);
	alloc_size_nodes = TYPE_NODE_CAPACITY_BASE * sizeof(struct multigraph_node);

	(*tn) = (struct type_node) {
		.index = num_types,
		.key = malloc(alloc_size_key),
		.node_capacity = TYPE_NODE_CAPACITY_BASE,
		.node_size = 0,
		.node_sorted_size = 0,
		.nodes = malloc(alloc_size_nodes),
		.added_node_since_last_update = 1, // !
		.node_capacity_step = node_capacity_step,
	};

	if(tn->key == NULL || tn->nodes == NULL){
		perror("failed to malloc\n");
		if(tn->key != NULL){free(tn->key);}
		if(tn->nodes != NULL){free(tn->nodes);}
		return 1;
	}

	strncpy(tn->key, key, str_len);

	tn->key[str_len] = '\0';

	num_types++;

	// tn->nodes_are_sorted = 0;

	return 0;
}

tn_size_t type_node_get_index(struct type_node* const tn, const ch_t* const key){
	tn_size_t lower_bound, higher_bound, index;

	lower_bound = 0;
	higher_bound = tn->node_sorted_size;

	while(lower_bound < higher_bound){
		index = lower_bound + (((higher_bound - lower_bound) - ((higher_bound - lower_bound) % 2)) >> 1);
		int32_t cmp_res = strcmp(tn->nodes[index].key, key);
		if(cmp_res == 0){
			return index;
		} else if(cmp_res < 0){
			lower_bound = index + 1;
			index++;
		} else {
			higher_bound = index;
		}
	}

	for(index = tn->node_sorted_size ; index < tn->node_size ; index++){
		if(strcmp(tn->nodes[index].key, key) == 0){
			return index;
		}
	}
	
	return -1;
}

mg_size_t multigraph_get_index(struct multigraph* const mg, const ch_t* const key){
	/* // DO NOT REMOVE
	mg_size_t lower_bound, higher_bound, index;

	lower_bound = 0;
	higher_bound = mg->node_size;

	while(lower_bound < higher_bound){
		index = lower_bound + (((higher_bound - lower_bound) - ((higher_bound - lower_bound) % 2)) >> 1);
		int32_t cmp_res = strcmp(mg->type_nodes[index].key, key);
		if(cmp_res == 0){
			return index;
		} else if(cmp_res < 0){
			lower_bound = index + 1;
			index++;
		} else {
			higher_bound = index;
		}
	}
	*/

	mg_size_t i;

	for(i = 0 ; i < mg->node_size ; i++){
		// printf("trying comparison: %s / %s\n", mg->type_nodes[i].key, key);
		if(strcmp(mg->type_nodes[i].key, key) == 0){
			return i;
		}
	}

	return -1;
}

int32_t type_node_request_more_node_capacity(struct type_node* const tn){
	size_t alloc_size;
	tn_size_t new_capacity;
	struct multigraph_node* tmp_nodes;
	tn_size_t a_index, b_index, tmp_index;

	new_capacity = tn->node_capacity + tn->node_capacity_step;

	alloc_size = ((size_t) new_capacity) * sizeof(struct multigraph_node);

	tn->nodes = realloc(tn->nodes, alloc_size);

	if(tn->nodes == NULL){
		perror("failed to realloc\n");
		return 1;
	}

	memset(&(tn->nodes[tn->node_size]), '\0', tn->node_capacity_step * sizeof(struct multigraph_node));

	tn->node_capacity = new_capacity;

	// qsort(tn->nodes, tn->node_size, sizeof(struct multigraph_node), multigraph_node_cmp); // old way, quicksort the whole thing
	
	qsort(&(tn->nodes[tn->node_sorted_size]), tn->node_size - tn->node_sorted_size, sizeof(struct multigraph_node), multigraph_node_cmp); // sorting the cache
	alloc_size = tn->node_size * sizeof(struct multigraph_node);
	tmp_nodes = malloc(alloc_size);
	if(tmp_nodes == NULL){
		perror("failed to malloc\n");
		return 1;
	}
	tmp_index = 0;
	a_index = 0;
	b_index = tn->node_sorted_size;
	while(tmp_index < tn->node_size){
		if(a_index < tn->node_sorted_size){
			if(b_index < tn->node_size){
				int32_t cmp_res;
				cmp_res = multigraph_node_cmp(&(tn->nodes[a_index]), &(tn->nodes[b_index]));
				if(cmp_res <= 0){
					tmp_nodes[tmp_index] = tn->nodes[a_index];
					a_index++;
				} else {
					tmp_nodes[tmp_index] = tn->nodes[b_index];
					b_index++;
				}
			} else {
				tmp_nodes[tmp_index] = tn->nodes[a_index];
				a_index++;
			}
		} else {
			tmp_nodes[tmp_index] = tn->nodes[b_index];
			b_index++;
		}
		tmp_index++;
	}

	memcpy(tn->nodes, tmp_nodes, alloc_size);

	free(tmp_nodes);

	tn->node_sorted_size = tn->node_size;

	return 0;
}

int32_t multigraph_node_create(struct multigraph_node* const mgn, const ch_t* const key){
	size_t	// alloc_size_edges,
		alloc_size_key,
		key_size;

	key_size = strlen(key);
	alloc_size_key = (key_size + 1) * sizeof(ch_t);

	(*mgn) = (struct multigraph_node) {
		.key_size = (mgn_size_t) key_size,
		.key = malloc(alloc_size_key),
		.count = 0,
	};

	if(mgn->key == NULL){
		perror("failed to malloc\n");
		if(mgn->key != NULL){free(mgn->key);}
		return 1;
	}

	strncpy(mgn->key, key, key_size);
	mgn->key[key_size] = '\0';

	return 0;
}
int32_t type_node_add_node(struct type_node* const tn, const ch_t* const key){
	if(tn->node_size == tn->node_capacity){
		if(type_node_request_more_node_capacity(tn) != 0){
			perror("failed to call type_node_request_more_node_capacity\n");
			return 1;
		}
	}

	if(multigraph_node_create(&(tn->nodes[tn->node_size]), key) != 0){
		perror("failed to call multigraph_node_create\n");
		return 1;
	}

	tn->node_size++;

	tn->added_node_since_last_update = 1;

	// tn->nodes_are_sorted = 0;

	return 0;
}

int32_t type_node_add_node_if_absent(struct type_node* const tn, const ch_t* const key, int64_t* const pointer_index){
	/**/ // DO NOT REMOVE
	/*
	if(tn->nodes_are_sorted == 0){
		qsort(tn->nodes, (size_t) tn->node_size, sizeof(struct multigraph_node), multigraph_node_cmp);
		tn->nodes_are_sorted = 1;
	}
	*/

	tn_size_t index;

	index = type_node_get_index(tn, key);
	if(index >= 0){
		if(tn->nodes[index].count < UINT32_MAX){
			tn->nodes[index].count++;
		}
		(*pointer_index) = index;
		return 0;
	}

	if(tn->node_size == tn->node_capacity){
		if(type_node_request_more_node_capacity(tn) != 0){
			perror("failed to call type_node_request_more_node_capacity\n");
			return 1;
		}
	}

	index = tn->node_size;

	if(multigraph_node_create(&(tn->nodes[index]), key) != 0){
		perror("failed to call multigraph_node_create\n");
		return 1;
	}

	if(tn->nodes[index].count < UINT32_MAX){
		tn->nodes[index].count++;
	}
	(*pointer_index) = index;

	tn->node_size++;

	return 0;
}

int32_t multigraph_create(struct multigraph* const mg){
	size_t alloc_size;

	alloc_size = MULTIGRAPH_CAPACITY_BASE * sizeof(struct type_node);

	(*mg) = (struct multigraph) {
		.node_capacity = MULTIGRAPH_CAPACITY_BASE,
		.node_size = 0,
		.type_nodes = malloc(alloc_size),
	};

	if(mg->type_nodes == NULL){
		perror("failed to malloc\n");
		return 1;
	}

	memset(mg->type_nodes, '\0', alloc_size);

	return 0;
}

int32_t multigraph_request_more_type_node_capacity(struct multigraph* const mg){
	size_t alloc_size;

	alloc_size = (mg->node_capacity + MULTIGRAPH_CAPACITY_STEP) * sizeof(struct type_node);

	mg->type_nodes = realloc(mg->type_nodes, alloc_size);

	if(mg->type_nodes == NULL){
		perror("failed to malloc\n");
		return 1;
	}

	memset(&(mg->type_nodes[mg->node_capacity]), '\0', MULTIGRAPH_CAPACITY_STEP * sizeof(struct type_node));

	mg->node_capacity += MULTIGRAPH_CAPACITY_STEP;

	return 0;
}

int32_t multigraph_add_type_node(struct multigraph* const mg, const ch_t* const key, const tn_size_t node_capacity_step){
	if(mg->node_size == mg->node_capacity){
		if(multigraph_request_more_type_node_capacity(mg) != 0){
			perror("multigraph_request_more_type_node_capacity\n");
			return 1;
		}
	}

	if(type_node_create(&(mg->type_nodes[mg->node_size]), key, node_capacity_step) != 0){
		perror("failed to call type_node_create\n");
		return 1;
	}

	mg->node_size++;

	return 0;
}

void multigraph_edge_free(struct multigraph_edge* const mge){
	free(mge->key_tgt);
}

void multigraph_node_free(struct multigraph_node* const mgn){
	for(uint32_t i = 0 ; i < mgn->edge_size ; i++){
		multigraph_edge_free(&(mgn->edges[i]));
	}

	free(mgn->edges);
	free(mgn->key);
}

void type_node_free(struct type_node* const tn){
	tn_size_t i;

	for(i = 0 ; i < tn->node_size ; i++){
		multigraph_node_free(&(tn->nodes[i]));
	}

	free(tn->nodes);
	free(tn->key);
}

void multigraph_free(struct multigraph* const mg){
	mg_size_t i;

	for(i = 0 ; i < mg->node_size ; i++){
		type_node_free(&(mg->type_nodes[i]));
	}

	free(mg->type_nodes);
}

// DO NOT REMOVE
int32_t multigraph_node_request_more_edge_capacity(struct multigraph_node* const mgn){
	size_t new_capacity, alloc_size;
	mgn_size_t a_index, b_index, tmp_index;
	struct multigraph_edge* tmp_edges;

	new_capacity = mgn->edge_capacity + MULTIGRAPH_NODE_EDGE_CAPACITY_STEP;

	alloc_size = new_capacity * sizeof(struct multigraph_edge);

	mgn->edges = realloc(mgn->edges, alloc_size);

	if(mgn->edges == NULL){
		perror("failed to realloc\n");
		return 1;
	}

	memset(&(mgn->edges[mgn->edge_capacity]), '\0', MULTIGRAPH_NODE_EDGE_CAPACITY_STEP * sizeof(struct multigraph_edge));

	mgn->edge_capacity = (mgn_size_t) new_capacity;

	// structure
	
	qsort(&(mgn->edges[mgn->edge_sorted_size]), mgn->edge_size - mgn->edge_sorted_size, sizeof(struct multigraph_edge), multigraph_edge_cmp);
	alloc_size = mgn->edge_size * sizeof(struct multigraph_edge);
	tmp_edges = malloc(alloc_size);
	if(tmp_edges == NULL){
		perror("failed to malloc\n");
		return 1;
	}

	tmp_index = 0;
	a_index = 0;
	b_index = mgn->edge_sorted_size;
	while(tmp_index < mgn->edge_size){
		if(a_index < mgn->edge_sorted_size){
			if(b_index < mgn->edge_size){
				int32_t cmp_res;
				cmp_res = multigraph_edge_cmp(&(mgn->edges[a_index]), &(mgn->edges[b_index]));
				if(cmp_res <= 0){
					tmp_edges[tmp_index] = mgn->edges[a_index];
					a_index++;
				} else {
					tmp_edges[tmp_index] = mgn->edges[b_index];
					b_index++;
				}
			} else {
				tmp_edges[tmp_index] = mgn->edges[a_index];
				a_index++;
			}
		} else {
			tmp_edges[tmp_index] = mgn->edges[b_index];
			b_index++;
		}
		tmp_index++;
	}

	memcpy(mgn->edges, tmp_edges, alloc_size);

	free(tmp_edges);

	mgn->edge_sorted_size = mgn->edge_size;

	return 0;
}


//int32_t multigraph_node_add_edge(struct multigraph_node* const mgn, tn_index_t edge_dst, tn_index_t local_index){
//	/* // DO NOT REMOVE
//	if(mgn->edge_size == mgn->edge_capacity){
//		if(multigraph_node_request_more_edge_capacity(mgn) != 0){
//			perror("failed to call multigraph_node_request_more_edge_capacity\n");
//			return 1;
//		}
//	}
//
//	mgn->edges[mgn->edge_size] = (struct multigraph_edge) {
//		.tn_dst_index = edge_dst,
//		.local_dst_index = local_index,
//	};
//
//	mgn->edge_size++;
//
//	return 0;
//	*/
//
//	size_t alloc_size;
//	void* malloc_pointer;
//	// uint32_t i;
//
//	// uint32_t new_dim0_capacity;
//	uint32_t new_dim0_size;
//
//	// printf("trying to add edge to %li / %li\n", edge_dst, local_index);
//	// printf("mgn->edge_dim0_capacity: %u\n", mgn->edge_dim0_capacity);
//	// printf("mgn->edge_dim0_size: %u\n", mgn->edge_dim0_size);
//
//	// if(edge_dst >= mgn->edge_dim0_capacity){
//	if(edge_dst >= mgn->edge_dim0_size){
//
//		// dim0
//
//		// new_dim0_capacity = (edge_dst + 1); // + 1 because it's an index and not a length
//		new_dim0_size = (edge_dst + 1); // + 1 because it's an index and not a length
//		// printf("unfit, increasing dim0 capacity to %u\n", new_dim0_capacity);
//		// printf("unfit, increasing dim0 size to %u\n", new_dim0_size);
//		// alloc_size = new_dim0_capacity * sizeof(tn_index_t*);
//		alloc_size = new_dim0_size * sizeof(tn_index_t*);
//		if(mgn->edges == NULL){
//			malloc_pointer = malloc(alloc_size);
//		} else {
//			malloc_pointer = realloc(mgn->edges, alloc_size);
//		}
//		if(malloc_pointer == NULL){
//			perror("failed to realloc\n");
//			// ?
//			/*
//			for(i = 0 ; i < mgn->edge_dim1_size ; i++){
//				free(mgn->edges[i]);
//			}
//			free(mgn->edges);
//			*/
//			return 1;
//		}
//		mgn->edges = malloc_pointer;
//		// memset(&(mgn->edges[mgn->edge_dim0_capacity]), '\0', (new_dim0_capacity - mgn->edge_dim0_capacity) * sizeof(tn_size_t*));
//		memset(&(mgn->edges[mgn->edge_dim0_size]), '\0', (new_dim0_size - mgn->edge_dim0_size) * sizeof(tn_size_t*));
//
//		// dim1
//
//		/*
//		// capacity
//
//		alloc_size = new_dim0_capacity * sizeof(uint32_t);
//		malloc_pointer = realloc(mgn->edge_dim1_capacity, alloc_size);
//		if(malloc_pointer == NULL){
//			perror("realloc failed\n");
//			return 1;
//		}
//		mgn->edge_dim1_capacity = malloc_pointer;
//		memset(&(mgn->edge_dim1_capacity[mgn->edge_dim0_capacity]), '\0', (new_dim0_capacity - mgn->edge_dim0_capacity) * sizeof(uint32_t));
//		*/
//
//		// size
//
//		// alloc_size = new_dim0_capacity * sizeof(uint32_t);
//		alloc_size = new_dim0_size * sizeof(uint32_t);
//		if(mgn->edge_dim1_size == NULL){
//			malloc_pointer = malloc(alloc_size);
//		} else {
//			malloc_pointer = realloc(mgn->edge_dim1_size, alloc_size);
//		}
//		if(malloc_pointer == NULL){
//			perror("realloc failed\n");
//			return 1;
//		}
//		mgn->edge_dim1_size = malloc_pointer;
//		// memset(&(mgn->edge_dim1_size[mgn->edge_dim0_capacity]), '\0', (new_dim0_capacity - mgn->edge_dim0_capacity) * sizeof(uint32_t));
//		memset(&(mgn->edge_dim1_size[mgn->edge_dim0_size]), '\0', (new_dim0_size - mgn->edge_dim0_size) * sizeof(uint32_t));
//
//
//		// mgn->edge_dim0_capacity = new_dim0_capacity;
//		mgn->edge_dim0_size = new_dim0_size;
//		
//	}
//
//	// printf("dim0 done\n");
//
//	// if(mgn->edges[edge_dst] == NULL){
//	// if(mgn->edges_dim1_){
//	// if(local_index >= mgn->edge_dim1_capacity[edge_dst]){
//	if(local_index >= mgn->edge_dim1_size[edge_dst]){
//		/*
//		alloc_size = (local_index + 1) * sizeof(uint32_t);
//		mgn->edges[edge_dst] = realloc(mgn->edges[edge_dst], alloc_size);
//		if(mgn->edges[edge_dst] == NULL){
//			perror("failed to realloc\n");
//			return 1;
//		}
//		// memset(&(mgn->edges[edge_dst][mgn->edge_dim1_capacity[edge_dst]]), '\0', (local_index - mgn->edge_dim1_capacity[edge_dst]) * sizeof(uint32_t));
//		memset(&(mgn->edges[edge_dst][mgn->edge_dim1_capacity[edge_dst]]), '\0', ((local_index + 1) - mgn->edge_dim1_capacity[edge_dst]) * sizeof(uint32_t));
//		*/
//
//		// alloc_size = (local_index + 1) * sizeof(uint32_t); // have to put +1 because it's an index and not a length
//		alloc_size = (local_index + 1) * sizeof(tn_index_t); // have to put +1 because it's an index and not a length
//		// printf("unfit, increasing dim1 capacity to %lu\n", local_index + 1);
//		// printf("unfit, increasing dim1 size to %lu\n", local_index + 1);
//		if(mgn->edges[edge_dst] == NULL){
//			mgn->edges[edge_dst] = malloc(alloc_size);
//		} else {
//			mgn->edges[edge_dst] = realloc(mgn->edges[edge_dst], alloc_size);
//		}
//		if(mgn->edges[edge_dst] == NULL){
//			perror("failed to realloc\n");
//			return 1;
//		}
//		// memset(&(mgn->edges[edge_dst][mgn->edge_dim1_size[edge_dst]]), '\0', (local_index - mgn->edge_dim1_size[edge_dst]) * sizeof(uint32_t));
//		// memset(&(mgn->edges[edge_dst][mgn->edge_dim1_size[edge_dst]]), '\0', ((local_index + 1) - mgn->edge_dim1_size[edge_dst]) * sizeof(uint32_t));
//		memset(&(mgn->edges[edge_dst][mgn->edge_dim1_size[edge_dst]]), '\0', ((local_index + 1) - mgn->edge_dim1_size[edge_dst]) * sizeof(tn_index_t));
//
//		// mgn->edge_dim1_capacity[edge_dst] = local_index;
//		// mgn->edge_dim1_size[edge_dst] = local_index;
//		mgn->edge_dim1_size[edge_dst] = (local_index + 1);
//	}
//
//	// printf("dim1 done\n");
//
//	mgn->edges[edge_dst][local_index]++;
//
//	// printf("incremented\n");
//
//	/*
//	for(uint32_t i = 0 ; i < mgn->edge_dim1_size[edge_dst] ; i++){
//		printf("%i: %li\n", i, mgn->edges[edge_dst][i]);
//	}
//	*/
//
//	return 0;
//}

mgn_size_t multigraph_node_find_edge(const struct multigraph_node* const mgn, const struct multigraph_edge* const mge){
	mgn_size_t lower_bound, higher_bound, index;
	int32_t cmp_res;

	lower_bound = 0;
	higher_bound = mgn->edge_size;

	while(lower_bound < higher_bound){
		index = lower_bound + (((higher_bound - lower_bound) % (higher_bound - lower_bound)) >> 1);
		cmp_res = multigraph_edge_cmp(&(mgn->edges[index]), mge);
		if(cmp_res == 0){
			return index;
		} else if(cmp_res < 0){
			lower_bound = index + 1;
		} else {
			higher_bound = index;
		}
	}

	for(index = mgn->edge_sorted_size ; index < mgn->edge_size ; index++){
		if(multigraph_edge_cmp(&(mgn->edges[index]), mge) == 0){
			return index;
		}
	}

	return -1;
}

int32_t multigraph_node_add_edge_v2(struct multigraph_node* const mgn, tn_index_t edge_dst, const ch_t* const key_edge, const ch_t* const key_tgt){
	struct multigraph_edge mge;
	mgn_size_t index;

	size_t len_key_tgt, alloc_size;

	len_key_tgt = strlen(key_tgt);

	alloc_size = (len_key_tgt + 1) * sizeof(ch_t);

	mge = (struct multigraph_edge) {
		.key_edge = key_edge,
		.tn_dst_index = edge_dst,
		.key_tgt = malloc(alloc_size),
	};

	if(mge.key_tgt == NULL){
		perror("failed to malloc\n");
		return 1;
	}
	memcpy(mge.key_tgt, key_tgt, len_key_tgt);
	mge.key_tgt[len_key_tgt] = '\0';

	index = multigraph_node_find_edge(mgn, &mge);

	if(index == -1){
		if(mgn->edge_size == mgn->edge_capacity){
			if(multigraph_node_request_more_edge_capacity(mgn) != 0){
				perror("failed to call multigraph_node_request_more_edge_capacity\n");
				return 1;
			}
		}

		mgn->edges[mgn->edge_size] = mge;
		mgn->edges[mgn->edge_size].passes = 1;
		mgn->edge_size++;

		// qsort(mgn->edges, mgn->edge_size, sizeof(struct multigraph_edge), multigraph_edge_cmp);
	} else {
		mgn->edges[index].passes++;
		multigraph_edge_free(&mge);
	}

	return 0;
}

// DO NOT REMOVE
int32_t multigraph_individual_random_path(struct multigraph* const mg, struct metapath* const mp, const uint64_t num_iter, const ch_t* const key){
	mg_size_t i;
	uint64_t j;
	uint32_t k;
	mg_size_t current_tn;
	tn_size_t base_index, local_index;
	mgn_size_t rand_edge;

	// base_index = type_node_get_index(&(mg->type_nodes[mp->path[0]]), key);
	base_index = type_node_get_index(&(mg->type_nodes[mp->path[0].step_destination_index]), key);

	if(base_index < 0){
		perror("unknown key\n");
		return 1;
	}

	for(i = 0 ; i < mg->node_size ; i++){
		type_node_reset_count_end(&(mg->type_nodes[i]));
		type_node_update_total_instance_count(&(mg->type_nodes[i]));
	}

	for(j = 0 ; j < num_iter ; j++){
		k = 0;
		local_index = base_index;
		// current_tn = mp->path[k];
		current_tn = mp->path[k].step_destination_index;
		while(k < mp->path_size - 1){
			// rand_edge = multigraph_node_random_edge_index_v2(&(mg->type_nodes[current_tn].nodes[local_index]), mp->path[k+1]);
			rand_edge = multigraph_node_random_edge_index_v2(&(mg->type_nodes[current_tn].nodes[local_index]), mp->path[k+1].step_destination_index, mp->path[k+1].edge_key);
			if(rand_edge == -1){break;}

			// local_index = type_node_get_index(&(mg->type_nodes[mp->path[k+1]]), mg->type_nodes[mp->path[k]].nodes[local_index].edges[rand_edge].key_tgt);
			local_index = type_node_get_index(&(mg->type_nodes[mp->path[k+1].step_destination_index]), mg->type_nodes[mp->path[k].step_destination_index].nodes[local_index].edges[rand_edge].key_tgt);
			// current_tn = mp->path[k+1];
			current_tn = mp->path[k+1].step_destination_index;

			k++;
		}

		mg->type_nodes[current_tn].nodes[local_index].count_end++;

		if(j == UINT64_MAX){break;}
	}

	type_node_update_total_instance_count_end(&(mg->type_nodes[mp->path[mp->path_size - 1].step_destination_index])); // !

	return 0;
}

/*
void multigraph_mean_random_path(struct multigraph* const mg, struct metapath* const mp, const uint64_t num_iter){
	mg_size_t i;
	uint64_t j;
	uint32_t k;
	mg_size_t current_tn;
	tn_size_t base_index, local_index, m;
	mgn_size_t rand_edge;

	base_index = type_node_get_index(&(mg->type_nodes[mp->path[0]]), key);

	for(i = 0 ; i < mg->node_size ; i++){
		type_node_reset_count_end(&(mg->type_nodes[i]));
		type_node_update_total_instance_count(&(mg->type_nodes[i]));
	}

	for(m = 0 ; m < mg->type_nodes[mp->path[0]].node_size ; m++){
		for(j = 0 ; j < num_iter ; j++){
			k = 0;
			local_index = m;
			current_tn = mp->path[k];
			while(k < mp->path_size - 1){
				rand_edge = multigraph_node_random_edge_index_v2(&(mg->type_nodes[current_tn].nodes[local_index]), mp->path[k+1]);
				if(rand_edge == -1){break;}
	
				local_index = type_node_get_index(&(mg->type_nodes[mp->path[k+1]]), mg->type_nodes[mp->path[k]].nodes[local_index].edges[rand_edge].key_tgt);
				current_tn = mp->path[k+1];
	
				k++;
			}
	
			mg->type_nodes[current_tn].nodes[local_index].count_end++;
	
			if(j == UINT64_MAX){break;}
		}
	}
}
*/

void multigraph_collective_random_path(struct multigraph* const mg, struct metapath* const mp, const uint64_t num_iter){
	mg_size_t i;
	uint64_t j;
	uint32_t k;
	mg_size_t current_tn;
	tn_size_t local_index;
	mgn_size_t rand_edge;

	for(i = 0 ; i < mg->node_size ; i++){
		type_node_reset_count_end(&(mg->type_nodes[i]));
		type_node_update_total_instance_count(&(mg->type_nodes[i]));
	}

	for(j = 0 ; j < num_iter ; j++){
		k = 0;
		// current_tn = mp->path[k];
		current_tn = mp->path[k].step_destination_index;
		local_index = type_node_random_index(&(mg->type_nodes[current_tn]));
		while(k < mp->path_size - 1){
			// rand_edge = multigraph_node_random_edge_index_v2(&(mg->type_nodes[current_tn].nodes[local_index]), mp->path[k+1]);
			rand_edge = multigraph_node_random_edge_index_v2(&(mg->type_nodes[current_tn].nodes[local_index]), mp->path[k+1].step_destination_index, mp->path[k+1].edge_key);

			if(rand_edge == -1){break;}

			// local_index = type_node_get_index(&(mg->type_nodes[mp->path[k+1]]), mg->type_nodes[mp->path[k]].nodes[local_index].edges[rand_edge].key_tgt);
			local_index = type_node_get_index(&(mg->type_nodes[mp->path[k+1].step_destination_index]), mg->type_nodes[mp->path[k].step_destination_index].nodes[local_index].edges[rand_edge].key_tgt);
			// current_tn = mp->path[k+1];
			current_tn = mp->path[k+1].step_destination_index;

			k++;
		}

		mg->type_nodes[current_tn].nodes[local_index].count_end++;

		if(j == UINT64_MAX){break;}
	}

	type_node_update_total_instance_count_end(&(mg->type_nodes[mp->path[mp->path_size - 1].step_destination_index])); // !
}

// ========= bridging with graph.h ========

int32_t type_node_count_end_to_graph(const struct type_node* const tn, struct graph* const g, uint8_t ignore_zero_occurrence_nodes){
	tn_size_t i;
	int32_t w2v_index;

	// if(create_graph(g, tn->node_size, 0, FP32) != 0){
	if(create_graph(g, 0, 0, FP32) != 0){
		perror("failed to call create_graph\n");
		return 1;
	}

	for(i = 0 ; i < tn->node_size ; i++){
		if(ignore_zero_occurrence_nodes && tn->nodes[i].count_end == 0){continue;}

		if(g->num_nodes == g->capacity){
			if(request_more_capacity_graph(g) != 0){
				perror("failed to call request_more_graph_capacity\n");
				return 1;
			}
		}

		if(tn->w2v != NULL){
			w2v_index = word2vec_key_to_index(tn->w2v, tn->nodes[i].key);
			if(w2v_index != -1){
				g->nodes[g->num_nodes].word2vec_entry_pointer = &(tn->w2v->keys[w2v_index]);
				g->nodes[g->num_nodes].vector.fp32 = &(tn->w2v->vectors[w2v_index * tn->w2v->num_dimensions]);
				g->nodes[g->num_nodes].num_dimensions = tn->w2v->num_dimensions;
			} else {
				continue;
			}
		}

		g->nodes[g->num_nodes].absolute_proportion = tn->nodes[i].count_end;

		// printf("tn->nodes[%li].count_end: %u\n", i, tn->nodes[i].count_end);

		/*
		// if(g->nodes[g->num_nodes].absolute_proportion <= 0 || isnan(g->nodes[g->num_nodes].absolute_proportion)){
		if(g->nodes[g->num_nodes].absolute_proportion <= 0){
			printf("not normal: %u\n", g->nodes[g->num_nodes].absolute_proportion);
		}
		*/

		g->num_nodes++;
	}

	if(tn->w2v != NULL){
		g->num_dimensions = tn->w2v->num_dimensions;
	}

	compute_graph_relative_proportions(g);

	/*
	for(i = 0 ; i < g->num_nodes ; i++){
		if(g->nodes[i].relative_proportion < 0.0 || isnan(g->nodes[i].relative_proportion)){
			printf("not normal: %f\n", g->nodes[i].relative_proportion);
		}
	}
	*/

	return 0;
}

int32_t multigraph_mean_individual_random_path(struct multigraph* const mg, struct metapath* const mp, const uint64_t num_iter, double* const mean_res){
	double res1, res2;
	double prod, prod_elem;
	tn_size_t i;
	struct graph g;

	prod = 1.0;

	for(i = 0 ; i < mg->type_nodes[mp->path[0].step_destination_index].node_size ; i++){
		if(multigraph_individual_random_path(mg, mp, num_iter, mg->type_nodes[mp->path[0].step_destination_index].nodes[i].key) != 0){
			perror("failed to call multigraph_individual_random_path\n");
			return 1;
		}

		g = (struct graph) { .num_nodes = 0, };

		if(type_node_count_end_to_graph(&(mg->type_nodes[mp->path[mp->path_size - 1].step_destination_index]), &g, 1) != 0){
			perror("failed to call type_node_count_end_to_graph\n");
			return 1;
		}

		shannon_weaver_entropy_from_graph(&g, &res1, &res2);

		if(isnan(res1) || isinf(res1)){continue;}

		prod_elem = pow(res1, ((double) mg->type_nodes[mp->path[0].step_destination_index].nodes[i].count) / ((double) mg->type_nodes[mp->path[0].step_destination_index].total_instance_count));

		if(prod_elem != 0.0){
			prod *= prod_elem;

			// printf("%li | individual sw entropy: %f; prod_elem: %f; prod: %f\n", i, res1, prod_elem, prod);
		}

		free_graph(&g);
	}

	(*mean_res) = prod;

	return 0;
}

#endif
