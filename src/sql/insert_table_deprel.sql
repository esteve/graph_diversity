INSERT INTO t_deprel (
	deprel,
	n
) VALUES (
	?1, -- deprel
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
