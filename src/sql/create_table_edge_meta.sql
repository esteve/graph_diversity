CREATE TABLE IF NOT EXISTS t_edge_meta (
	edge_type	TEXT,
	domain		TEXT,
	codomain	TEXT,
	--CONSTRAINT	t_edge_meta_pk	PRIMARY KEY (edge_type, domain, codomain)
	CONSTRAINT	t_edge_meta_pk	PRIMARY KEY (edge_type)
) STRICT;
