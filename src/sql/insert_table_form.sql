INSERT INTO t_form (
	form,
	n
) VALUES (
	?1, -- form
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
