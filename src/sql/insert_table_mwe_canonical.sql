INSERT INTO t_mwe_canonical (
	mwe_canonical,
	n
) VALUES (
	?1, -- mwe_canonical
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
