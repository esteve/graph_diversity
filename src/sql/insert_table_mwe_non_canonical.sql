INSERT INTO t_mwe_non_canonical (
	mwe_non_canonical,
	n
) VALUES (
	?1, -- mwe_non_canonical
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
