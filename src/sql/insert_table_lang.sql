INSERT INTO t_lang (
	lang,
	n
) VALUES (
	?1, -- lang
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
