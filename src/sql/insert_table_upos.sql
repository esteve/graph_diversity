INSERT INTO t_upos (
	upos,
	n
) VALUES (
	?1, -- upos
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
