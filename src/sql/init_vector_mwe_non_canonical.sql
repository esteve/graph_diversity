SELECT rowid, CAST(n AS FLOAT) / CAST((SELECT SUM(n) FROM t_mwe_non_canonical WHERE mwe_non_canonical REGEXP ?1) AS FLOAT) FROM t_mwe_non_canonical WHERE mwe_non_canonical REGEXP ?1;
