SELECT
	(SELECT rowid FROM t_mwe_non_canonical WHERE mwe_non_canonical=src LIMIT 1) AS a,
	(SELECT rowid FROM t_xpos WHERE xpos=tgt LIMIT 1) AS b,
	CAST(passes AS FLOAT) / (
		--SELECT CAST(SUM(passes) AS FLOAT) FROM (SELECT * FROM t_edge WHERE src REGEXP ?1 AND tgt REGEXP ?2 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="mwe_non_canonical_to_xpos" LIMIT 1)) AS t2 WHERE t1.src=t2.src
		SELECT CAST(n AS FLOAT) FROM t_mwe_non_canonical WHERE mwe_non_canonical=src LIMIT 1
	) AS n
FROM t_edge AS t1 WHERE src REGEXP ?1 AND tgt REGEXP ?2 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="mwe_non_canonical_to_xpos" LIMIT 1);
