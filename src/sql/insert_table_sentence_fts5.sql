INSERT OR IGNORE INTO t_sentence (
	fk_file,
	sent_id,
	raw_form,
	raw_lemma
) VALUES (
	-- (SELECT rowid FROM t_filename WHERE filename=?1), -- fk_file
	?1, -- fk_file
	?2, -- sent_id
	?3, -- raw_form
	?4  -- raw_lemma
);
