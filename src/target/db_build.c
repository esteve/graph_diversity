/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdint.h>
#include <sqlite3.h>

#include "db_interface.h"

#ifndef DB_PATH
#define DB_PATH "data/target/main.db"
#endif

int32_t main(int32_t argc, char** argv){
	sqlite3* db;

	if(argc <= 2 || strlen(argv[1]) != 2){
		perror("ERROR: must be called with syntax\ndb_build EN path_to_file1.cupt path_to_file2.cupt\ndb_build EN *\n");
		return 1;
	}

	printf("Using %s as language\n", argv[1]);

	/*
	char* filepaths[] = {
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/DE/raw_batch-00/raw-001_annote.config250.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/EL/raw_batch-00/raw-001_annote.config170.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/EU/raw_batch-00/raw-001_annote.config171.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-001_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/GA/raw_batch-00/raw-001_annote.config175.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/HE/raw_batch-00/raw-001_annote.config228.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/HI/raw_batch-00/raw-001_annote.config217.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/IT/raw_batch-00/raw-001_annote.config160.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-001_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PT/raw_batch-00/raw-001_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/RO/raw_batch-00/raw-001_annote.config214.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/SV/raw_batch-00/raw-001_annote.config186.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/TR/raw_batch-00/raw-001_annote.config224.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/ZH/raw_batch-00/raw-001_annote.config215.cupt",
	};

	char* langs[] = {
		"DE",
		"EL",
		"EU",
		"FR",
		"GA",
		"HE",
		"HI",
		"IT",
		"PL",
		"PT",
		"RO",
		"SV",
		"TR",
		"ZH",
	};
	*/

	printf("Using SQLITE3 v%s (%s)\n", SQLITE_VERSION, SQLITE_SOURCE_ID);
	printf("Connecting to %s\n", DB_PATH);

	if(sqlite3_open_v2(DB_PATH, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL) != SQLITE_OK){
		fprintf(stderr, "Failed to call sqlite3_open on %s\n", DB_PATH);
		return 1;
	}

	if(db_prepare(db) != 0){
		perror("Failed to call db_prepare\n");
		sqlite3_close(db);
		return 1;
	}

	/*
	for(size_t i = 0 ; i < sizeof(filepaths) / sizeof(filepaths[0]) ; i++){
		if(cupt_to_db_insert(db, filepaths[i], langs[i]) != 0){
			perror("failed to call cupt_to_db_insert\n");
			sqlite3_close(db);
			return 1;
		}
	}
	*/

	for(int32_t i = 2 ; i < argc ; i++){
		if(cupt_to_db_insert(db, argv[i], argv[1]) != 0){
			perror("failed to call cupt_to_db_insert\n");
			sqlite3_close(db);
			return 1;
		}	
	}

	if(ENABLE_EDGE_CONSTRUCTION && LATE_STANDARD_EDGE_COMPUTATION){
		if(db_recompute_standard_edges(db) != 0){
			perror("Failed to call db_recompute_standard_edges\n");
			sqlite3_close(db);
			return 1;
		}
	}

	if(ENABLE_LATE_TOKEN_INDEX_GENERATION){
		if(db_create_token_indices(db) != 0){
			perror("Failed to call db_create_token_indices\n");
			sqlite3_close(db);
			return 1;
		}
	}

	sqlite3_close(db);

	return 0;
}
