SELECT
	(SELECT rowid FROM t_lemma WHERE lemma=src LIMIT 1) AS a,
	(SELECT rowid FROM t_feats WHERE feats=tgt LIMIT 1) AS b,
	CAST(passes AS FLOAT) / (
		--SELECT CAST(SUM(passes) AS FLOAT) FROM (SELECT * FROM t_edge WHERE src REGEXP ?1 AND tgt REGEXP ?2 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="lemma_to_feats" LIMIT 1)) AS t2 WHERE t1.src=t2.src
		SELECT CAST(n AS FLOAT) FROM t_lemma WHERE lemma=src LIMIT 1
	) AS n
FROM t_edge AS t1 WHERE src REGEXP ?1 AND tgt REGEXP ?2 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="lemma_to_feats" LIMIT 1);
