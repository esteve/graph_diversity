--SELECT
--	(SELECT rowid FROM t_feats WHERE feats=src) AS a,
--	(SELECT CAST(SUM(passes) AS FLOAT) / CAST((SELECT SUM(passes) FROM t_edge WHERE src REGEXP ?1 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="feats_to_form")) AS FLOAT)) AS p
--FROM t_edge WHERE src REGEXP ?1 AND fk_edge_type=(SELECT rowid FROM t_edge_meta WHERE edge_type="feats_to_form") GROUP BY src;
SELECT rowid, CAST(n AS FLOAT) / CAST((SELECT SUM(n) FROM t_feats WHERE feats REGEXP ?1) AS FLOAT) FROM t_feats WHERE feats REGEXP ?1;
