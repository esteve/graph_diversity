INSERT INTO t_feats (
	feats,
	n
) VALUES (
	?1, -- feats
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
