INSERT INTO t_xpos (
	xpos,
	n
) VALUES (
	?1, -- xpos
	1
) ON CONFLICT DO UPDATE SET n = n + 1;
