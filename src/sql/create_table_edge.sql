CREATE TABLE IF NOT EXISTS t_edge (
	src		TEXT,
	--edge_type	TEXT,
	fk_edge_type	INTEGER,
	tgt		TEXT,
	passes		INTEGER,
	CONSTRAINT	t_edge_fk_edge_type	FOREIGN KEY (fk_edge_type)		REFERENCES t_edge_meta (edge_type),
	CONSTRAINT	t_edge_pk		PRIMARY KEY (src, fk_edge_type, tgt)
) STRICT;
