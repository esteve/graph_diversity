SELECT fk_sentence, id_raw, (SELECT form FROM t_form WHERE rowid=fk_form LIMIT 1) AS form FROM t_token WHERE fk_sentence IN (
	SELECT DISTINCT fk_sentence FROM t_token WHERE fk_lemma=(
		SELECT rowid FROM t_lemma WHERE lemma=?1 LIMIT 1
	)
) ORDER BY fk_sentence, CAST(id_raw AS INTEGER);
