CREATE TABLE IF NOT EXISTS t_sentence (
	fk_file		INTEGER,
	sent_id		TEXT,
	CONSTRAINT	t_sentence_fk_file	FOREIGN KEY (fk_file)	REFERENCES t_filename (filename),
	CONSTRAINT	t_sentence_unique	UNIQUE (fk_file, sent_id)
) STRICT;
