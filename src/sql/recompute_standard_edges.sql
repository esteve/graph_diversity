INSERT OR REPLACE INTO t_edge (src, fk_edge_type, tgt, passes) (SELECT 
	(SELECT ?4 FROM ?5 WHERE rowid=?1), (SELECT rowid FROM t_edge_meta WHERE edge_type=?2), (SELECT ?6 FROM ?7 WHERE rowid=?3), COUNT(*) AS n
	FROM
	t_token
	GROUP BY
	--?1, ?2
	?1, ?3
);
--ON CONFLICT DO UPDATE SET passes = n;
