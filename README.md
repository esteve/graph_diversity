# DiversGraph

DiversGraph is a tool to measure the diversity of linguistic structures
based on multigraph relations (see Morales et al. (2020)).
It is meant to work with `*.conllu` or related data formats (_e.g._, `*.cupt`).

# Data indexing

Part of DiversGraph is indexing linguistic data, as well as their relations.
This may prove useful for other projects.

## SQL

DiversGraph makes use of SQLite to index linguistic data.
**The pipeline has been tested with SQLite 3.41.2. It is recommended to use at
least SQLite 3.37.0.**

SQL queries are stored under `src/sql/*.sql`.
This includes database creation, indexing, querying, _etc_.

In command-line interface, you may run
```bash
sqlite3 data/target/main.db
```
to have an interface for querying the database.

You may also query the database through the programming language of your choice
such as C
```c
#include <sqlite3.h>
```
or Python
```python
import sqlite3
```
for which we invite you to read dedicated documentation.

### Building the database

A `makefile` is available to simplify pipeline compilation.
Building the database has two steps.

#### Configuring the makefile

In the `makefile`, you may modify some macro-parameters.
* `ENABLE_EARLY_TOKEN_INDEX_GENERATION` and
  `ENABLE_LATE_TOKEN_INDEX_GENERATION` handle whether indices on the `t_token`
  table should be created before or after inserts.
  Recommended: `ENABLE_EARLY_TOKEN_INDEX_GENERATION = 0`,
  `ENABLE_LATE_TOKEN_INDEX_GENERATION = 1`.
* `FILTER_OUT_FORMS` handles whether to ignore forms that contain capital
  letters, digits, and punctuations.
  Recommended: `FILTER_OUT_FORMS = 0`.
* `EARLY_STANDARD_EDGE_COMPUTATION` and `LATE_STANDARD_EDGE_COMPUTATION` handle
  whether standard edges should be computed iteratively or at the end.
  Recommended: `EARLY_STANDARD_EDGE_COMPUTATION = 0` and
  `LATE_STANDARD_EDGE_COMPUTATION = 1`.
* `ENABLE_EDGE_CONSTRUCTION` handles whether edges should be computed or not.
  Recommended: `ENABLE_EDGE_CONSTRUCTION = 1` if you're interested in the
  multigraph, `ENABLE_EDGE_CONSTRUCTION = 0` if you're only interested in the
  indexing of linguistic data.
* `ENABLE_CAP_NUM_SENTENCES` and `CAP_NUM_SENTENCES` handle whether there
  should be a limit to the number of sentences added per CLI call, and how many
  it should be.
  Recommended: depends on your application.
* `ENABLE_SHORT_TRANSACTIONS` and `SHORT_TRANSACTION_SIZE` handle whether
  SQL transactions should have a limited size, and which size it is.
  Recommended: `ENABLE_SHORT_TRANSACTIONS = 0`.
* `DB_PATH` is the path in which the database is stored.
  Recommended: `DB_PATH = data/target/main.db`.

#### Compiling

You may then run
```bash
make compile_db_build
```
which, if successful, will create a binary `bin/target/db_build`.

This has been tested with `clang` 14.0.0.

#### Running

You may then run
```bash
./bin/target/db_build FR path/to/your/files/*.conllu
```
which will consider FR to be the language associated with the files you give
it to process.

### General structure

Table names start with `t_*`:
* `t_form`: mapping forms to a primary key
* `t_lemma`: mapping lemmas to a primary key
* `t_feats`: mapping feats (intersection) to a primary key
* `t_deprel`: mapping deprel to a primary key
* `t_upos`: mapping upos to a primary key
* `t_xpos`: mapping xpos to a primary key
* `t_lang`: mapping lang to a primary key
* `t_token`: individual tokens from the `*.conllu` format, with foreign keys to
  tables described above
* `t_filename`: mapping file names to a primary key
* `t_sentence`: mapping sentences to a primary key and a file name foreign key
* `t_edge_meta`: stores edge types
* `t_edge`: stores actual edge information

With regards to columns, starting with `pk_*` indicates _primary key_, while
starting with `fk_*` indicates _foreign key_.

It should be noted that, unless explicitly set otherwise, the primary key is
SQLite's `rowid`.
A query such as
```sql
SELECT * FROM t_form;
```
will only yield columns `form` and `n`:

|     form      |   n    |
|---------------|--------|
| Algorithmique | 8      |
| .             | 967467 |
| L'            | 50229  |
| algorithmique | 62     |
| est           | 260854 |


To also obtain `rowid`, the query should be
```sql
SELECT rowid, * FROM t_form;
```

| rowid |     form      |   n    |
|-------|---------------|--------|
| 1     | Algorithmique | 8      |
| 2     | .             | 967467 |
| 3     | L'            | 50229  |
| 4     | algorithmique | 62     |
| 5     | est           | 260854 |

(_similarly, index names start with_ `i_*`)

### General queries

Most queries that do not wonder about multigraph edges, just about retrieving
some indexed data, focus on the `t_token` table.
Its columns are (almost) a one-to-one equivalent to the `*.cupt` format.
The main columns don't have raw data, but rather foreign keys pointing at
respective tables (`fk_form` points to `rowid` of `t_form`, `fk_lemma` points
to `rowid` of `t_lemma`, _etc_).

Thus, to retrieve all primary keys of sentences containing a specific form for
example, we must first retrieve its `rowid` from `t_form`:
```sql
SELECT rowid FROM t_form WHERE form="manger" LIMIT 1;
```

| rowid |
|-------|
| 9764  |

We may then put it in a query on the `t_token` table:
```sql
SELECT DISTINCT fk_sentence FROM t_token WHERE fk_form=(
	SELECT rowid FROM t_form WHERE form="manger" LIMIT 1
);
```

| fk_sentence |
|-------------|
| 1777        |
| 2147        |
| 5202        |
| 10180       |
| 11573       |

These may then be used to retrieve the associated tokens in `t_token`.

### Other

It should be noted that heavy querying of specific columns in `t_token` may
require creating an index on said column to improve performance.
See `src/sql/create_index_token_*.sql` for possible indices.


