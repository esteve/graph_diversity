/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "gengraph.h"
#include "gengraph_cli.h"
#include "graph.h"
#include "dfunctions.h"
#include "cupt/parser.h"

#define ENABLE_SENTENCE_INSTANCE 0
#define ENABLE_TOKEN_INSTANCE 0

#define FILTER_OUT_FORMS 1

int32_t main(){
	struct multigraph mg;
	/*
	struct type_node tn;
	int64_t i;
	ch_t key[] = "test type";
	*/

	/*
	if(multigraph_create(&mg) != 0){
		perror("failed to call multigraph_create\n");
		return 1;
	}

	for(i = 0 ; i < 64 ; i++){
		// if(multigraph_request_more_node_capacity(&mg) != 0){
		if(multigraph_request_more_type_node_capacity(&mg) != 0){
			perror("failed to call multigraph_request_more_node_capacity\n");
			multigraph_free(&mg);
			return 1;
		}
	}

	multigraph_free(&mg);

	// ---- type_node test ----
	
	if(type_node_create(&tn, key) != 0){
		perror("failed to call type_node_create\n");
		return 1;
	}

	printf("%s\n", tn.key);

	type_node_free(&tn);
	*/

	// ---- actual use ----
	
	struct word2vec w2v;

	// if(load_word2vec_binary(&w2v, "/home/esteve/Documents/thesis/other_repos/word2vec/bin/MWE_S2S_FR_raw_004-006_011-029_skip-gram_100d.bin") != 0){
	if(load_word2vec_binary(&w2v, "/home/esteve/Documents/thesis/other_repos/word2vec/bin/MWE_S2S_PL_44GB_100d_skip-gram.bin") != 0){
		perror("failed to call load_word2vec_binary\n");
		return 1;
	}

	if(multigraph_create(&mg) != 0){
		perror("failed to call multigraph_create\n");
		return 1;
	}

	mg_size_t	sentence_instance_i,
			token_instance_i,
			form_i,
			lemma_i,
			upos_i,
			xpos_i,
			mwe_instance_i,
			mwe_non_canonical_i,
			mwe_canonical_i,
			lang_i
			;

	if(ENABLE_SENTENCE_INSTANCE){
		if(multigraph_add_type_node(&mg, "sentence_instance", 4096) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
		sentence_instance_i = mg.node_size - 1;
	}
	if(ENABLE_TOKEN_INSTANCE){
		if(multigraph_add_type_node(&mg, "token_instance", 4096) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
		token_instance_i = mg.node_size - 1;
	}
	if(multigraph_add_type_node(&mg, "form", 2048) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	form_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "lemma", 2048) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	lemma_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "upos", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	upos_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "xpos", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	xpos_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_instance", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	mwe_instance_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_non_canonical", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	mwe_non_canonical_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "mwe_canonical", 32) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	mwe_canonical_i = mg.node_size - 1;
	if(multigraph_add_type_node(&mg, "lang", 1) != 0){perror("failed to call multigraph_add_type_node\n"); multigraph_free(&mg);}
	lang_i = mg.node_size - 1;

	/**/
	mg.type_nodes[form_i].w2v = &(w2v);
	mg.type_nodes[lemma_i].w2v = &(w2v);
	mg.type_nodes[mwe_canonical_i].w2v = &(w2v);
	/**/

	for(mg_size_t i = 0 ; i < mg.node_size ; i++){
		printf("%li / %s\n", i, mg.type_nodes[i].key);
	}

	struct cupt_sentence_iterator csi;
	// char* filename = "/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw-batch-01/raw-005_annote.config218.cupt";
	// char* filename = "/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-001_annote.config162.cupt";
	/*
	char* filepaths[] = {
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-001_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-002_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-003_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-004_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-005_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-006_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-007_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-008_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-009_annote.config162.cupt",
	};
	*/
	char* filepaths[] = {
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-001_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-002_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/PL/raw_batch-00/raw-003_annote.config162.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-001_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-002_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-003_annote.config218.cupt",
	};
	/*
	char* filepaths[] = {
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-001_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-002_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-00/raw-003_annote.config218.cupt",
	};
	*/
	/*
	char* filepaths[] = {
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-01/raw-004_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-01/raw-005_annote.config218.cupt",
		"/home/esteve/Documents/thesis/other_repos/Seen2Seen/OUTPUT_seen/ANNOTATIONS/FR/raw_batch-01/raw-006_annote.config218.cupt",
	};
	*/

	char* langs[] = {
		"PL",
		"PL",
		"PL",
		"FR",
		"FR",
		"FR",
	};

	uint64_t sentence_count = 0;

	for(uint64_t p = 0 ; p < sizeof(filepaths) / sizeof(filepaths[0]) ; p++){
		ch_t* filename;
		
		filename = filepaths[p];

		printf("%s\n", filename);

		csi = (struct cupt_sentence_iterator) {};

		if(create_cupt_sentence_iterator(&csi, filename) != 0){
			perror("failed to call create_cupt_sentence_iterator\n");
			multigraph_free(&mg);
			free_cupt_sentence_iterator(&csi);
			return 1;
		}

		if(iterate_cupt_sentence_iterator(&csi) != 0){
			perror("failed to call iterate_cupt_sentence_iterator\n");
			multigraph_free(&mg);
			free_cupt_sentence_iterator(&csi);
			return 1;
		}
	
		int64_t j = 0;
	
		const int64_t mwe_capacity = 32;
		const int64_t mwe_max_num_tokens = 32;
		const int64_t mwe_token_bfr_size = 32;
	
		// while(csi.file_is_done == 0 && j < 2){
		// while(csi.file_is_done == 0 && j < 10){
		// while(csi.file_is_done == 0 && j < 50){
		// while(csi.file_is_done == 0 && j < 100){
		// while(csi.file_is_done == 0 && j < 200){
		while(csi.file_is_done == 0 && j < 1000){
		// while(csi.file_is_done == 0 && j < 5000){
		// while(csi.file_is_done == 0 && j < 10000){ // 0.7s ; 1.2s
		// while(csi.file_is_done == 0 && j < 100000){ // 22s ; 42s (64), 14s (256), 6s (4096)
		// while(csi.file_is_done == 0 && j < 500000){ // forever it seems; 43s
		// while(csi.file_is_done == 0 && j < 1000000){
		// while(csi.file_is_done == 0){
			const uint32_t sentence_instance_key_length = 64;
			ch_t sentence_instance_key[sentence_instance_key_length];
			int64_t index_sentence_instance;
			memset(sentence_instance_key, '\0', sentence_instance_key_length * sizeof(ch_t));
			snprintf(sentence_instance_key, sentence_instance_key_length, "sentence-%lu", sentence_count);

			if(ENABLE_SENTENCE_INSTANCE){
				if(type_node_add_node_if_absent(&(mg.type_nodes[sentence_instance_i]), sentence_instance_key, &index_sentence_instance) != 0){perror("failed to call type_node_add_node\n"); return 1;}
			}

			int64_t mwe_lengths[mwe_capacity];
			memset(mwe_lengths, '\0', mwe_capacity * sizeof(int64_t));
			ch_t mwe_bfr_lemma[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
			memset(mwe_bfr_lemma, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
			ch_t mwe_bfr_form[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
			memset(mwe_bfr_form, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
			ch_t mwe_bfr_upos[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
			memset(mwe_bfr_upos, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
			ch_t mwe_bfr_xpos[mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size];
			memset(mwe_bfr_xpos, '\0', mwe_capacity * mwe_max_num_tokens * mwe_token_bfr_size * sizeof(ch_t));
		
			for(int32_t i = 0 ; i < csi.current_sentence.num_tokens ; i++){
				int64_t index_token_instance, index_form, index_lemma, index_upos, index_xpos, index_mwe_instance, index_lang;
				const uint32_t token_instance_key_length = 64;
				ch_t token_instance_key[token_instance_key_length];

				if(FILTER_OUT_FORMS && strcmp(csi.current_sentence.tokens[i].mwe, "*") == 0){
					uint8_t	found_digit,
						found_punct,
						found_uppercase //,
						// found_lowercase
						;
	
					found_digit = 0;
					found_punct = 0;
					found_uppercase = 0;
					// found_lowercase = 0;
	
					int32_t k = 0;
					while(csi.current_sentence.tokens[i].form[k] != '\0'){
						if('0' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '9'){
							found_digit = 1;
						} else if('A' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= 'Z'){
							found_uppercase = 1;
						} else if(
							('!' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '&') ||
							('(' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '/') ||
							(':' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '@') ||
							('[' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '`') ||
							('{' <= csi.current_sentence.tokens[i].form[k] && csi.current_sentence.tokens[i].form[k] <= '~')
						){
							found_punct = 1;
						}
							
						k++;
					}
	
					if(found_digit || found_punct || found_uppercase){continue;}
				}

				if(type_node_add_node_if_absent(&(mg.type_nodes[lang_i]), langs[p], &index_lang) != 0){perror("failed to call type_node_add_node\n"); return 1;}

				memset(token_instance_key, '\0', token_instance_key_length * sizeof(ch_t));
				snprintf(token_instance_key, token_instance_key_length, "%s_token-%s", sentence_instance_key, csi.current_sentence.tokens[i].id_raw);
	
				if(ENABLE_TOKEN_INSTANCE){
					if(type_node_add_node_if_absent(&(mg.type_nodes[token_instance_i]), token_instance_key, &index_token_instance) != 0){perror("failed to call type_node_add_node\n"); return 1;}
				}
				if(type_node_add_node_if_absent(&(mg.type_nodes[form_i]), csi.current_sentence.tokens[i].form, &index_form) != 0){perror("failed to call type_node_add_node\n"); return 1;}
				if(type_node_add_node_if_absent(&(mg.type_nodes[lemma_i]), csi.current_sentence.tokens[i].lemma, &index_lemma) != 0){perror("failed to call type_node_add_node\n"); return 1;}
				if(type_node_add_node_if_absent(&(mg.type_nodes[upos_i]), csi.current_sentence.tokens[i].upos, &index_upos) != 0){perror("failed to call type_node_add_node\n"); return 1;}
				if(type_node_add_node_if_absent(&(mg.type_nodes[xpos_i]), csi.current_sentence.tokens[i].xpos, &index_xpos) != 0){perror("failed to call type_node_add_node\n"); return 1;}

				/*
				if(strcmp(csi.current_sentence.tokens[i].head, "0") != 0){
					for(int32_t g = 0 ; g < csi.current_sentence.num_tokens ; g++){
						if(strcmp(csi.current_sentence.tokens[i].head, csi.current_sentence.tokens[i].id_raw) == 0){
							if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_FORM_HAS_HEAD_FORM, csi.current_sentence.tokens[g].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}

							break;
						}
					}
				}
				*/



				if(ENABLE_TOKEN_INSTANCE && ENABLE_SENTENCE_INSTANCE){
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[sentence_instance_i].nodes[index_sentence_instance]), (tn_index_t) token_instance_i, MULTIGRAPH_EDGE_KEY_SENTENCE_INSTANCE_TO_TOKEN_INSTANCE, token_instance_key) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[token_instance_i].nodes[index_token_instance]), (tn_index_t) sentence_instance_i, MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_SENTENCE_INSTANCE, sentence_instance_key) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				}
				if(ENABLE_TOKEN_INSTANCE){
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[token_instance_i].nodes[index_token_instance]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[token_instance_i].nodes[index_token_instance]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_LEMMA, csi.current_sentence.tokens[i].lemma) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[token_instance_i].nodes[index_token_instance]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_UPOS, csi.current_sentence.tokens[i].upos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[token_instance_i].nodes[index_token_instance]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_TOKEN_INSTANCE_TO_XPOS, csi.current_sentence.tokens[i].xpos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_FORM_TO_LEMMA, csi.current_sentence.tokens[i].lemma) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_FORM_TO_UPOS, csi.current_sentence.tokens[i].upos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_FORM_TO_XPOS, csi.current_sentence.tokens[i].xpos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lemma_i].nodes[index_lemma]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lemma_i].nodes[index_lemma]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_UPOS, csi.current_sentence.tokens[i].upos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lemma_i].nodes[index_lemma]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_XPOS, csi.current_sentence.tokens[i].xpos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[upos_i].nodes[index_upos]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_UPOS_TO_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[upos_i].nodes[index_upos]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_UPOS_TO_LEMMA, csi.current_sentence.tokens[i].lemma) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[upos_i].nodes[index_upos]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_UPOS_TO_XPOS, csi.current_sentence.tokens[i].xpos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[xpos_i].nodes[index_xpos]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_XPOS_TO_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[xpos_i].nodes[index_xpos]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_XPOS_TO_LEMMA, csi.current_sentence.tokens[i].lemma) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[xpos_i].nodes[index_xpos]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_XPOS_TO_UPOS, csi.current_sentence.tokens[i].upos) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) lang_i, MULTIGRAPH_EDGE_KEY_FORM_TO_LANG, langs[p]) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lemma_i].nodes[index_lemma]), (tn_index_t) lang_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_LANG, langs[p]) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lang_i].nodes[index_lang]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_LANG_TO_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
				if(multigraph_node_add_edge_v2(&(mg.type_nodes[lang_i].nodes[index_lang]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_LANG_TO_LEMMA, csi.current_sentence.tokens[i].lemma) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
	
				if(strcmp(csi.current_sentence.tokens[i].mwe, "*") != 0){
					const int32_t key_size = 8;
					size_t bytes_to_cpy;
					ch_t key[key_size];
					memset(key, '\0', key_size);
					
					ch_t* tok = strtok(csi.current_sentence.tokens[i].mwe, ";");
	
					do {
						int32_t k = 0;
						while(k < key_size - 1){
							if('0' <= tok[k] && tok[k] <= '9'){
								key[k] = tok[k];
							}
							k++;
						}
		
						int64_t mwe_index = strtol(key, NULL, 10);
		
						if(mwe_index < mwe_capacity){
							if(mwe_lengths[mwe_index] < mwe_max_num_tokens){
								bytes_to_cpy = strlen(csi.current_sentence.tokens[i].lemma);
								if(bytes_to_cpy > mwe_token_bfr_size - 1){
									bytes_to_cpy = mwe_token_bfr_size - 1;
								}
								memcpy(&(mwe_bfr_lemma[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].lemma, bytes_to_cpy);
		
								bytes_to_cpy = strlen(csi.current_sentence.tokens[i].form);
								if(bytes_to_cpy > mwe_token_bfr_size - 1){
									bytes_to_cpy = mwe_token_bfr_size - 1;
								}
								memcpy(&(mwe_bfr_form[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].form, bytes_to_cpy);
		
								bytes_to_cpy = strlen(csi.current_sentence.tokens[i].upos);
								if(bytes_to_cpy > mwe_token_bfr_size - 1){
									bytes_to_cpy = mwe_token_bfr_size - 1;
								}
								memcpy(&(mwe_bfr_upos[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].upos, bytes_to_cpy);
		
								bytes_to_cpy = strlen(csi.current_sentence.tokens[i].xpos);
								if(bytes_to_cpy > mwe_token_bfr_size - 1){
									bytes_to_cpy = mwe_token_bfr_size - 1;
								}
								memcpy(&(mwe_bfr_xpos[mwe_index * mwe_max_num_tokens * mwe_token_bfr_size + mwe_lengths[mwe_index] * mwe_token_bfr_size]), csi.current_sentence.tokens[i].xpos, bytes_to_cpy);
								mwe_lengths[mwe_index]++;
							}
						}
		
						const int32_t gen_key_size = 32;
						ch_t gen_key[gen_key_size];
						memset(gen_key, '\0', gen_key_size);
						snprintf(gen_key, gen_key_size, "%li_%s", j, key);
		
						if(type_node_add_node_if_absent(&(mg.type_nodes[mwe_instance_i]), gen_key, &index_mwe_instance) != 0){perror("failed to call type_node_add_node_if_absent\n"); return 1;}
		
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_instance_i].nodes[index_mwe_instance]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_MWE_INSTANCE_TO_FORM, csi.current_sentence.tokens[i].form) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_instance_i].nodes[index_mwe_instance]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_MWE_INSTANCE_TO_FORM, csi.current_sentence.tokens[i].lemma) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
		
						tok = strtok(NULL, ";");
						if(tok == NULL){break;}
	
					} while (1);
				}


			}

			/**/
			// dependencies
			for(int32_t i = 0 ; i < csi.current_sentence.num_tokens ; i++){
				if(strcmp(csi.current_sentence.tokens[i].head, "0") != 0){
					tn_index_t index_form = type_node_get_index(&(mg.type_nodes[form_i]), csi.current_sentence.tokens[i].form);
					for(int32_t g = 0 ; g < csi.current_sentence.num_tokens ; g++){
						if(strcmp(csi.current_sentence.tokens[i].head, csi.current_sentence.tokens[i].id_raw) == 0){
							tn_index_t head_index_form = type_node_get_index(&(mg.type_nodes[form_i]), csi.current_sentence.tokens[g].form);
							if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[index_form]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_FORM_HAS_HEAD_FORM, csi.current_sentence.tokens[g].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
							if(multigraph_node_add_edge_v2(&(mg.type_nodes[form_i].nodes[head_index_form]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_FORM_IS_HEAD_OF_FORM, csi.current_sentence.tokens[i].form) != 0){perror("failed to call multigraph_node_add_edge_v2\n"); multigraph_free(&mg); return 1;}
							break;
						}
					}
				}
			}
			/**/

	
			for(int64_t k = 0 ; k < mwe_capacity ; k++){
				/* // DO NOT REMOVE
				if(mwe_lengths[k] == 1){
					for(int32_t m = 0 ; m < csi.current_sentence.num_tokens ; m++){
						printf("%i: %s / %s\n", m, csi.current_sentence.tokens[m].form, csi.current_sentence.tokens[m].mwe);
					}
					continue;
				}
				*/
				if(mwe_lengths[k] > 0){
					const int32_t key_canonical_size = 256;
					const int32_t key_non_canonical_size = 256;
					int32_t local_str_index;
					ch_t key_canonical[key_canonical_size];
					ch_t key_non_canonical[key_non_canonical_size];
	
					// -------- lemma --------
	
					qsort(&(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size]), mwe_lengths[k], mwe_token_bfr_size, strcmp);
	
					local_str_index = 0;
					memset(key_canonical, '\0', key_canonical_size);
					strncpy(key_canonical + local_str_index, "_MWE", 4);
					local_str_index += 4;
	
					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(local_str_index + 1 < key_canonical_size - 1){
							strncpy(key_canonical + local_str_index, "_", 1);
							local_str_index++;
							size_t len_lemma = strlen(&(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]));
							if(local_str_index + len_lemma < key_canonical_size - 1){
								strncpy(key_canonical + local_str_index, &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), len_lemma);
								local_str_index += len_lemma;
							}
						}
					}
	
					int64_t index_mwe_canonical;
					if(type_node_add_node_if_absent(&(mg.type_nodes[mwe_canonical_i]), key_canonical, &index_mwe_canonical) != 0){perror("failed to call type_node_add_node_if_absent\n"); return 1;}
					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_canonical_i].nodes[index_mwe_canonical]), (tn_index_t) lemma_i, MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_LEMMA, &(mwe_bfr_lemma[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_canonical_i].nodes[index_mwe_canonical]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_UPOS, &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_canonical_i].nodes[index_mwe_canonical]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_XPOS, &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
					}
	
					// -------- form --------
	
					qsort(&(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size]), mwe_lengths[k], mwe_token_bfr_size, strcmp);
	
					local_str_index = 0;
					memset(key_non_canonical, '\0', key_non_canonical_size);
					strncpy(key_non_canonical + local_str_index, "_MWE", 4);
					local_str_index += 4;
	
					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(local_str_index + 1 < key_non_canonical_size - 1){
							strncpy(key_non_canonical + local_str_index, "_", 1);
							local_str_index++;
							size_t len_form = strlen(&(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]));
							if(local_str_index + len_form < key_non_canonical_size - 1){
								strncpy(key_non_canonical + local_str_index, &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size]), len_form);
								local_str_index += len_form;
							}
						}
					}
	
					int64_t index_mwe_non_canonical;
					if(type_node_add_node_if_absent(&(mg.type_nodes[mwe_non_canonical_i]), key_non_canonical, &index_mwe_non_canonical) != 0){perror("failed to call type_node_add_node_if_absent\n"); return 1;}
					for(int64_t m = 0 ; m < mwe_lengths[k] ; m++){
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_non_canonical_i].nodes[index_mwe_non_canonical]), (tn_index_t) form_i, MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_FORM, &(mwe_bfr_form[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_non_canonical_i].nodes[index_mwe_non_canonical]), (tn_index_t) upos_i, MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_UPOS, &(mwe_bfr_upos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
						if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_non_canonical_i].nodes[index_mwe_non_canonical]), (tn_index_t) xpos_i, MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_XPOS, &(mwe_bfr_xpos[k * mwe_max_num_tokens * mwe_token_bfr_size + m * mwe_token_bfr_size])) != 0){
							perror("failed to call multigraph_node_add_edge_v2\n");
							multigraph_free(&mg);
							return 1;
						}
					}
	
	
					// -------- binding --------
	
					// if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_canonical_i].nodes[index_mwe_canonical]), (tn_index_t) mwe_non_canonical_i, index_mwe_non_canonical) != 0){
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_canonical_i].nodes[index_mwe_canonical]), (tn_index_t) mwe_non_canonical_i, MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_MWE_NON_CANONICAL, key_non_canonical) != 0){
						perror("failed to call multigraph_node_add_edge_v2\n");
						multigraph_free(&mg);
						return 1;
					}
					if(multigraph_node_add_edge_v2(&(mg.type_nodes[mwe_non_canonical_i].nodes[index_mwe_non_canonical]), (tn_index_t) mwe_canonical_i, MULTIGRAPH_EDGE_KEY_MWE_NON_CANONICAL_TO_MWE_CANONICAL, key_canonical) != 0){
						perror("failed to call multigraph_node_add_edge_v2\n");
						multigraph_free(&mg);
						return 1;
					}
				}
			}
	
			if(iterate_cupt_sentence_iterator(&csi) != 0){
				perror("failed to call iterate_cupt_sentence_iterator\n");
				multigraph_free(&mg);
				return 1;
			}
			j++;
			sentence_count++;
		}

		free_cupt_sentence_iterator(&csi);
	}

	for(mg_size_t i = 0 ; i < mg.node_size ; i++){
		mgn_size_t edge_count = 0;
		for(tn_size_t k = 0 ; k < mg.type_nodes[i].node_size ; k++){
			edge_count += mg.type_nodes[i].nodes[k].edge_size;
		}
		printf("%li: %lu nodes, %lu edges (%s)\n", i, mg.type_nodes[i].node_size, edge_count, mg.type_nodes[i].key);
		
		// for(int64_t j = 0 ; j < mg.type_nodes[i].node_size ; j++){
		for(int64_t j = 0 ; j < mg.type_nodes[i].node_sorted_size ; j++){
			// printf("%s ", mg.type_nodes[i].nodes[j].key);
			if(j > 0){
				if(!(strcmp(mg.type_nodes[i].nodes[j-1].key, mg.type_nodes[i].nodes[j].key) <= 0)){
					perror("NOT SORTED\n");
					multigraph_free(&mg);
					return 1;
				}
				if(strcmp(mg.type_nodes[i].nodes[j-1].key, mg.type_nodes[i].nodes[j].key) == 0){
					fprintf(stderr, "NOT UNIQUE: %s / %s\n", mg.type_nodes[i].nodes[j-1].key, mg.type_nodes[i].nodes[j].key);
					// multigraph_free(&mg);
					// return 1;
				}
			}
		}
		
		// printf("\n");
	}

	// random walk
	
	/**/
	struct metapath mp;

	mp = (struct metapath) {};

	/*
	metapath_add_step(&mp, form_i);
	metapath_add_step(&mp, lemma_i);
	*/
	// metapath_add_step(&mp, mwe_canonical_i);
	// metapath_add_step(&mp, mwe_non_canonical_i);
	/*
	metapath_add_step(&mp, mwe_canonical_i, NULL);
	metapath_add_step(&mp, lemma_i, MULTIGRAPH_EDGE_KEY_MWE_CANONICAL_TO_LEMMA);
	metapath_add_step(&mp, form_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_FORM);
	*/
	// metapath_add_step(&mp, upos_i);
	// metapath_add_step(&mp, xpos_i);
	/**/

	/*
	metapath_add_step(&mp, form_i, NULL);
	// metapath_add_step(&mp, form_i, MULTIGRAPH_EDGE_KEY_FORM_IS_HEAD_OF_FORM);
	metapath_add_step(&mp, form_i, MULTIGRAPH_EDGE_KEY_FORM_HAS_HEAD_FORM);
	*/

	metapath_add_step(&mp, lemma_i, NULL, NULL);
	metapath_add_step(&mp, form_i, MULTIGRAPH_EDGE_KEY_LEMMA_TO_FORM, NULL);

	srand(3141592);

	// multigraph_collective_random_path(&mg, &mp, 100);
	// multigraph_collective_random_path(&mg, &mp, 10000);
	// multigraph_collective_random_path(&mg, &mp, 200000);
	// multigraph_collective_random_path(&mg, &mp, 500000);
	// multigraph_collective_random_path(&mg, &mp, 2000000);
	multigraph_collective_random_path(&mg, &mp, 5000000);
	// multigraph_collective_random_path(&mg, &mp, 50000000);

	int64_t target_i;

	// target_i = mwe_non_canonical_i;
	// target_i = mwe_canonical_i;
	target_i = form_i;
	// target_i = upos_i;
	// target_i = xpos_i;
	// target_i = lemma_i;
	// target_i = mp.path[mp.path_size - 1];

	/* // DO NOT REMOVE
	for(tn_size_t i = 0 ; i < mg.type_nodes[target_i].node_size ; i++){
		if(mg.type_nodes[target_i].nodes[i].count_end > 0){
			printf("%lu: %s -> %u\n", i, mg.type_nodes[target_i].nodes[i].key, mg.type_nodes[target_i].nodes[i].count_end);
		}
	}
	*/

	struct graph g;

	if(type_node_count_end_to_graph(&(mg.type_nodes[target_i]), &g, 1) != 0){
		perror("failed to call type_node_count_end_to_graph\n");
		multigraph_free(&mg);
		return 1;
	}

	/* // DO NOT REMOVE
	double res1, res2;

	shannon_weaver_entropy_from_graph(&g, &res1, &res2);
	printf("(collective) sw entropy: %f, %f\n", res1, res2);

	if(create_matrix(&g.dist_mat, g.num_nodes, g.num_nodes, FP32) != 0){
		perror("failed to call create_matrix\n");
		free_graph(&g);
		free_word2vec(&w2v);
		multigraph_free(&mg);
		return 1;
	}
	if(compute_graph_dist_mat(&g, 4) != 0){
		perror("failed to call compute_graph_dist_mat\n");
		free_graph(&g);
		free_word2vec(&w2v);
		multigraph_free(&mg);
		return 1;
	}

	pairwise_from_graph(&g, &res1, FP32, &(g.dist_mat));
	printf("(collective) pairwise: %f\n", res1);
	*/

	free_graph(&g);

	/* // DO NOT REMOVE
	g = (struct graph) {};

	if(multigraph_individual_random_path(&mg, &mp, 1000000, "manger") != 0){
		perror("failed to call multigraph_individual_random_path\n");
		free_graph(&g);
		metapath_free(&mp);
		multigraph_free(&mg);
		return 1;
	}

	if(type_node_count_end_to_graph(&(mg.type_nodes[target_i]), &g) != 0){
		perror("failed to call type_node_count_end_to_graph\n");
		multigraph_free(&mg);
		return 1;
	}

	shannon_weaver_entropy_from_graph(&g, &res1, &res2);

	printf("(relative) sw entropy: %f, %f\n", res1, res2);

	free_graph(&g);
	*/

	/* // DO NOT REMOVE
	double mean_res;

	if(multigraph_mean_individual_random_path(&mg, &mp, 100, &mean_res) != 0){
		perror("failed to call multigraph_mean_individual_random_path\n");
		metapath_free(&mp);
		multigraph_free(&mg);
		return 1;
	}

	printf("(mean individual) sw entropy: %f\n", mean_res);
	*/

	// ...
	
	metapath_free(&mp);





	// --------
	
	read_cli(&mg);

	multigraph_free(&mg);

	free_word2vec(&w2v);

	return 0;
}
