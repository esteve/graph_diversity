--CREATE VIRTUAL TABLE IF NOT EXISTS t_sentence USING fts5(
--	fk_file		INTEGER,
--	sent_id		TEXT,
--	raw_form	TEXT,
--	raw_lemma	TEXT,
--	CONSTRAINT	t_sentence_fk_file	FOREIGN KEY (fk_file)	REFERENCES t_filename (filename),
--	CONSTRAINT	t_sentence_unique	UNIQUE (fk_file, sent_id)
--);
CREATE VIRTUAL TABLE IF NOT EXISTS t_sentence USING fts5(
	fk_file UNINDEXED,
	sent_id UNINDEXED,
	--raw_form	TEXT	CHECK(length(raw_form<4096)),
	--raw_lemma	TEXT	CHECK(length(raw_form<4096))
	raw_form,
	raw_lemma,
	--detail=column
	detail=none
);
