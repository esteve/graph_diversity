/*
 *      DiversGraph - Graphs to measure diversity
 *
 * Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GENGRAPH_CLI_H
#define GENGRAPH_CLI_H

#include <stdint.h>
#include <sqlite3.h>

#include "dfunctions.h"
#include "gengraph.h"
#include "db_interface.h"

int32_t read_cli(struct multigraph* const mg){
	const int32_t bfr_length = 1024;

	ch_t bfr[bfr_length];

	memset(bfr, '\0', bfr_length * sizeof(ch_t));

	while(1){
		printf("----------------\n#> ");
		fgets(bfr, bfr_length, stdin);

		if(bfr[0] == '.'){
			if(strncmp(bfr, ".help", 5) == 0){
				printf("test help message\n");
			} else if(strncmp(bfr, ".exit", 5) == 0){
				break;
			} else {
				printf("Unknown command: %s\n", bfr);
			}
		} else {
			struct metapath mp = (struct metapath) {};

			ch_t* strtok_pointer = strtok(bfr, "/");

			mg_size_t target_i;
			
			while (strtok_pointer != NULL){
				ch_t* linefeed_pointer = strchr(strtok_pointer, '\n');
				if(linefeed_pointer != NULL){
					(*linefeed_pointer) = '\0';
				}


				target_i = multigraph_get_index(mg, strtok_pointer);

				if(metapath_add_step(&mp, target_i, NULL, NULL, "%") != 0){
					perror("failed to call metapath_add_step\n");
					metapath_free(&mp);
					break;
				}

				strtok_pointer = strtok(NULL, "/");
			}

			if(mp.path_size <= 0){
				perror("mp.path_size <= 0\n");
				metapath_free(&mp);
				break;
			}

			// srand(3141592);

			multigraph_collective_random_path(mg, &mp, 5000000);

			struct graph g = (struct graph) {};

			// if(type_node_count_end_to_graph(&(mg->type_nodes[mp.path[mp.path_size - 1]]), &g) != 0){
			if(type_node_count_end_to_graph(&(mg->type_nodes[mp.path[mp.path_size - 1].step_destination_index]), &g, 1) != 0){
				perror("failed to call type_node_count_end_to_graph\n");
				break;
			}

			printf("Number of nodes in diversity computation: %lu\n", g.num_nodes);

			double res1, res2;

			shannon_weaver_entropy_from_graph(&g, &res1, &res2);

			printf("(collective) sw entropy: %f, %f\n", res1, res2);
		
			if(create_matrix(&g.dist_mat, g.num_nodes, g.num_nodes, FP32) != 0){
				perror("failed to call create_matrix\n");
				free_graph(&g);
				return 1;
			}
			if(compute_graph_dist_mat(&g, 4) != 0){
				perror("failed to call compute_graph_dist_mat\n");
				free_graph(&g);
				return 1;
			}
		
			/**/
			pairwise_from_graph(&g, &res1, FP32, &(g.dist_mat));
			printf("(collective) pairwise: %f\n", res1);

			chao_et_al_functional_diversity_from_graph(&g, &res1, &res2, 2.0, FP32, &(g.dist_mat));
			printf("(collective) chao et al func.: %f\n", res1);
			/**/

			free_graph(&g);

			metapath_free(&mp);
		}
	}

	return 0;
}

int32_t read_cli_sql_v2(sqlite3* const db, struct multigraph* const mg){
	const int32_t bfr_length = 1024;
	uint8_t mean_mode = 0;
	uint8_t backward_mode = 0;
	uint8_t relative_mode = 0;
	uint8_t do_continue = 0;
	uint8_t do_exit = 0;
	uint32_t offset_delta = 0;
	double avg;
	int32_t n;
	ch_t *ptr_last_slash;

	ch_t bfr[bfr_length];

	memset(bfr, '\0', bfr_length * sizeof(ch_t));

	while(1){
		printf("================================\n#> ");
		fgets(bfr, bfr_length, stdin);

		ptr_last_slash = strrchr(bfr, '/');

		while(bfr[offset_delta] == '.'){
			if(strncmp(&(bfr[offset_delta]), ".help", 5) == 0){
				do_continue = 1;
				printf("Query syntax: each metatype is to be referred to by its name.\nList of loaded metatypes:\n");
				for(int32_t i = 0 ; i < mg->node_size ; i++){
					printf("%i / %s\n", i, mg->type_nodes[i].key);
				}
				printf("\nA simple query is of the form\n\ta/b/c\nwhere a, b, and c are metatypes from the list above.\n");
				printf("You may add filters based on POSIX extended regex patterns.\n\n");
				printf("Say we want to filter b from the example above for keys that start with the letter 'f',\nwe may do it with:\n");
				printf("\ta/b=f.*/c\n");
				printf("\nYou may also add .backward which inverts the query (its meaning also changes):\n");
				printf("\t.backward a/b=f.*/c\n");
				printf("\nLikewise you may add .mean to compute for each option of the first metatype\n(or last if .backward is also present):\n");
				printf("\t.mean a/b=f.*/c\n");
				printf("\t.backward .mean a/b=f.*/c\n");
				printf("\nYou may compute relative diversity between individual query and unconditional query with:\n");
				printf("\t.relative a=.*someregex.*/b=.*/c\n");
				printf("\nYou may compute relative diversity two arbitraty queries with:\n");
				printf("\t.relative2 a/b=.*/c d/e/c\n");
				printf("\n.help\t\tDisplay this help message.\n");
				printf(".mean\t\tCompute mean individual diversity. Compatible with .backward\n");
				printf(".backward\tInverse the metapath. Compatible with .mean\n");
				printf(".relative\tRelative diversity between an individual query and an unconditional query.\n");
				printf(".relative2\tRelative diversity between two arbitraty queries (separate with a space).\n");
				break;
			} else if(strncmp(&(bfr[offset_delta]), ".exit", 5) == 0){
				do_exit = 1;
				break;
			} else if(strncmp(&(bfr[offset_delta]), ".mean", 5) == 0){
				mean_mode = 1;
				offset_delta += 6;
			} else if(strncmp(&(bfr[offset_delta]), ".backward", 9) == 0){
				backward_mode = 1;
				offset_delta += 10;
			} else if(strncmp(&(bfr[offset_delta]), ".relative2", 10) == 0){
				relative_mode = 2;
				offset_delta += 11;
			} else if(strncmp(&(bfr[offset_delta]), ".relative", 9) == 0){
				relative_mode = 1;
				offset_delta += 10;
			} else {
				printf("Unknown command: %s\n", &(bfr[offset_delta]));
				do_continue = 1;
				break;
			}
		}

		if(do_exit){break;}
		if(do_continue){do_continue = 0; continue;}

		// ptr_first_space = strchr(&(bfr[offset_delta]), ' ');

		struct metapath mps[2] = {
			(struct metapath) { .backward = backward_mode, },
			(struct metapath) { .backward = backward_mode, },
		};
		int32_t current_mp_index = 0;
		ch_t* strtok_pointer;
		const size_t filter_bfr_size = 64;
		ch_t filter[filter_bfr_size];
		ch_t* ptr_eq;
		size_t bytes_to_cpy;

		mg_size_t target_i;

		int32_t j = 0;
		
		int32_t m = offset_delta;
		while(1){
			strtok_pointer = strtok(bfr + m, "/");
			while (strtok_pointer != NULL){
				ch_t* linefeed_pointer = strchr(strtok_pointer, '\n');
				if(linefeed_pointer != NULL){
					(*linefeed_pointer) = '\0';
				}
				ch_t* space_pointer = strchr(strtok_pointer, ' ');
				/**/
				if(space_pointer != NULL){
					(*space_pointer) = '\0';
				}
				/**/
	
				memset(filter, '\0', filter_bfr_size * sizeof(ch_t));
				ptr_eq = strchr(strtok_pointer, '=');
				if(ptr_eq == NULL){
					// filter[0] = '%';
					filter[0] = '.';
					filter[1] = '*';
				} else {
					(*ptr_eq) = '\0';
					bytes_to_cpy = strlen(ptr_eq + 1);
					if(bytes_to_cpy > filter_bfr_size - 1){
						bytes_to_cpy = filter_bfr_size - 1;
					}
					memcpy(filter, ptr_eq + 1, bytes_to_cpy);
				}
				printf("q%i / %i / %s; filter: %s\n", current_mp_index, j, strtok_pointer, filter);
	
				target_i = multigraph_get_index(mg, strtok_pointer);
	
				if(metapath_add_step(&(mps[current_mp_index]), target_i, NULL, NULL, filter) != 0){
					perror("failed to call metapath_add_step\n");
					// metapath_free(&mp1);
					metapath_free(&(mps[0]));
					break;
				}

				if(ptr_eq != NULL){(*ptr_eq) = '=';}
				if(j > 0){*(strtok_pointer - 1) = '/';}
	
				
				if(space_pointer == NULL){
					strtok_pointer = strtok(NULL, "/");
					j++;
				} else {
					/*
					printf("starting back from %s\n", space_pointer + 1);
					strtok_pointer = strtok(space_pointer + 1, "/");
					j = 0;
					if(current_mp_index == 0){
						current_mp_index++;
					} else {
						perror("Trying to add a third query is not accepted\n");
						metapath_free(&(mps[0]));
						metapath_free(&(mps[1]));
						return 1;
					}
					*/
					(*space_pointer) = ' ';
					break;
				}
	
			}

			uint8_t break_loop = 0;
			while(1){
				if(bfr[m] == '\0'){
					// printf("found a null byte at index %i\n", m);
					break_loop = 1;
					break;
				}
				if(bfr[m] == ' '){
					// printf("found a space at index %i\n", m);
					if(current_mp_index == 0){
						current_mp_index++;
						j = 0;
						ch_t* ptr_null_byte = strchr(&(bfr[m + 1]), '\0');
						if(ptr_last_slash != NULL && ptr_null_byte <= ptr_last_slash){
							(*ptr_null_byte) = '/';
						}
						// printf("starting back at %s\n", &(bfr[m + 1]));
						m++;
						break;
					} else {
						perror("Trying to add a third query is not accepted\n");
						metapath_free(&(mps[0]));
						metapath_free(&(mps[1]));
						return 1;
					}
				}
				m++;
			}
			if(break_loop){break;}
		}

		if(mps[0].path_size <= 0){
			perror("mps[0].path_size <= 0\n");
			metapath_free(&(mps[0]));
			break;
		}

		if(mean_mode == 0){
			printf("----------------\n");
			if(relative_mode == 0){
				multigraph_collective_random_path_sql_v2(db, mg, &(mps[0]), NULL, NULL);
			} else if(relative_mode == 1){
				/*
				struct metapath mp2;
				memcpy(&mp2, &mp1, sizeof(struct metapath));
				mp2.path[0].filter = malloc(2);
				if(mp2.path[0].filter == NULL){
					perror("failed to malloc\n");
					metapath_free(&mp1);
					return 1;
				}
				printf("mp1.path[0].filter: %p, %s\n", mp1.path[0].filter, mp1.path[0].filter);
				printf("mp2.path[0].filter: %p, %s\n", mp2.path[0].filter, mp2.path[0].filter);
				mp2.path[0].filter[0] = '%';
				mp2.path[0].filter[1] = '\0';
				printf("mp1.path[0].filter: %p, %s\n", mp1.path[0].filter, mp1.path[0].filter);
				printf("mp2.path[0].filter: %p, %s\n", mp2.path[0].filter, mp2.path[0].filter);
				multigraph_collective_random_path_sql_v2(db, mg, &mp1, &mp2, NULL);
				free(mp2.path[0].filter);
				*/
				// struct metapath mp2 = (struct metapath) {};
				// if(metapath_add_step(&mp2, mp1.path[0].step_destination_index, mp1.path[0].edge_key, mp1.path[0].path_sql, "%") != 0){
				if(metapath_add_step(&(mps[1]), mps[0].path[0].step_destination_index, mps[0].path[0].edge_key, mps[0].path[0].path_sql, "%") != 0){
					perror("failed to call metapath_add_step\n");
					// metapath_free(&mp1);
					metapath_free(&(mps[0]));
					return 1;
				}
				// for(int32_t k = 1 ; k < mp1.path_size ; k++){
				for(uint32_t k = 1 ; k < mps[0].path_size ; k++){
					// if(metapath_add_step(&mp2, mp1.path[k].step_destination_index, mp1.path[k].edge_key, mp1.path[k].path_sql, mp1.path[k].filter) != 0){
					if(metapath_add_step(&(mps[1]), mps[0].path[k].step_destination_index, mps[0].path[k].edge_key, mps[0].path[k].path_sql, mps[0].path[k].filter) != 0){
						perror("failed to call metapath_add_step\n");
						// metapath_free(&mp1);
						metapath_free(&(mps[0]));
						return 1;
					}	
				}
				// multigraph_collective_random_path_sql_v2(db, mg, &mp1, &mp2, NULL);
				multigraph_collective_random_path_sql_v2(db, mg, &(mps[0]), &(mps[1]), NULL);
				// metapath_free(&mp2);
				metapath_free(&(mps[1]));
			} else if(relative_mode == 2){
				multigraph_collective_random_path_sql_v2(db, mg, &(mps[0]), &(mps[1]), NULL);
				metapath_free(&(mps[1]));
			}
		} else if(mean_mode == 1){
			char bfr_backup_start_filter[bfr_length];
			sqlite3_stmt* stmt_list;
			uint8_t can_continue_list;
			int32_t starting_index;

			if(mps[0].backward){starting_index = mps[0].path_size - 1;} else {starting_index = 0;}

			memset(bfr_backup_start_filter, '\0', bfr_length);

			bytes_to_cpy = strlen(mps[0].path[starting_index].filter);
			if(bytes_to_cpy > bfr_length - 1){
				bytes_to_cpy = bfr_length - 1;
			}
			memcpy(bfr_backup_start_filter, mps[0].path[starting_index].filter, bytes_to_cpy);

			free(mps[0].path[starting_index].filter);

			if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "lang") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_LANG) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "form") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_FORM) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "lemma") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_LEMMA) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "feats") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_FEATS) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "deprel") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_DEPREL) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "upos") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_UPOS) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "xpos") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_XPOS) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "mwe_canonical") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_MWE_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else if(strcmp(mg->type_nodes[mps[0].path[starting_index].step_destination_index].key, "mwe_non_canonical") == 0){
				if(db_prepare_statement(db, &stmt_list, PATH_SQL_LIST_MWE_NON_CANONICAL) != 0){perror("failed to call db_prepare_statement\n"); metapath_free(&mps[0]); return 1;}
			} else {
				perror("Cannot list this starting metatype, needs to be defined\n");
				metapath_free(&mps[0]);
				return 1;
			}

			if(sqlite3_bind_text(stmt_list, 1, bfr_backup_start_filter, strlen(bfr_backup_start_filter), NULL) != 0){perror("failed to call sqlite3_bind_text\n"); metapath_free(&mps[0]); return 1;}

			avg = 0.0;
			n = 0;

			can_continue_list = 1;
			do {
				printf("----------------\n");
				if(try_statement(stmt_list, &can_continue_list) != 0){perror("failed to call try_statement\n"); metapath_free(&mps[0]); return 1;}
				mps[0].path[starting_index].filter = (char*) sqlite3_column_text(stmt_list, 0);
				if(mps[0].path[starting_index].filter == NULL){continue;}
				multigraph_collective_random_path_sql_v2(db, mg, &mps[0], NULL, &avg);
				n++;
			} while(can_continue_list);

			mps[0].path[starting_index].filter = NULL;

			printf("Average: %f\n", avg / ((double) n));
		}

		metapath_free(&mps[0]);

		mean_mode = 0;
		backward_mode = 0;
		relative_mode = 0;
		offset_delta = 0;
	}

	return 0;
}
#endif
